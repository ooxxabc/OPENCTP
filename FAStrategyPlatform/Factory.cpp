#include "Factory.h"
#include "TaskFactory.h"


CTask * CFactory::Create(FLOW_TYPE type)
{
	switch (type)
	{
	case FLOW_TRADE_CONNECT:
	//case FLOW_CONNECT:
		{
			CTradeConnectTask * p = new CTradeConnectTask();
			CTask * task = new CTask(type, p);
			return task;
		}
		break;
	case FLOW_QUOTE_CONNECT:
		{
			CQuoteConnectTask * p = new CQuoteConnectTask();
			CTask * task = new CTask(type, p);
			return task;
		}
		break;
	case FLOW_LOGIN:
		{
			CLoginTask * p = new CLoginTask();
			CTask * task = new CTask(type,p);
			return task;
		}
		break;
	case FLOW_SETTLEMENT:
		{
			CSettlementTask * p = new CSettlementTask();
			CTask * task = new CTask(type, p);
			return task;
		}
		break;
	case FLOW_CONFIRM_SETTLEMENT:
	{
		CConfirmSettlementTask * p = new CConfirmSettlementTask();
		CTask * task = new CTask(type, p);
		return task;
	}
	break;
	case FLOW_INSTRUMENT:
		{
			CInstrumentTask * p = new CInstrumentTask();
			CTask * task = new CTask(type, p);
			return task;
		}
		break;
	case FLOW_ACCOUNT:
		{
			CAccountTask * p = new CAccountTask();
			CTask * task = new CTask(type, p);
			return task;
		}
		break;
	case FLOW_POSITION:
		{
			CPositionTask * p = new CPositionTask();
			CTask * task = new CTask(type, p);
			return task;
		}
		break;
	case FLOW_EXIT:
		{
			CExitTask * p = new CExitTask();
			CTask * task = new CTask(type, p);
			return task;
		}
		break;
	default:
		return 0;
		break;
	}

	return 0;
}

