#pragma once
#include "StrategyPlatform/StrategyPlatform_Def.h"

class CFAQuoteEvent : public IQuoteEvent
{
public:
	CFAQuoteEvent();
	~CFAQuoteEvent();

	// 行情连接处理
	virtual void OnQuoteConnected();

	// 行情用户登录成功
	virtual void OnQuoteUserLoginSuccess();

	// 行情用户登录失败
	virtual void OnQuoteUserLoginFailed(int nErrorID, const char* pszMsg) {

	}

	// 行情断开处理
	virtual void OnQuoteDisconnected(int nReason);

	// 行情错误处理
	virtual void OnQuoteError(int nErrorID, const char* pszMsg);


	// 行情订阅成功
	virtual void OnQuoteSubMarketDataSuccess(char* pszInstrumentID) {

	}

	// 行情订阅失败
	virtual void OnQuoteSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg) {

	}


	// 行情数据处理
	virtual void OnQuoteMarketData(tagMarketData* pstMarketData);


	// 行情退订成功
	virtual void OnQuoteUnSubMarketDataSuccess(char* pszInstrumentID) {

	}

	// 行情退订失败
	virtual void OnQuoteUnSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg){

	}
};

