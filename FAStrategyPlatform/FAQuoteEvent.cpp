#include "FAQuoteEvent.h"
#include "StrategyPlatform.h"
#include "dll_res.h"

CFAQuoteEvent::CFAQuoteEvent()
{
}


CFAQuoteEvent::~CFAQuoteEvent()
{
}

void CFAQuoteEvent::OnQuoteConnected(){
	gStrategyPlatrom->GetQuote()->Login(CONFIG->GetBrokerID(), CONFIG->GetAccount(), CONFIG->GetPassword());
	OUTPUT2(LOG_INFO, "����QUOTE�ɹ�\n");
}

void CFAQuoteEvent::OnQuoteDisconnected(int nReason)
{
	OUTPUT2(LOG_ERROR, "����QUOTEʧ��:%d\n", nReason);
	gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_QUOTE_CONNECT));
	gStrategyPlatrom->GetFlow()->SetContinue(true);
}

void CFAQuoteEvent::OnQuoteUserLoginSuccess() {
	//����
	char *p[INS_MAX_COUNT] = { 0 };
	int nCount = 0;
	gStrategyPlatrom->GetIns(p,nCount);
	//���ĺ�Լ
	gStrategyPlatrom->GetQuote()->SubIns(p, nCount);

	gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_TRADE_CONNECT));
	gStrategyPlatrom->GetFlow()->SetContinue(true);

}

void CFAQuoteEvent::OnQuoteMarketData(tagMarketData* pstMarketData) {

	if (pstMarketData)
	{
		//OUTPUT2(LOG_TRACE, "QUOTE:%s,%.1f\n", pstMarketData->szINSTRUMENT,pstMarketData->dLastPrice);
		if (DLL->m_print)
		{
			//DLL->m_print->Print(LOG_TRACE, "[Quote]%s,%.1lf\n", pstMarketData->szINSTRUMENT, pstMarketData->dLastPrice);
			//DLL->m_print->Print(LOG_TRACE, "[Quote]%s\n", pstMarketData->szINSTRUMENT);
		}			
	}

	gStrategyPlatrom->BroadCastQuote(pstMarketData);

}

void CFAQuoteEvent::OnQuoteError(int nErrorID, const char* pszMsg)
{
	OUTPUT2(LOG_ERROR, "QUOTE ERR:%d[%s]\n", nErrorID, pszMsg);
}