// StrategyPlatform.cpp : 定义 DLL 应用程序的导出函数。
//

#include "StrategyPlatform.h"

CStrategyPlatform * gStrategyPlatrom = NULL;

/************************************************************************/
/* 
*/
/************************************************************************/

// 这是导出函数的一个示例。
FA_API IStrategyPlatform* CreateObject(void)
{
	if (!gStrategyPlatrom)
	{
		gStrategyPlatrom = new CStrategyPlatform();
	}
	return gStrategyPlatrom;
}

FA_API void ReleaseObject(void)
{
	if (gStrategyPlatrom)
	{
		gStrategyPlatrom->UnInit();

		delete gStrategyPlatrom;

		gStrategyPlatrom = NULL;
	}
}