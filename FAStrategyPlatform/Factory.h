#pragma once
#include "FAFlow/FAFlow_def.h"
#include "single.h"

/*
	工厂类基类
*/

class CFactory
{
public:
	CFactory(){}
	virtual ~CFactory(){}

	CTask * Create(FLOW_TYPE type);
};
#define FACTORY singleton_t<CFactory>::instance()
