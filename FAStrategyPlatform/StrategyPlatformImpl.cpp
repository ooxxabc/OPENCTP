
#include "dll_res.h"

CStrategyPlatform::CStrategyPlatform(void) 
:m_pQuote(NULL)
, m_pTrade(NULL)
, m_pQuoteEvent(NULL)
, m_pTradeEvent(NULL)
, m_plusCount(0)
, nPlusInsCount(0)
, m_bConfirmSettlement(0)
//, m_semaQuit(0)
, m_pFlow(0)
,m_pPositionAnalyze(0)
{
	DLL->Init();

	memset(m_plus, 0, sizeof(m_plus));
	memset(m_plus, 0, sizeof(m_plus));
	memset(szPlusIns, 0, sizeof(szPlusIns));
	memset(m_PlusDll, 0, sizeof(m_PlusDll));
	//m_semaQuit = new CYKSemaphore;
}

int CStrategyPlatform::LoadConfig()
{
	//return CONFIG->Load();
	return CONFIG->Load();
}

int CStrategyPlatform::ReLoadConfig()
{
	//return CONFIG->ReLoad();
	return CONFIG->ReLoad();


}

int CStrategyPlatform::UnInit()
{
	//m_semaQuit->signal();

	Sleep(1000);

	if (m_pFlow)
		m_pFlow->Stop();

	DLL->UInit();	

	//if (_uintQuote)
	//	_uintQuote();

	//if (_uintTrade)
	//	_uintTrade();

	SAFE_DEL(m_pQuoteEvent);
	SAFE_DEL(m_pTradeEvent);
	SAFE_DEL(m_pFlow);

	for (int i = 0; i < INS_MAX_COUNT;i++)
	{
		SAFE_DEL(m_PlusDll[i]);
	}

	////等待线程结束
	//WaitForSingleObject(m_semaQuit, SEMA_WAIT_INFINITE);
	//WaitForSingleObject(m_semaQuit, SEMA_WAIT_INFINITE);

	//SAFE_DEL(m_semaQuit);


	return FAIL;
}

int CStrategyPlatform::Init()
{
	if (SUCCESS != LoadConfig())
		return FAIL;
	
	//加载策略dll
	tagStratePlus *plusDll = CONFIG->GetPlusDll();
	int nPlusCount = CONFIG->GetPlusCount();
	if (nPlusCount < 1)
	{
		OUTPUT2(LOG_INFO, "无策略加载...");
	}

	m_plusCount = 0;
	char szFile[URL_LEN];
	CreateStrategyPlus _plusFunc;
	for (int i = 0; i < nPlusCount; i++)
	{
		sprintf_s(szFile, URL_LEN, "%s.dll", plusDll[i].szDll);
		m_PlusDll[i] = new CDllHelper(szFile);
		_plusFunc = m_PlusDll[i]->GetProcedure<CreateStrategyPlus>("CreateObject");

		//CDllHelperAuto dll(new CDllHelper(szFile));
		//_plusFunc = dll->GetProcedure<CreateStrategyPlus>("CreateObject");
		if (_plusFunc)
		{
			//注册多策略分析
			IPosition * pPositionHandle = NULL;
			if(m_pPositionAnalyze)
			{
				CStrategyID id(plusDll[i].nId, plusDll[i].szDll);
				pPositionHandle = m_pPositionAnalyze->RegStrategy(id);
			}

			m_plus[m_plusCount] = _plusFunc(this);
			if (SUCCESS != m_plus[m_plusCount]->Init(DLL->m_print,pPositionHandle))
			{
				OUTPUT2(LOG_ERROR, "策略%d加载失败", m_plusCount);
				continue;
			}
			m_mPlusInstance[plusDll[i].szDll] = m_plus[m_plusCount];

			

			m_plusCount++;
			_plusFunc = 0;
		}
	}

	//CTP接口初始化
	if (!m_pQuoteEvent)
	{
		m_pQuoteEvent = new CFAQuoteEvent();
	}

	if (!m_pTradeEvent)
	{
		m_pTradeEvent = new CFATradeEvent();
	}

	if (DLL->pfCreateQuote)
	{
		m_pQuote = DLL->pfCreateQuote(m_pQuoteEvent);
	}

	if (DLL->pfCreateTrade)
	{
		m_pTrade = DLL->pfCreateTrade(m_pTradeEvent);
	}

	if (DLL->pfCreatePosition)
	{
		m_pPositionAnalyze = DLL->pfCreatePosition();
	}

	//连接服务器
	if(!m_pQuote || !m_pTrade)
		return FAIL;
	
	//m_pQuote->Connect(CONFIG->GetQuoteURL());
	//m_pTrade->Connect(CONFIG->GetTradeURL());

	/*
		flow控制模块
	*/
	m_pFlow = DLL->pfCreateFlow();
	m_pFlow->Start();
	//工厂类生产task
	//m_pFlow->RegisterQueryTask(FACTORY->Create(FLOW_TRADE_CONNECT));
	m_pFlow->RegisterQueryTask(FACTORY->Create(FLOW_QUOTE_CONNECT));

	OUTPUT2(LOG_INFO, "策略运行中...");


	//监控初始化
	m_orderMonitor.Init(this, CONFIG->GetMonitorCfg().fSplippage, CONFIG->GetMonitorCfg().nCancelTime);
	m_orderMonitor.Start();

	return SUCCESS;

}

void	CStrategyPlatform::BroadCastQuote(tagMarketData *pData)
{
	//广播给所有plus
	for (int i = 0; i < m_plusCount; i++)
	{
		if (!m_plus[i])
			continue;

		m_plus[i]->OnQuote(pData);
	}

	//存储行情
	StorgeTick(pData);
}

void	CStrategyPlatform::StorgeTick(tagMarketData *pData)
{
	//提交到队列,交由其他线程处理
}

int CStrategyPlatform::SubInstrument(char*szIns[], int &nCount)
{
	if (nCount < 1)
		return FAIL;

	//m_pQuote->SubIns(szIns, nCount);
	
	//增加订阅队列
	AddPlusIns(szIns, nCount);

	return SUCCESS;
}

void CStrategyPlatform::AddPlusIns(char *p[], int &nCount)
{
	for (int i = 0; i < nCount;i++)
	{
		sprintf_s(szPlusIns[nPlusInsCount + i],INS_ID_LENGTH,p[i]);
	}

	nPlusInsCount += nCount;
}


char ** CStrategyPlatform::GetIns(char *p[], int &nCount)
{
	size_t i = 0;
	for (; i < nPlusInsCount; i++)
	{
		p[i] = szPlusIns[i];
	}

	char *ins[INS_MAX_COUNT] = { 0 };
	CONFIG->GetSubIns(ins);
	for (size_t j = 0; j < CONFIG->GetInsCount(); j++)
	{
		p[i + j] = ins[j];
	}

	nCount = nPlusInsCount + CONFIG->GetInsCount();
	
	return p;
}

int CStrategyPlatform::Order(tagOrderDelegation &orderInsert,IPosition * pPos)
{
	if (!m_bConfirmSettlement)
	{
		OUTPUT2(LOG_ERROR, "结算单未确认，无法下单!");
		gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_CONFIRM_SETTLEMENT));
		gStrategyPlatrom->GetFlow()->SetContinue(true);
		return FAIL;
	}

	//const char* pszInstrumentID, enTradeType nTradeType, enTradeDir nTradeDir, enTradeOperate nTradeOperate, enTradeOrderType nOrderType, double dPrice, int nVolume, char* pszOrderRefSuffix
	enTradeType nTradeType = TRADE_TYPE_SPECULATION;
	enTradeDir nTradeDir = (orderInsert.typeOrder == BUY || orderInsert.typeOrder == BUYTOCOVER) ? TRADE_DIR_BUY : TRADE_DIR_SELL;
	enTradeOperate nTradeOperate = (orderInsert.typeOrder == BUY || orderInsert.typeOrder == SELLSHORT) ? TRADE_OPERATE_OPEN : TRADE_OPERATE_CLOSE_TODAY;
	if (nTradeOperate == TRADE_OPERATE_CLOSE_TODAY)
	{
		if (orderInsert.bCoverYestoday)
		{
			nTradeOperate = TRADE_OPERATE_CLOSE;
		}
	}
	enTradeOrderType nOrderType = TRADE_ORDER_TYPE_LIMIT;
	int nRequestID = 0;
	int ret = m_pTrade->Order(orderInsert.szIns,nTradeType,nTradeDir,nTradeOperate,\
		nOrderType,orderInsert.price,orderInsert.volume,SOFT_FLAG, nRequestID);

	//将此order加入监控队列(线程安全的队列)
	orderInsert.timestamp = time(NULL);
	if (ret == 1)
	{
		//m_MOrderDelegation[nRequestID] = orderInsert;
		orderInsert.nRequestID = nRequestID;
		
	}
	return ret == 1 ?SUCCESS:FAIL;
}

int CStrategyPlatform::OrderCancel(tagOrder*pOrder)
{
	if (!pOrder) return 1;
	m_pTrade->CancelOrder(pOrder);
	return 0;
}

LPSTRATEGYPLUS  CStrategyPlatform::GetStrategy(char *szStrategy)
{
	return m_mPlusInstance[szStrategy];
}

int CStrategyPlatform::OnAccountDetail(tagTradingAccount*pDetail)
{
	for (mIStrategyInstanceIt it = m_mPlusInstance.begin(); it !=m_mPlusInstance.end(); ++it)
	{
		it->second->OnAccountDetail(pDetail);
	}
	return 0;
}

int CStrategyPlatform::OnPositionDetail(tagPosition*pPos,bool bLast)
{
	//通知策略仓位信息
	for (mIStrategyInstanceIt it = m_mPlusInstance.begin(); it != m_mPlusInstance.end(); ++it)
	{
		it->second->OnPositionDetail(pPos, bLast);
	}
	return 0;
}

int CStrategyPlatform::OnTradeRtnTrade(tagTrade* pstTrade)
{
	return 0;
}

int CStrategyPlatform::OnTradeRtnOrder(tagOrder* pstOrder)
{
	for (mIStrategyInstanceIt it = m_mPlusInstance.begin(); it != m_mPlusInstance.end(); ++it)
	{
		it->second->OnTraded(pstOrder);
	}
	return 0;
}

int CStrategyPlatform::OnTradeError(int nRequestID)
{
	return 0;
}

