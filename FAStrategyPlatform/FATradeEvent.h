#pragma once
#include "StrategyPlatform/StrategyPlatform_Def.h"


class CFATradeEvent:public ITradeEvent
{
public:
	CFATradeEvent();
	~CFATradeEvent();

	// 交易连接处理
	virtual void OnTradeConnected();
	// 交易断开处理
	virtual void OnTradeDisconnected(int nReason);

	// 交易用户登录成功
	virtual void OnTradeUserLoginSuccess(int nSessionID, int nFrontID, int nOrderRef);

	// 交易用户登录失败
	virtual void OnTradeUserLoginFailed(int nErrorID, const char* pszMsg);

	// 交易错误处理
	virtual void OnTradeError(int nErrorID, const char* pszMsg,int nRequestID = 0);

	// 结算单事件
public:
	// 查询结算单确认情况处理
	virtual void OnTradeQueryConfirmSettlement(bool bNeedConfirm);
	// 确认结算单成功
	virtual void OnTradeConfirmSettlementSuccess();
	// 确认结算单失败
	virtual void OnTradeConfirmSettlementFailed();
	// 查询结算单响应处理
	virtual void OnTradeQuerySettelment(tagSettlement* pstSettlement, bool bLast) {
	}
	virtual void OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	// 账户事件
public:
	// 查询资金账户响应事件处理
	virtual void OnTradeQueryTradingAccount(tagTradingAccount* pstTradingAccount, bool bLast);
	// 持仓事件
public:
	// 查询持仓响应事件处理
	virtual void OnTradeQueryPosition(tagPosition* pstPosition, bool bLast);
	// 查询持仓明细响应事件处理
	virtual void OnTradeQueryPositionDetail(tagPositionDetail* pstPositionDetail, bool bLast) {
	}

	// 报单事件
public:
	// 报单返回
	virtual void OnTradeRtnOrder(tagOrder* pstOrder);
	// 成交返回
	virtual void OnTradeRtnTrade(tagTrade* pstTrade);


	// 
public:
	// 查询合约响应事件处理
	virtual void OnTradeQueryInstrument(tagInstrument* pstINSTRUMENT1, bool bLast);
	// 查询报单响应事件处理
	virtual void OnTradeQueryOrder(tagOrder* pOrder, bool bLast) {
	}
	// 查询成交响应事件处理
	virtual void OnTradeQueryTrade(tagTrade* pTrade, bool bLast) {
	}


	// ITradeProtocolEvent impl - 结算单事件
public:
	// 确认结算单成功
	virtual void OnTradeSettlementConfirmSuccess() {
	}
	// 确认结算单失败
	virtual void OnTradeSettlementConfirmFailed() {
	}

protected:
	void QueryInstrumentDetail();
};



