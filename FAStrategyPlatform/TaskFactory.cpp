#include "TaskFactory.h"
#include "StrategyPlatform/Config.h"
#include "StrategyPlatform.h"

void  CTradeConnectTask::Handle()
{
	//执行
	gStrategyPlatrom->GetTrade()->Connect(CONFIG->GetTradeURL());
}


void  CQuoteConnectTask::Handle()
{
	//执行
	gStrategyPlatrom->GetQuote()->Connect(CONFIG->GetQuoteURL());
}


void  CLoginTask::Handle()
{
	//执行
	gStrategyPlatrom->GetTrade()->Login(CONFIG->GetBrokerID(), CONFIG->GetAccount(), CONFIG->GetPassword());
}

void  CSettlementTask::Handle()
{
	//执行
	gStrategyPlatrom->GetTrade()->QuerySettlement();
}

void  CConfirmSettlementTask::Handle()
{
	//执行
	gStrategyPlatrom->GetTrade()->ComfirmSettlement();
}

void  CInstrumentTask::Handle()
{
	//执行,全部合约查询
	gStrategyPlatrom->GetTrade()->QueryInstrumentDetail(0,0);
}


void  CAccountTask::Handle()
{
	//执行
	gStrategyPlatrom->GetTrade()->QueryAccountDetail();
}

void  CPositionTask::Handle()
{
	//执行
	gStrategyPlatrom->GetTrade()->QueryPosition();
}