#include "FATradeEvent.h"
#include "StrategyPlatform.h"
#include "dll_res.h"


CFATradeEvent::CFATradeEvent()
{
}


CFATradeEvent::~CFATradeEvent()
{
}

void CFATradeEvent::OnTradeConnected()
{
	gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_LOGIN));
	OUTPUT2(LOG_INFO, "链接TRADE成功\n");
	gStrategyPlatrom->GetFlow()->SetContinue(true);
}

void CFATradeEvent::OnTradeDisconnected(int nReason)
{
	gStrategyPlatrom->GetTrade()->OnDisconnect();
	OUTPUT2(LOG_INFO, "链接TRADE失败\n");
	gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_TRADE_CONNECT));
	gStrategyPlatrom->GetFlow()->SetContinue(true);
}
void CFATradeEvent::OnTradeUserLoginSuccess(int nSessionID, int nFrontID, int nOrderRef)
{
	gStrategyPlatrom->GetTrade()->OnLoginSuccess(nSessionID, nFrontID, nOrderRef);
	gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_SETTLEMENT));

	OUTPUT2(LOG_INFO, "登录TRADE成功，查询确认单\n");
	gStrategyPlatrom->GetFlow()->SetContinue(true);
}
void CFATradeEvent::OnTradeUserLoginFailed(int nErrorID, const char* pszMsg)
{
	gStrategyPlatrom->GetTrade()->OnLoginFail();
	OUTPUT2(LOG_INFO, "登录TRADE失败\n");
	gStrategyPlatrom->GetFlow()->SetContinue(false);

}

void CFATradeEvent::OnTradeRtnOrder(tagOrder* pstOrder)
{
	if (!pstOrder)
		return;

	gStrategyPlatrom->OnTradeRtnOrder(pstOrder);

	gStrategyPlatrom->GetFlow()->SetContinue(true);

	gStrategyPlatrom->m_orderMonitor.OnOrder(pstOrder);
}

void CFATradeEvent::OnTradeRtnTrade(tagTrade* pstTrade)
{
	if (!pstTrade)
		return;

	gStrategyPlatrom->OnTradeRtnTrade(pstTrade);
	//gStrategyPlatrom->GetFlow()->SetContinue(true);

	gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_POSITION));
	gStrategyPlatrom->GetFlow()->SetContinue(true);
}

void CFATradeEvent::OnTradeQueryConfirmSettlement(bool bNeedConfirm) 
{
	if (bNeedConfirm)
	{
		//gStrategyPlatrom->GetTrade()->ComfirmSettlement();
		gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_CONFIRM_SETTLEMENT));
		OUTPUT2(LOG_INFO, "确认确认单\n");
	}
	else
	{
		gStrategyPlatrom->m_bConfirmSettlement = true;
		//QueryInstrumentDetail();
		gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_INSTRUMENT));
		OUTPUT2(LOG_INFO, "确认单ok[1]\n");

	}
	
	//注册循环任务
	//gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_ACCOUNT));	//查询账户权益
	////gStrategyPlatrom->GetFlow()->RegisterPositionTask(FACTORY->Create(FLOW_POSITION));	//查询持仓
	//只查询一次
	//gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_POSITION));
	gStrategyPlatrom->GetFlow()->SetContinue(true);

}

void CFATradeEvent::QueryInstrumentDetail()
{
	//查询合约详细信息
	char *p[INS_MAX_COUNT] = { 0 };
	int nCount = 0;
	gStrategyPlatrom->GetIns(p, nCount);
	gStrategyPlatrom->GetTrade()->QueryInstrumentDetail(p, nCount);
}


void CFATradeEvent::OnTradeConfirmSettlementSuccess() 
{
	//结算单确认后，才可以下单
	gStrategyPlatrom->m_bConfirmSettlement = true;	
	//QueryInstrumentDetail();
	gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_INSTRUMENT));
	gStrategyPlatrom->GetFlow()->SetContinue(true);
	OUTPUT2(LOG_INFO, "确认单ok[2]\n");
}
void CFATradeEvent::OnTradeConfirmSettlementFailed()
{
	OUTPUT2(LOG_INFO, "确认单fail[1],再次请求\n");

	gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_CONFIRM_SETTLEMENT));
	gStrategyPlatrom->m_bConfirmSettlement = false;
	gStrategyPlatrom->GetFlow()->SetContinue(true);
}

void CFATradeEvent::OnTradeError(int nErrorID, const char* pszMsg,int nRequestID)
{
	if (nRequestID != 0)
	{
		gStrategyPlatrom->OnTradeError(nRequestID);
	}
	gStrategyPlatrom->GetFlow()->SetContinue(true);

	OUTPUT2(LOG_INFO, "错误ID：%d\n",nErrorID);

}

void CFATradeEvent::OnTradeQueryTradingAccount(tagTradingAccount* pstTradingAccount, bool bLast)
{
	//用户权益,通知到exe
	gStrategyPlatrom->OnAccountDetail(pstTradingAccount);


	//gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_ACCOUNT));
	//gStrategyPlatrom->GetFlow()->SetContinue(true);
}

void CFATradeEvent::OnTradeQueryPosition(tagPosition* pstPosition, bool bLast)
{
	//用户权益,通知到exe
	gStrategyPlatrom->OnPositionDetail(pstPosition,bLast);

	gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_POSITION));

	gStrategyPlatrom->GetFlow()->SetContinue(true);
}

void CFATradeEvent::OnTradeQueryInstrument(tagInstrument* pstINSTRUMENT1, bool bLast)
{
	//处理合约细节
	DLL->m_insManage->Init(pstINSTRUMENT1);

	if (bLast)
	{
		gStrategyPlatrom->GetFlow()->RegisterQueryTask(FACTORY->Create(FLOW_POSITION));
	}
	gStrategyPlatrom->GetFlow()->SetContinue(true);
}

void CFATradeEvent::OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (!pRspInfo)
	{
		return;
	}

	if (pRspInfo->ErrorID != 0)
	{
		OUTPUT2(LOG_ERROR, "确认单错误ID：%d-%s\n", pRspInfo->ErrorID, pRspInfo->ErrorMsg);
	}
	else
	{
		OUTPUT2(LOG_INFO, "确认单OK[3]\n");
	}
}