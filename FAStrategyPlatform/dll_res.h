#pragma once
#include "single.h"
#include "StrategyPlatform.h"
#include "dllHelper.h"
#include "FAMarket/FAMarket_def.h"
#include "FAPrint/TPrint_def.h"
#include "FAPositionAnalyze/IPosition.h"
#include "FAPositionMonitor/IMonitor.h"


typedef IFAQuote* (*CreateFAQuote)(IQuoteEvent*pEvent);
typedef int(*ReleaseFAQuote)();

typedef IFATrade* (*CreateFATrade)(ITradeEvent*pEvent);
typedef int(*ReleaseFATrade)();

typedef IStrategyPlus* (*CreateStrategyPlus)(IStrategyPlatform*pPlatform);
typedef int(*ReleaseStrategyPlus)();

typedef IFlow* (*CreateFlow)();
typedef int(*ReleaseFlow)();

typedef CInstrumentManage* (*CreateMarketRule)();
typedef int(*ReleaseMarketRule)();

typedef IYKPrint* (*CreatePrint)();
typedef void* (*ReleasePrint)();


typedef IAnalyze* (*CreatePositionAnalyze)();
typedef void* (*ReleasePositionAnalyze)();


typedef IMonitor* (*CreatePositionMonitor)();
typedef void* (*ReleasePositionMonitor)();

class CDllRes
{
public:
	CDllRes()
		:pfCreateQuote(0)
		, pfReleaseQuote(0)
		, pfCreateTrade(0)
		, pfReleaseTrade(0)
		, pfCreateFlow(0)
		, pfReleaseFlow(0)
		, pfCreateMarket(0)
		, pfReleaseMarket(0)
		, pfCreatePrint(0)
		, pfReleasePrint(0)
		, mDllQuote(new CDllHelper(_FTA("FAQuote.dll")))
		, mDllTrade(new CDllHelper(_FTA("FATrade.dll")))
		, mDllFlow(new CDllHelper(_FTA("FAFlow.dll")))
		, mDllMarket(new CDllHelper(_FTA("FAMarket.dll")))
		, mDllPrint(new CDllHelper(_FTA("FAPrint.dll")))
		, mDllPositionAnalyze(new CDllHelper(_FTA("FAPositionAnalyze.dll")))
	{
		Init();
	}
	~CDllRes()
	{
		UInit();
	}

	void Init()
	{
		if(!pfCreateQuote)
			pfCreateQuote = mDllQuote.get()->GetProcedure<CreateFAQuote>("CreateObject");
		if (!pfReleaseQuote)
			pfReleaseQuote = mDllQuote.get()->GetProcedure<ReleaseFAQuote>("ReleaseObject");

		if (!pfCreateTrade)
			pfCreateTrade = mDllTrade.get()->GetProcedure<CreateFATrade>("CreateObject");
		if (!pfReleaseTrade)
			pfReleaseTrade = mDllTrade.get()->GetProcedure<ReleaseFATrade>("ReleaseObject");

		if (!pfCreateFlow)
			pfCreateFlow = mDllFlow.get()->GetProcedure<CreateFlow>("CreateObject");
		if (!pfReleaseFlow)
			pfReleaseFlow = mDllFlow.get()->GetProcedure<ReleaseFlow>("ReleaseObject");

		if (!pfCreateMarket)
			pfCreateMarket = mDllMarket.get()->GetProcedure<CreateMarketRule>("CreateObject");
		if (!pfReleaseMarket)
			pfReleaseMarket = mDllMarket.get()->GetProcedure<ReleaseMarketRule>("ReleaseObject");

		if (!pfCreatePrint)
			pfCreatePrint = mDllPrint.get()->GetProcedure<CreatePrint>("CreateObject");
		if (!pfReleasePrint)
			pfReleasePrint = mDllPrint.get()->GetProcedure<ReleasePrint>("ReleaseObject");

		if (!pfCreatePosition)
			pfCreatePosition = mDllPositionAnalyze.get()->GetProcedure<CreatePositionAnalyze>("CreateObject");
		if (!pfReleasePosition)
			pfReleasePosition = mDllPositionAnalyze.get()->GetProcedure<ReleasePositionAnalyze>("CreateObject");


		if (pfCreateMarket)
			m_insManage = pfCreateMarket();

		if (pfCreatePrint && !m_print)
			m_print = pfCreatePrint();		
	}

	void UInit()
	{
		if (pfReleaseQuote)
			pfReleaseQuote();

		if (pfReleaseTrade)
			pfReleaseTrade();

		if (pfReleaseFlow)
			pfReleaseFlow();

		if (pfReleaseMarket)
			pfReleaseMarket();

		if (pfReleasePrint)
			pfReleasePrint();
		
		if (pfReleasePosition)
			pfReleasePosition();
	}
public:

	CDllHelperAuto mDllQuote;
	CDllHelperAuto mDllTrade;
	CDllHelperAuto mDllFlow;
	CDllHelperAuto mDllMarket;
	CDllHelperAuto mDllPrint;
	CDllHelperAuto mDllPositionAnalyze;	//��λ����(accredit)

	CreateFAQuote pfCreateQuote ;
	ReleaseFAQuote pfReleaseQuote;

	CreateFATrade pfCreateTrade;
	ReleaseFATrade pfReleaseTrade ;

	CreateFlow	pfCreateFlow ;
	ReleaseFlow	pfReleaseFlow;

	CreateMarketRule	pfCreateMarket ;
	ReleaseMarketRule	pfReleaseMarket;

	CreatePrint pfCreatePrint;
	ReleasePrint pfReleasePrint;


	CreatePositionAnalyze pfCreatePosition;
	ReleasePositionAnalyze pfReleasePosition;

public:
	IYKPrint * m_print;
	CInstrumentManage * m_insManage;

};

#define DLL singleton_t<CDllRes>::instance()