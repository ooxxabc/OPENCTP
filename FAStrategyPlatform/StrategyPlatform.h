/*
	策略平台底层
	1，存储tick数据，用于历史回测@sqlite
	2，加载xml文件中plus.dll
	3，多个策略执行AB商品下单，区分来源
*/
#pragma once

#include "FABase/common.h"
#include "StrategyPlatform/StrategyPlatform_Def.h"
#include "StrategyPlatform/Config.h"
#include "FAQuoteEvent.h"
#include "FATradeEvent.h"
#include <map>
#include "FABase/Lock.h"
#include "FAFlow/FAFlow_def.h"
#include "Factory.h"
//#include "AutoTradeObj.h"
#include "FAPositionAnalyze/IPosition.h"
#include "FAOrderMonitor/FAOrderMonitor.h"
#include "dllHelper.h"

//#ifdef _DEBUG
//#include "vld.h"
//#endif

#define  THREAD_REQ_LOOP_INTERVAL 1000

//#ifdef STRATEGYPLATFORM_EXPORTS
//#define STRATEGYPLATFORM_API __declspec(dllexport)
//#else
//#define STRATEGYPLATFORM_API __declspec(dllimport)
//#endif

class CDllHelper;

class CStrategyPlatform :public IStrategyPlatform{
public:
	CStrategyPlatform(void);
	~CStrategyPlatform(){}

public:
	IFAQuote * GetQuote(){ return m_pQuote; }
	IFATrade * GetTrade(){ return m_pTrade; }
	//StrategyPlatformConfig &GetCfg() { return m_cfg; }
	IFlow * GetFlow() { return m_pFlow; }
	
	LPSTRATEGYPLUS *	GetPlus(){ return m_plus; }
	
	int					GetPlusCount(){ return m_plusCount; }
	void				BroadCastQuote(tagMarketData *pData);

	char ** GetIns(char *p[],int &nCount);
	void AddPlusIns(char *p[], int &nCount);

public:
	virtual int Init();
	virtual int UnInit();


	//订阅
	virtual int SubInstrument(char**szIns, int &nCount);

	//下单
	virtual int Order(tagOrderDelegation &orderInsert,IPosition * pPos);
	virtual int OrderCancel(tagOrder*pOrder);
	//sqlite
	virtual int LoadTick(char**szIns, int &nCount, char *dateTime) { return SUCCESS; }
	virtual int PlayHistoryTick() { return SUCCESS; }

	virtual LPSTRATEGYPLUS  GetStrategy(char *szStrategy);


	//报单回调
	int OnTradeRtnTrade(tagTrade* pstTrade);
	int OnTradeRtnOrder(tagOrder* pstOrder);
	int OnTradeError(int nRequestID);

public:
	//通知到策略，账户权益，用于风险控制
	int OnAccountDetail(tagTradingAccount*pDetail);
	//通知策略，持仓信息
	int OnPositionDetail(tagPosition*pPos, bool bLast);


private:
	int		LoadConfig();
	int		ReLoadConfig();
	void	StorgeTick(tagMarketData *pData);		//存储行情
	
public:
	typedef map<string, LPSTRATEGYPLUS>mIStrategyInstance;
	typedef mIStrategyInstance::iterator mIStrategyInstanceIt;
private:
	IFAQuote * m_pQuote;
	IFATrade * m_pTrade;
	CFAQuoteEvent * m_pQuoteEvent;
	CFATradeEvent * m_pTradeEvent;
	IAnalyze*		m_pPositionAnalyze;

	//StrategyPlatformConfig	m_cfg;
	int						m_plusCount;
	LPSTRATEGYPLUS			m_plus[ARRAY_SIZE];
	mIStrategyInstance		m_mPlusInstance;

	CDllHelper		*	m_PlusDll[INS_MAX_COUNT];
	int					nPlusInsCount;
	char				szPlusIns[INS_MAX_COUNT][INS_ID_LENGTH];
	tagTradingAccount	m_pAccount;
	//CYKSemaphore		*m_semaQuit;

	IFlow		*		m_pFlow;

public:
	bool				m_bConfirmSettlement;
	CFAOrderMonitor		m_orderMonitor;

public:
	//Flow回调函数
	void FlowConnect(CTask* pFlow);
	void FlowLogin(CTask* pFlow);
	void FlowInstrument(CTask* pFlow);	//全行情合约规则查询
public:
	//virtual VPosition& GetPosition();
	//virtual void ClearPosition();

};

extern CStrategyPlatform * gStrategyPlatrom;

FA_API IStrategyPlatform * CreateObject(void);
FA_API void ReleaseObject(void);
