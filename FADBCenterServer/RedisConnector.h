#pragma once

#ifdef _MSC_VER
#include "../../../include/xRedis/src/xRedisClient.h"
#endif

#define MAX_CACHE_SIZE 16
#define CACHE_TYPE_WR 1
#define CACHE_TYPE_RD 2

#define OUTPUT printf 

unsigned int DCHash(const char *str);
#define DB_INDEX(dbi, key, cache_type)						\
	RedisDBIdx dbi(m_pRedisClient);							\
if (!dbi.CreateDBIndex(key, DCHash, cache_type))			\
{															\
	OUTPUT("DB_INDEX failed. key(%s) cache_type(%s)", \
	key, #cache_type);										\
	return 0;												\
}

#define DB_INDEX_RET(dbi, key, cache_type, ret)				\
	RedisDBIdx dbi(m_pRedisClient);							\
if (!dbi.CreateDBIndex(key, DCHash, cache_type))			\
{															\
	OUTPUT("DB_INDEX failed. key(%s) cache_type(%s)", \
	key, #cache_type);										\
	return ret;												\
}

#define DB_INDEX_NULL(dbi, key, cache_type)					\
	RedisDBIdx dbi(m_pRedisClient);							\
if (!dbi.CreateDBIndex(key, DCHash, cache_type))			\
{															\
	OUTPUT("DB_INDEX failed. key(%s) cache_type(%s)", \
	key, #cache_type);										\
	return;													\
}

class CRedisConnector
{
public:
	CRedisConnector();
	~CRedisConnector();

public:
	void Start();
	void Stop();
private:
	int				m_iCacheMum;
	RedisNode*		m_pWRRedisList;
	RedisNode*		m_pRDRedisList;
	xRedisClient*	m_pRedisClient;
};

