// StrategyArbitrageA.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "StrategyArbitrageA.h"


CStrategyArbitrageA * _pPlusA = NULL;

// 这是导出函数的一个示例。
FA_API IStrategyPlus* CreateObject(IStrategyPlatform*pPlatform)
{
	if (!_pPlusA)
	{
		_pPlusA = new CStrategyArbitrageA(pPlatform);
	}

	return _pPlusA;
}

FA_API int ReleaseObject(void)
{
	if (_pPlusA)
	{
		delete _pPlusA;
		_pPlusA = NULL;
	}

	return SUCCESS;
}


