#pragma once

/************************************************************************/
/* 

	标准套利模型(仿TB)

	参考UI: DOC/TB套利模型.png
	
*/
/************************************************************************/

#include "../inc/FABase/AutoPt.h"
#include "../inc/FABase/thread.h"
#include "../inc/FABase/safe_vector.h"

#define PLUS_CFG_XML	_FTA("StrategyPlus.xml")

typedef char		int_8;
typedef short int	int_16;
typedef int			int_32;

#define DEFAULT_MAX_POSITON		1	//最大仓位
#define DEFAULT_PER_HAND		1	//单笔数量
#define DEFAULT_ORDER_JUMP		1	//下单偏移N跳

//价差设置
struct tagSpread
{
	float	fLongOpen;				//多头开仓
	float	fLongCover;				//多头平仓
	float	fShortOpen;				//空头开仓
	float	fShortCover;			//空头平仓
};

//套利模型设置
struct tagConfig
{
	char		szInsA[INS_ID_LENGTH];
	char		szInsB[INS_ID_LENGTH];
	int_8		cPerHand;		//单笔数量
	int_8		cMaxPosition;	//最大仓位
	int_8		cOrderJum;		//下单偏移N跳
	float		fJump;			//1跳n点
	float		fPoingRMB;		//1点RMB
	float		fSlippage;		//滑点
	int_8		cCancelTime;	//撤单延时
	bool		bFar2Near;		//远期优先
	tagSpread	stSpread;		//价差设置
};

class CArbitrage;

class AStrategy :public IRunnable
{
public:
	AStrategy(CArbitrage*pA) :mpa(pA){}
	virtual int32_t operator()(const bool* isstopped, void* param = NULL);
	virtual ~AStrategy() { }
private:
	CArbitrage * mpa;
};

class CArbitrage
{
public:
	CArbitrage();
	~CArbitrage();
public:
	int_32 LoadConfig();
	tagConfig&GetConfig(){ return m_stConfig; }

	//添加价差到策略线程
	void AddSpread(float spread);

private:
	tagConfig m_stConfig;
	CYKAutoThread m_th;
	CYKSafeVector<float> m_v;	//spread of tick
};

