#pragma once
//#ifdef STRATEGYARBITRAGEA_EXPORTS
//#define STRATEGYARBITRAGEA_API __declspec(dllexport)
//#else
//#define STRATEGYARBITRAGEA_API __declspec(dllimport)
//#endif
//
#include "StrategyPlatform/StrategyPlatform_Def.h"

FA_API IStrategyPlus* CreateObject(IStrategyPlatform*pPlatform);
FA_API int ReleaseObject(void);