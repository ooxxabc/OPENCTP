#include "stdafx.h"
#include "StrategyArbitrageA.h"
#include "dllHelper.h"

//////////////////////////////////////////////////////////////////////////


CStrategyArbitrageA::CStrategyArbitrageA()
:m_pCore(0)
, m_pPlatform(0)
, m_print(0)
{
}

CStrategyArbitrageA::CStrategyArbitrageA(IStrategyPlatform*pPlatform)
:m_pPlatform(pPlatform)
, m_print(0)
, m_pCore(0)
{
}

int CStrategyArbitrageA::Init(IYKPrint *print,IPosition *pPos)
{
	m_print = print;
	m_tlb.LoadConfig();
	int count = 2;
	char *p[2] = { m_tlb.GetConfig().szInsA, m_tlb.GetConfig().szInsB};
	m_pPlatform->SubInstrument(p, count);
	return SUCCESS;

}

int CStrategyArbitrageA::UnInit()
{
	return SUCCESS;

}
int CStrategyArbitrageA::OnQuote(tagMarketData * pTick)
{
	if (strcmp(pTick->szINSTRUMENT,m_tlb.GetConfig().szInsA) == 0)
	{
	}
	else if (strcmp(pTick->szINSTRUMENT, m_tlb.GetConfig().szInsB) == 0)
	{
	}
	else
		return SUCCESS;

	//OUTPUT("[Quote]%s,%.1f\n", pTick->szINSTRUMENT, pTick->dLastPrice);
	if (m_print)
		m_print->Print(LOG_TRACE, "[Quote]%s,%.1f\n", pTick->szINSTRUMENT, pTick->dLastPrice);
	
	return SUCCESS;

}
int CStrategyArbitrageA::OnOrderStatus()
{
	return SUCCESS;

}
int CStrategyArbitrageA::OnTradeStatus()
{
	return SUCCESS;

}