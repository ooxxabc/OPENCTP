#include "stdafx.h"
#include "Arbitrage.h"
#include "tinyxml/tinyxml.h"

CArbitrage::CArbitrage()
:m_th(0)
{
}


CArbitrage::~CArbitrage()
{
}

int_32 CArbitrage::LoadConfig()
{

	TiXmlDocument xmlDoc;
	if (!xmlDoc.LoadFile(PLUS_CFG_XML, TIXML_ENCODING_UTF8))
		return FAIL;

	TiXmlElement * node = xmlDoc.FirstChildElement("StrategyArbitrageA");
	if (!node)
		return FATAL;

	TiXmlElement *node1 = node->FirstChildElement("Instrument");
	if (!node1)
		return FATAL;

	if (!node1->Attribute("num"))
		return FAIL;

	int_8 count = atoi(node1->Attribute("num"));

	if (count != 2)
		return FAIL;

	sprintf_s(m_stConfig.szInsA,INS_ID_LENGTH, node1->Attribute("Ins1"));
	sprintf_s(m_stConfig.szInsB,INS_ID_LENGTH, node1->Attribute("Ins2"));

	node1 = node->FirstChildElement("Spread");

	if (!node1)
		return FATAL;

	m_stConfig.stSpread.fLongOpen	= atof(node1->Attribute("longOpen"));
	m_stConfig.stSpread.fLongCover	= atof(node1->Attribute("longCover"));
	m_stConfig.stSpread.fShortOpen	= atof(node1->Attribute("shortOpen"));
	m_stConfig.stSpread.fShortCover	= atof(node1->Attribute("shortCover"));
	
	node1 = node->FirstChildElement("Open");

	if (!node1)
		return FATAL;

	m_stConfig.cPerHand = atoi(node1->Attribute("position"));
	m_stConfig.cMaxPosition= atof(node1->Attribute("maxPosition"));
	m_stConfig.fJump = atof(node1->Attribute("jump"));
	m_stConfig.fPoingRMB = atof(node1->Attribute("PointRMB"));
	m_stConfig.fSlippage = atof(node1->Attribute("Slippage"));
	m_stConfig.cCancelTime = atoi(node1->Attribute("CancelTime"));
	m_stConfig.bFar2Near = atoi(node1->Attribute("far2Near"));

	return SUCCESS;
}