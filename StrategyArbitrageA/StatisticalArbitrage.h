#pragma once
#include "../inc/single.h"
#include "../inc/FAStrategyCore/FAStrategyBase.h"
#include <map>
using namespace std;

/************************************************************************/
/* 
	统计套利
	1.协整
	2.升贴水
	3.
*/
/************************************************************************/

class CStatisticalArbitrage
{
public:
	CStatisticalArbitrage();
	~CStatisticalArbitrage();
};

/*
	价差套利
	1.基于结算价
*/
class CSpreadArbitrage
{
public:

	enum enmORDER
	{
		OPenLS = 0,
		OPenSL,
		CoverLS,
		CoverSL,
	};

	enum enmBOOK
	{
		LAST = 0,
		ASK,
		BID,
	};

	struct stHistory
	{
		float	price;
		int		vol_long;
		int		vol_short;
	};

	typedef map<float, stHistory> mHistory;
	typedef mHistory::iterator mHistoryIter;

public:
	//最新价动态价差
	void OnDynamicSpread(enmBOOK b,float sp);

	//突破和假突破

private:
	//入场条件
	float m_fEnterSpread;
	//出场条件:止盈止损
	float m_fLeaveWin;
	float m_fLeaveLose;
	////成交历史
	//mHistory m_mHistory;
	//分钟K
	CFAVector<stHistory> HistoryBar;
};

#define AST singleton_t<CStatisticalArbitrage>::instance() 
#define ASP singleton_t<CSpreadArbitrage>::instance() 