#pragma once
/************************************************************************/
/* 
	下单中心(撤单)
*/
/************************************************************************/

#include "../inc/single.h"

class CYKOrder
{
public:
	CYKOrder();
	~CYKOrder();
};

#define ORDER singleton_t<CYKOrder>::instance()