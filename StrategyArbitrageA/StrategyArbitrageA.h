// 下列 ifdef 块是创建使从 DLL 导出更简单的
// 宏的标准方法。此 DLL 中的所有文件都是用命令行上定义的 STRATEGYARBITRAGEA_EXPORTS
// 符号编译的。在使用此 DLL 的
// 任何其他项目上不应定义此符号。这样，源文件中包含此文件的任何其他项目都会将
// STRATEGYARBITRAGEA_API 函数视为是从 DLL 导入的，而此 DLL 则将用此宏定义的
// 符号视为是被导出的。
#pragma once

#include "Arbitrage.h"
#include "FAPrint/TPrint_def.h"
#include "StrategyArbitrageA_Def.h"

// 此类是从 StrategyArbitrageA.dll 导出的
class CStrategyArbitrageA :public IStrategyPlus{
public:
	CStrategyArbitrageA(void);
	CStrategyArbitrageA(IStrategyPlatform*pPlatform);
	// TODO:  在此添加您的方法。
public:
public:
	virtual int Init(IYKPrint *p=0,IPosition *pPos=0);
	virtual int UnInit();
	virtual int OnQuote(tagMarketData * pTick);
	virtual int OnOrderStatus();
	virtual int OnTradeStatus();

private:
	IStrategyPlatform * m_pPlatform;
	IStrategyCore * m_pCore;
private:
	CArbitrage m_tlb;
	IYKPrint * m_print;

};


