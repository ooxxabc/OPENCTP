#include "FALog/ELog.h" 
#include "FALog/mutex.h"

static CLock _Mutex;

#define LOG2FILE		//��¼���ļ�
#define LOG_FILE ("ykit.log")

/**
* EasyLogger port initialize
*
* @return result
*/
ElogErrCode elog_port_init(void) {
	ElogErrCode result = ELOG_NO_ERR;

#ifndef WIN32 
	pthread_mutex_init(&output_lock, NULL);
#else 

#endif 

	return result;
}

/**
* output log port interface
*
* @param log output of log
* @param size log size
*/
void elog_port_output(const char *log, size_t size) {
	/* output to terminal */

#ifdef LOG2FILE

	FILE *fp=NULL;
	fp = fopen(LOG_FILE, "a+");
	if (fp)
	{
		fprintf(fp, "%s", log);
		fclose(fp);
	}
#else
	printf("%.*s", size, log);

#endif
}

/**
* output lock
*/
void elog_port_output_lock(void) {
#ifndef WIN32 
	pthread_mutex_lock(&output_lock);
#else
	_Mutex.Lock();
#endif 
}

/**
* output unlock
*/
void elog_port_output_unlock(void) {
#ifndef WIN32 
	pthread_mutex_unlock(&output_lock);
#else
	_Mutex.Unlock();
#endif 
}


/**
* get current time interface
*
* @return current time
*/
const char *elog_port_get_time(void) {
	static char cur_system_time[24] = { 0 };
	static SYSTEMTIME currTime;

	GetLocalTime(&currTime);
	sprintf_s(cur_system_time, 24, "%02d-%02d %02d:%02d:%02d.%03d", currTime.wMonth, currTime.wDay,
		currTime.wHour, currTime.wMinute, currTime.wSecond, currTime.wMilliseconds);

	return cur_system_time;
}

/**
* get current process name interface
*
* @return current process name
*/
const char *elog_port_get_p_info(void) {
	static char cur_process_info[10] = { 0 };

	sprintf_s(cur_process_info, 10, "pid:%04ld", GetCurrentProcessId());

	return cur_process_info;
}

/**
* get current thread name interface
*
* @return current thread name
*/
const char *elog_port_get_t_info(void) {
	static char cur_thread_info[10] = { 0 };

	sprintf_s(cur_thread_info, 10, "tid:%04ld", GetCurrentThreadId());

	return cur_thread_info;
}
