// FALog.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "FALog.h"


// 这是已导出类的构造函数。
// 有关类定义的信息，请参阅 FALog.h
CFALog::CFALog()
{
	return;
}

// 这是导出函数的一个示例。
FALOG_API int CreateObject(void)
{
	return 42;
}

FALOG_API int ReleaseObject(void)
{
	return 42;
}