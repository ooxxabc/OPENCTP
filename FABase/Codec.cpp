#include "FABase/Codec.h"

// 编码int8
int32_t FACodec::encode_int8(char** out_ptr, uint8_t src)
{
	if (out_ptr == NULL || *out_ptr == NULL) return 0;

	**out_ptr = (char)src;
	(*out_ptr)++;

	return int32_t(sizeof(uint8_t));
}

// 解码int8
int32_t FACodec::decode_int8(char** in_ptr, uint8_t* out_ptr)
{
	if (in_ptr == NULL || *in_ptr == NULL || out_ptr == NULL) return 0;

	*out_ptr = **in_ptr;
	(*in_ptr)++;

	return int32_t(sizeof(uint8_t));
}

// 编码int16
int32_t FACodec::encode_int16(char** out_ptr, uint16_t src)
{
	if (out_ptr == NULL || *out_ptr == NULL) return 0;

	**out_ptr = char((src & 0xff00) >> 8);
	(*out_ptr)++;

	**out_ptr = char(src & 0xff);
	(*out_ptr)++;

	return int32_t(sizeof(uint16_t));
}


// 解码int16
int32_t FACodec::decode_int16(char** in_ptr, uint16_t* out_ptr)
{
	if (in_ptr == NULL || *in_ptr == NULL || out_ptr == NULL) return 0;

	*out_ptr = (unsigned char)**in_ptr;
	*out_ptr <<= 8;
	(*in_ptr)++;
	*out_ptr += (unsigned char)**in_ptr;
	(*in_ptr)++;

	return int32_t(sizeof(uint16_t));
}


// 编码int32
int32_t FACodec::encode_int32(char** out_ptr, uint32_t src)
{
	if (out_ptr == NULL || *out_ptr == NULL) return 0;

	**out_ptr = (char)((src & 0xff000000) >> 24);
	(*out_ptr)++;
	**out_ptr = (char)((src & 0xff0000) >> 16);
	(*out_ptr)++;
	**out_ptr = (char)((src & 0xff00) >> 8);
	(*out_ptr)++;
	**out_ptr = (char)(src & 0xff);
	(*out_ptr)++;

	return int32_t(sizeof(uint32_t));
}

// 解码int32
int32_t FACodec::decode_int32(char** in_ptr, uint32_t* out_ptr)
{
	if (in_ptr == NULL || *in_ptr == NULL || out_ptr == NULL) return 0;

	*out_ptr = (unsigned char)**in_ptr;
	*out_ptr <<= 8;
	(*in_ptr)++;

	*out_ptr += (unsigned char)**in_ptr;
	*out_ptr <<= 8;
	(*in_ptr)++;

	*out_ptr += (unsigned char)**in_ptr;
	*out_ptr <<= 8;
	(*in_ptr)++;

	*out_ptr += (unsigned char)**in_ptr;
	(*in_ptr)++;

	return int32_t(sizeof(uint32_t));
}

// 编码int64
int32_t FACodec::encode_int64(char** out_ptr, uint64_t src)
{
	if (out_ptr == NULL || *out_ptr == NULL) return 0;

		// high bits
	uint32_t part = uint32_t((src >> 32) & 0xffffffff);
	uint32_t tmp =  htonl(part);
	memcpy((void*)*out_ptr, (const void*)&tmp, sizeof(tmp));
	*out_ptr += sizeof(tmp);

		// low bits
	part = uint32_t(src & 0xffffffff);
	tmp =  htonl((uint32_t)part);
	memcpy((void*)*out_ptr, (const void*)&tmp, sizeof(tmp));
	*out_ptr += sizeof(tmp);

	return int32_t(sizeof(uint64_t));
}

// 解码int64
int32_t FACodec::decode_int64(char** in_ptr, uint64_t* out_ptr)
{
	if (in_ptr == NULL || *in_ptr == NULL || out_ptr == NULL) return 0;

	uint32_t part = 0;
	uint32_t tmp = 0;

		// high bits
	memcpy((void*)&part, (const void*)*in_ptr, sizeof(part));
	tmp = ntohl(part);
	*in_ptr += sizeof(part);

	*out_ptr = (uint64_t)tmp << 32;	

		// low bits
	memcpy((void*)&part, (const void*)*in_ptr, sizeof(part));
	tmp = ntohl(part);
	*in_ptr += sizeof(part);

	*out_ptr += tmp;

	return int32_t(sizeof(uint64_t));
}

// 编码string(把'\0'也进行了编码，字符串长度包含'\0')
int32_t FACodec::encode_string(char** out_ptr, const char* src_ptr, const int16_t max_string_len)
{
	if (out_ptr == NULL || *out_ptr == NULL || src_ptr == NULL || max_string_len <= 0)
	{
		return 0;
	}

	int16_t tmp_string_length = (int16_t)strlen(src_ptr);
	if (tmp_string_length != 0)
	{
		tmp_string_length += 1;
	}
	
	if (tmp_string_length  > max_string_len)
	{
		tmp_string_length = max_string_len;
	}

		// 首先编入字符串的长度
	int32_t code_len = encode_int16(out_ptr, tmp_string_length);
	if (tmp_string_length == 0) return code_len;

	strncpy(*out_ptr, src_ptr, tmp_string_length);
	(*out_ptr)[tmp_string_length - 1] = '\0';			//保证编码进去的string一定以'\0'结尾
	*out_ptr += (tmp_string_length);

	return (code_len + tmp_string_length);
}

// 解码string
int32_t FACodec::decode_string(char** in_ptr, char* out_ptr, const int16_t max_string_len)
{
	if (in_ptr == NULL || *in_ptr == NULL || out_ptr == NULL || max_string_len <= 0)
	{
		return 0;
	}

	int16_t string_length = 0;
	int32_t tmp_length = decode_int16(in_ptr, (uint16_t*)&string_length);
	if (string_length <= 0)
	{
		out_ptr[0] = '\0';
		return tmp_length;
	}

	int16_t real_length = string_length;
	if (string_length > max_string_len)
	{
		real_length = max_string_len;
	}

	strncpy(out_ptr, (*in_ptr), real_length);
	
	// 不能只移动real_length大小
	*in_ptr += string_length;
	out_ptr[real_length - 1] = '\0';

	return (string_length + tmp_length);
}

// 编码内存块
int32_t FACodec::encode_memory(char** out_ptr, char* src_ptr, const int32_t mem_size)
{
	if (out_ptr == NULL || *out_ptr == NULL || src_ptr == NULL || mem_size <= 0)
	{
		return 0;
	}

	memcpy((void*)*out_ptr, (const void*)src_ptr, mem_size);
	*out_ptr += mem_size;

	return mem_size;
}

// 解码内存块
int32_t FACodec::decode_memory(char** in_ptr, char* out_ptr, const int32_t mem_size)
{
	if (in_ptr == NULL || *in_ptr == NULL || out_ptr == NULL || mem_size <= 0)
	{
		return 0;
	}

	memcpy((void*)out_ptr, (const void*)*in_ptr, mem_size);
	*in_ptr += mem_size;

	return mem_size;
}

