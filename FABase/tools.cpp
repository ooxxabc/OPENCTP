#include "FABase/tools.h"

#ifdef WIN32

#include "windows.h"

bool GetLocalTime(tagYKTime* dt)
{
	SYSTEMTIME st;

	GetLocalTime(&st);

	dt->nYear = st.wYear;
	dt->nMonth = st.wMonth;
	dt->nDay = st.wDay;
	dt->nHour = st.wHour;
	dt->nMinute = st.wMinute;
	dt->nSecond = st.wSecond;
	dt->nMilliseconds = st.wMilliseconds;
	dt->nDayOfWeek = st.wDayOfWeek;

	return true;
}
#else
bool GetLocalTime(tagYKTime* dt)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	tm* pt = localtime(&tv.tv_sec);

	dt->nYear = pt->tm_year + 1900;
	dt->nMonth = pt->tm_mon + 1;
	dt->nDay = pt->tm_mday;
	dt->nHour = pt->tm_hour;
	dt->nMinute = pt->tm_min;
	dt->nSecond = pt->tm_sec;
	dt->nMilliseconds = tv.tv_usec / 1000;
	dt->nDayOfWeek = pt->tm_wday;

	return true;
}
#endif

void CTools::GetTime(tm&tmtemp)
{
	tagYKTime pt;
	GetLocalTime(&pt);
	tmtemp.tm_year = pt.nYear - 1900;
	tmtemp.tm_mon = pt.nMonth - 1;
	tmtemp.tm_mday = pt.nDay;
	tmtemp.tm_hour = pt.nHour;
	tmtemp.tm_min = pt.nMinute;
	tmtemp.tm_sec = pt.nSecond;
	tmtemp.tm_isdst = 0;
	tmtemp.tm_wday = 0;
	tmtemp.tm_yday = 0;
}

time_t CTools::GetTime()
{
	tagYKTime pt;
	GetLocalTime(&pt);

	tm tmtemp;
	tmtemp.tm_year = pt.nYear - 1900;
	tmtemp.tm_mon = pt.nMonth - 1;
	tmtemp.tm_mday = pt.nDay;
	tmtemp.tm_hour = pt.nHour;
	tmtemp.tm_min = pt.nMinute;
	tmtemp.tm_sec = pt.nSecond;
	tmtemp.tm_isdst = 0;
	tmtemp.tm_wday = 0;
	tmtemp.tm_yday = 0;

	time_t convtime;
	convtime = mktime(&tmtemp);

	return convtime;
}

bool CTools::IsDigital(char c)
{
	if(c>='0' && c<='9')
		return true;

	return false;

}

bool CTools::IsCharacter(char c)
{
	if(c>='a' && c<='z')
		return true;

	if(c>='A' && c<='Z')
		return true;
	
	return false;
}