#pragma once
#include "FAStrategyCore/FAStrategyBase.h"

/************************************************************************/
/* 
	加载历史行情，标准接口
*/
/************************************************************************/

class IDB
{
public:
	virtual bool Load() = 0;
};
