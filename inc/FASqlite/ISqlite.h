#pragma once

enum
{
	READ_ONLY = 1,
	READ_WRITE = 2,
	CREATE = 4,
};


typedef int (*YKSQLITE_CALLBACK)(void*,int,char**, char**);
struct tagMarketData;

class ISqlite
{
public:
	virtual bool	Open(char* lpszFileName,int nFlag) = 0;
	virtual bool	IsOpen() = 0;
	virtual bool	Close() = 0;
	virtual bool	Execute(char* lpszSQL, YKSQLITE_CALLBACK pCallback, void *pUnuse, int &nErrCode) = 0;
	virtual char*	GetLastError() = 0;
	virtual int		GetLastErrorCode() = 0;
};


class ISqliteHandle
{
public:
	virtual void OnErr(const char *szErrCode) = 0;
};
