#pragma once
#include <vector>
#include <map>
#include "ctpImpl/Trade_def.h"

/*
	单账户多策略的仓位管理模块	
*/

using namespace std;


//#ifdef FA_EXPORTS
//#define FA_API __declspec(dllexport)
//#else
//#define FA_API __declspec(dllimport)
//#endif
//
typedef vector<tagOrder> vOrder;
typedef vector<tagTrade> vTrade;
typedef vector<tagPosition> vPosition;

class CStrategyID
{
public:

	int nID;
	char szName[33];

	CStrategyID()
	{

	}

	CStrategyID(int id,char *sz)
	{
		nID = id;
		sprintf_s(szName,33,"%s",sz);
	}

	bool operator==(const CStrategyID&tA)const
	{
		if (tA.nID == nID && strcmp(tA.szName,szName) ==0)
		{
			return true;
		}

		return false;
	}

	bool operator<(const CStrategyID&tA)const
	{
		return nID < tA.nID;
	}
};

class IPosition
{
public:
	virtual void OnOrder(tagOrder*pOrder) = 0;
	virtual void OnTrade(tagTrade*pTrade) = 0;
	virtual void OnPosition(tagPosition*pPos) = 0;
};


typedef map<CStrategyID,IPosition*> mStrategyPosition;
typedef mStrategyPosition::iterator mStrategyPositionIter;

class IAnalyze
{
public:
	virtual IPosition* RegStrategy(CStrategyID& stID) = 0;		//策略名唯一，如冲突不计后面加入
	virtual void GetPosition(CStrategyID &stID,vPosition &vPos) = 0;

};
