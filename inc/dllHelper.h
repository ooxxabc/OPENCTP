#pragma once
#include <stdio.h> 
#include <string>
#include "FABase/AutoPt.h"
#ifdef WIN32
#include "windows.h"

	#ifdef FA_EXPORTS
	#define FA_API __declspec(dllexport)
	#else
	#define FA_API __declspec(dllimport)
	#endif

#else
	#include <dlfcn.h>
	#define FA_API 
#endif

#define DLL_FILE_LEN	256

// 动态库辅助类
class CDllHelper:public CYKCounter
{
protected:
	char					m_szFileName[DLL_FILE_LEN];                  // 文件名
#ifdef WIN32
    HMODULE					m_hLibrary;                    // 模块句柄
#else
	void *					m_hLibrary;
#endif
public:
	CDllHelper(const char *lpszFileName) :
		m_hLibrary(NULL)
	{
		sprintf_s(m_szFileName, DLL_FILE_LEN, lpszFileName);
		Load();
	}
	~CDllHelper(){
		if (m_hLibrary != NULL)
		{
#ifdef WIN32
			::FreeLibrary(m_hLibrary);
#else
			dlclose(m_hLibrary);
#endif
			m_hLibrary = NULL;
		}
	}

public:
    // 获取Proc
	template<typename Proc>
	Proc GetProcedure(const char* lpszProc)
	{
		Load();

        Proc pfnProc = NULL;
#ifdef WIN32
		if (m_hLibrary != NULL)
			pfnProc = (Proc)::GetProcAddress(m_hLibrary, lpszProc);
#else
		if (m_hLibrary != NULL)
			pfnProc = (Proc)dlsym(m_hLibrary, lpszProc);
#endif
		return pfnProc;
	}

    // 加载动态库
	void Load()
	{
		if (m_hLibrary != NULL) return;

#ifdef WIN32
		char szPath[MAX_PATH] = { 0 };

		HMODULE hModule = ::GetModuleHandle(NULL);
		if (hModule != NULL)
		{
			DWORD dwLen = GetModuleFileNameA(hModule, szPath, MAX_PATH);
			if (dwLen != 0)
			{
				char szDrive[_MAX_DRIVE];
				char szDirectory[_MAX_DIR];

				_splitpath_s(szPath, szDrive, _MAX_DRIVE, szDirectory, _MAX_DIR, NULL, 0, NULL, 0);
				_makepath_s(szPath, MAX_PATH, szDrive, szDirectory, NULL, NULL);

				strcat_s(szPath, MAX_PATH, m_szFileName);
				m_hLibrary = ::LoadLibraryA(szPath);
				if(!m_hLibrary)
				{
					printf("[Err][%s][%s]%d\n", m_szFileName,__FUNCTION__, OCGetLastError());
				}
			}
		}
#else
		m_hLibrary = dlopen(m_szFileName, RTLD_LAZY);
		if (!m_hLibrary)
		{
			printf("[Err][%s][%s]%d\n", m_szFileName, __FUNCTION__, OCGetLastError());
		}
#endif
		
	}
    // 是否已经加载动态库
	bool IsLoaded()
	{
		return m_hLibrary != NULL;
	}
};


typedef CYKAutoPoint<CDllHelper> CDllHelperAuto;
