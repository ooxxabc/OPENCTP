#pragma once

/************************************************************************/
/* 
	波动率：vix 和 GVIX

	vix: 股价的波动率即股价在单位时间内连续复利收益率的标准差。
*/
/************************************************************************/

enum
{
	TRADE_DAY = 1,
	TRADE_WEEK = 5,
	TRADE_MONTH = 22,
	TRADE_YEAR = 252,
};

class CFAVIX
{
public:

};

class CFAGVIX
{
public:
	CFAGVIX();
	~CFAGVIX();
};

