#pragma once
#include "FAIndex.h"

class CFAMACD:public CFAIndex
{
public:
	CFAMACD();
	virtual ~CFAMACD(){}
public:
	virtual void OnBar(CFABar *p, IIndexImpl*pIndex);
	virtual void OnTick(tagMarketData *p, IIndexImpl*pIndex);

protected:
	// �洢short EMA����
	CFAVector<double> m_EMAs_short;

	// �洢long EMA����
	CFAVector<double> m_EMAs_long;

	// �洢DIFF����
	CFAVector<double> m_DIFFs;

	// �洢DEA����
	CFAVector<double> m_DEAs;

	// �洢MACD����
	CFAVector<double> m_MACDs;

private:
	int m_short;  // ����
	int m_long;   // ����
	int m_m;      // mֵ
};