#pragma once
#include "FAIndex.h"

class CFARSI:public CFAIndex
{
public:
	CFARSI();
	virtual ~CFARSI(){}
public:
	virtual void OnBar(CFABar *p, IIndexImpl*pIndex);
	virtual void OnTick(tagMarketData *p, IIndexImpl*pIndex);

protected:

	// �洢RSI1����
	CFAVector<double> m_RSI1;

	// �洢RSI1�ĸ���U����
	CFAVector<double> m_U1;

	// �洢RSI1�ĸ���D����
	CFAVector<double> m_D1;

	// �洢RSI2����
	CFAVector<double> m_RSI2;

	// �洢RSI1�ĸ���U����
	CFAVector<double> m_U2;

	// �洢RSI1�ĸ���D����
	CFAVector<double> m_D2;

private:
	int m_long_val;
	int m_short_val;
};