#pragma once
#include "FAIndex.h"

/************************************************************************/
/* KDJ                                                                  */
/************************************************************************/
class CFAKDJ :public CFAIndex
{
public:
	CFAKDJ();
	virtual ~CFAKDJ(){}
public:
	virtual void OnBar(CFABar *p, IIndexImpl*pIndex);
	virtual void OnTick(tagMarketData *p, IIndexImpl*pIndex);

protected:
	// �洢K����
	CFAVector<double> m_K;

	// �洢D����
	CFAVector<double> m_D;

	// �洢J����
	CFAVector<double> m_J;

	// �洢RSV����(δ�������ֵ)
	CFAVector<double> m_RSV;
private:
	int m_length;
	int m_slowLength;
	int m_smoothLength;
};