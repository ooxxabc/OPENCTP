#pragma once
#include <string>
#include "FAStrategyBase.h"

class IStrategy
{
public:

	// ************************ 操作 ************************ 
	/*
		price：价格；unit：手数；slipNum：滑点
	*/

	//买入开仓
	virtual COrderKey Buy(double price, int unit = 1, int slipNum = 0) = 0;

	//卖出开仓
	virtual COrderKey SellShort(double price, int unit = 1, int slipNum = 0) = 0;

	//卖出平仓
	virtual COrderKey Sell(double price, int unit = 1, int slipNum = 0) = 0;

	 //买入平仓
	virtual COrderKey BuyToCover(double price, int unit = 1, int slipNum = 0) = 0;
	
	// 在某时间段内出场
	virtual void StopLoss(std::string et, std::string end, int volume = 0) = 0;

	// ************************ 回报 ************************ 

	///< 有tick数据到来
	virtual void OnTick() = 0;

	///< 一根bar已经形成
	virtual void OnBar() = 0;

	///< 一根bar开始形成
	virtual void OnBarOpen() = 0;

	///< 委托成功
	virtual void OnOrder(CThostFtdcOrderField* pRtnOrder) = 0;

	///< 成交
	virtual void OnTrade(CThostFtdcTradeField *pTrade) = 0;

	// 撤单
	virtual void OnRspOrderAction(CThostFtdcOrderField *pOrder) = 0;
};