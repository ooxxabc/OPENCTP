#pragma once
#include "FAIndex.h"

/*
ATR平均真实波幅（ATR）的计算方法：
1、当前交易日的最高价与最低价间的波幅
2、前一交易日收盘价与当个交易日最高价间的波幅
3、前一交易日收盘价与当个交易日最低价间的波幅

TR : MAX(MAX((HIGH-LOW),ABS(REF(CLOSE,1)-HIGH)),ABS(REF(CLOSE,1)-LOW));   
ATR : MA(TR,N)

*/

class CFAATR :public CFAIndex
{
public:
	CFAATR();
	~CFAATR();
	virtual void OnBar(CFABar *p, IIndexImpl*pIndex);
	virtual void OnTick(tagMarketData *p, IIndexImpl*pIndex);
private:
	// TR序列
	CFAVector<double> m_TR;
	//ATR
	CFAVector<double> m_ATR;
};