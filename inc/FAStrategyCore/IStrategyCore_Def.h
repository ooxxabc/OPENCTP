#pragma once
#include "FAStrategyBase.h"
#include "dllHelper.h"

class IStrategyCore
{
public:
	virtual void OnBar(CFABar *p) = 0;
	virtual void OnTick(tagMarketData *p) = 0;

};

class IIndexImpl
{
public:
	virtual void OnKDJ(BAR_KDJ &k, BAR_KDJ&d, BAR_KDJ&j) = 0;
	virtual void OnRSI(BAR_RSI &r1,BAR_RSI &r2) = 0;
	virtual void OnMACD(BAR_MACD &m) = 0;
	virtual void OnATR(BAR_ATR&m) {}
};

//#ifdef FASTRATEGYCORE_EXPORTS
//#define FASTRATEGYCORE_API __declspec(dllexport)
//#else
//#define FASTRATEGYCORE_API __declspec(dllimport)
//#endif
//
//
FA_API IStrategyCore* CreateObject(IIndexImpl*p);
FA_API int ReleaseObject(IStrategyCore*p);
