#pragma once
#include "FAIndex.h"

/************************************************************************/
/* 
MID:MA(CLOSE,N);//求N个周期的收盘价均线，称为布林通道中轨
TMP2:=STD(CLOSE,M);//求M个周期内的收盘价的标准差
TOP:MID+P*TMP2;//布林通道上轨
BOTTOM:MID-P*TMP2;//布林通道下轨

N:26
M:26
P:2
*/
/************************************************************************/

enum
{
	TRADE_DAY = 1,
	TRADE_WEEK = 5,
	TRADE_MONTH = 22,
	TRADE_YEAR = 252,
};
struct tagBollValue
{
	double dUp;
	double dMid;
	double dDown;
};

struct tagBollCfg
{
	int M;
	int N;
	int P;
};

class CFABoll :public CFAIndex
{
public:
	CFABoll();
	CFABoll(tagBollCfg &cfg);
	~CFABoll();
public:
	virtual void OnBar(CFABar *p, IIndexImpl*pIndex);
private:
	CFAVector<tagBollValue> m_bollValues;
	tagBollCfg				m_cfg;
};

