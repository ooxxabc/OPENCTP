#pragma once
#include "ctpImpl/Quote_def.h"
#include "FAStrategyCore/FAStrategyBase.h"
#include "FABase/thread.h"



#define	MIN_1	1
#define	MIN_5	5
#define	MIN_15	15
#define	MIN_30	30

struct tagTick
{
	tagMarketData data;
	int tickcount;
};

typedef CFAVector<tagTick> VTick;


class IBarCreator
{
public:
	virtual void OnCreateBar(CFABar *p) = 0;
};

class FABarMaker
{
public:
	FABarMaker();
	~FABarMaker(){}
public:
	//周期单位minute
	void Init(IBarCreator*pCreator, int nRound);
	void OnTick(tagMarketData * p);
public:
	static void * _run(const bool * isstop, void *param);
	int CreateLastBar(CFABar &bar);
protected:
	int CreateBar(CFABar &bar);
	double Lowest(VTick &t, int len);
	double Highest(VTick &t, int len);
	int Volumeest(VTick &t, int len);
private:
	VTick	m_vTick;
	IBarCreator * m_pCreator;
	int		m_nRound;		//周期(minute)
	CYKThread * m_pTH;
	int		m_nLastCreateBarTime;
};
