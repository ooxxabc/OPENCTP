#pragma once
#include <stdio.h>
#include <vector>
#include <string.h>
using namespace std;

#define STR_TYPE_LEN 33
#define STR_CONTENT_LEN 128
#define STR_SQL_LEN 1024

struct tagResult
{
	char rowType[STR_TYPE_LEN];
	char rowValue[STR_CONTENT_LEN];

	tagResult(){}
	tagResult(char*p1,char*p2)
	{
		strncpy(rowType,p1, STR_TYPE_LEN);
		strncpy(rowValue,p2, STR_CONTENT_LEN);
	}
};

typedef vector<tagResult>VResult;
typedef VResult::iterator VResultIter;

class CArchiveResult
{
public:
	CArchiveResult(){}
	~CArchiveResult(){}

	void operator<<(const tagResult&r1)
	{
		m_result.push_back(r1);
	}
	void operator>>(tagResult&r1)
	{
		VResultIter it = m_result.begin();
		strncpy(r1.rowType,it->rowType, STR_TYPE_LEN);
		strncpy(r1.rowValue,it->rowValue, STR_CONTENT_LEN);
		m_result.erase(it);
	}
	tagResult&operator[](int index)
	{
		return m_result[index];
	}
	

	int Size(){return m_result.size();}
protected:
private:
	VResult m_result;
};

//#ifndef FAMYSQL_EXPORTS
//#define MYSQL_API __declspec(dllimport)
//#else
//#define MYSQL_API __declspec(dllexport)
//#endif

struct tagMarketData;

class IMysql
{
public:
	virtual void Storge(tagMarketData*pData) = 0;

	virtual void Start() = 0;
	virtual void Stop() = 0;
};

class IMysqlHandle
{
public:
	virtual void OnErr(const char *szErrCode) = 0;
};