#pragma once

/************************************************************************/
/* 
	从db读取数据，500ms一条；模拟行情发送
*/
/************************************************************************/

class tagMarketData;

class IBTQuote
{
public:
	virtual void OnBTQuote(tagMarketData * p) = 0;
};

class CFABackTest
{
public:
	void Init(IBTQuote*p,char *tickTable);
	void Start();
protected:
private:
	IBTQuote * m_quote;
};