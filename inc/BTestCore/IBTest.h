#pragma once

/*
	BTestCore模块是用于回测核心库
*/

class IBTestPlatform
{
public:
	virtual void Init() = 0;
	virtual void UnInit() = 0;
};


/************************************************************************/
/* 回测接口																*/
/************************************************************************/
class IBTest
{

};