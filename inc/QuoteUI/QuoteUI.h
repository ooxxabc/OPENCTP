// QuoteUI.h : QuoteUI DLL 的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

//#ifdef UI_EXPORTS
//#define UI_API __declspec(dllexport)
//#else
//#define UI_API __declspec(dllimport)
//#endif
//
/************************************************************************/
/* 行情接口                                                              */
/************************************************************************/

#include "ctpImpl/Quote_def.h"

enum enmBQuoteType
{
	QT_TICK = 0,
	QT_KLINE,
};

struct tagKLine
{
	float high;
	float low;
	float open;
	float close;

	int volume;
	char time[33];
};

typedef tagMarketData tagTick;

/*
创建Quote视图的参数
*/

struct tagQuoteViewParam
{
	double			grid;		//价格网格区间：如一格10元
	double			limit_up;	//涨停价(可以不填，由行情自动变更)
	double			limit_down;	//跌停价(可以不填，由行情自动变更)
	char			szIns[33];
};

class IQuoteView
{
	//填充行情
	virtual void PushTick(tagTick &BQuote) = 0;
	virtual void PushKLine(tagKLine &BQuote) = 0;

};

class CQuoteUIApp: public CWinApp
{
public:
	CQuoteUIApp();

	// 重写
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};

