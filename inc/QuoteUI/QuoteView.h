#pragma once
#include "QuoteUI.h"
#include "ChartViewer.h"

// CQuoteView 

// The number of samples per data series used in this demo
static const int sampleSize = 300000;	//每日最大30w tick，收盘后清空
// The initial full range is set to 60 seconds of data.
static const int initialFullRange = 60;
// The maximum zoom in is 10 seconds.
static const int zoomInLimit = 10;

class CQuoteView : public CDialog, public IQuoteView
{
	DECLARE_DYNAMIC(CQuoteView)

public:
	CQuoteView(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CQuoteView();
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持


	// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSelchangeUpdatePeriod();
	afx_msg void OnViewPortChanged();
	afx_msg void OnMouseMovePlotArea();
	afx_msg void OnPointerPB();
	afx_msg void OnZoomInPB();
	afx_msg void OnZoomOutPB();
	afx_msg void OnSavePB();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

	DECLARE_MESSAGE_MAP()

	void OnDataRateTimer();
	void OnChartUpdateTimer();

private:

	double m_timeStamps[sampleSize];	// The timestamps for the data series
	double m_dataSeriesA[sampleSize];	// The values for the data series A

	// The index of the array position to which new data values are added.
	int m_currentIndex;

	// Used by the random number generator to generate real time data.
	double m_nextDataTime;

	// Draw chart
	void drawChart(CChartViewer *viewer);
	void trackLineLabel(XYChart *c, int mouseX);

	// Update controls when the view port changed
	void updateControls(CChartViewer *viewer);
	// Moves the scroll bar when the user clicks on it
	double moveScrollBar(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

private:

	CChartViewer m_ChartViewer;
	CScrollBar m_HScrollBar;
	CWnd * m_pParent;
	tagQuoteViewParam m_stCreateParam;
public:
	virtual void PostNcDestroy();

	//填充行情
	virtual void PushTick(tagTick &BQuote);
	virtual void PushKLine(tagKLine &BQuote);
};




/************************************************************************/
/*                                                                      */
/************************************************************************/

extern "C"
{
	UI_API CQuoteView*CreateObject(HWND hwnd);
	UI_API void ReleaseObject();
}