#pragma once
#include "FABase/SockCommon.h"

class IClientHandle;
class bufferevent;

class IClient
{
public:
	virtual void Connect(char *ip, int port, IP_ADDRESS ipAddress) = 0;
	virtual void Init(IClientHandle * pHandle) = 0;
	virtual int Release() = 0;
	virtual void Send(bufferevent *bev, int32_t msgID, char *pData, int nLength) = 0;
};

class IClientHandle
{
public:
	virtual void Recv(bufferevent *bev, int32_t msgID, char *pData, int nLength) = 0;
	virtual void OnDisconnect() = 0;
	virtual void OnErr() = 0;

};

