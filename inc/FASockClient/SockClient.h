#pragma once
#include "IClient.h"
#include "dllHelper.h"

//#ifdef SOCKCLIENT_EXPORTS
//#define SOCKCLIENT_API __declspec(dllexport)
//#else
//#define SOCKCLIENT_API __declspec(dllimport)
//#endif
//
extern "C"{
FA_API IClient * CreateObject(char *ip, int port, IP_ADDRESS ipAddress, IClientHandle * pHandle);
FA_API void Run();
FA_API void ReleaseObject(IClient * pClient);
}