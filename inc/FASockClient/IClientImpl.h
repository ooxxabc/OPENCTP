#pragma once
#ifndef WIN32
	#include<sys/types.h>
	#include<sys/socket.h>
	#include<netinet/in.h>
	#include<arpa/inet.h>
	#include<errno.h>
	#include<unistd.h>
#endif

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include<event.h>
#include<event2/bufferevent.h>
#include<event2/buffer.h>
#include<event2/util.h>
#include "IClient.h"
#include "Single.h"

class IClientImpl:public IClient
{
public:
	IClientImpl();
	IClientImpl(IClientHandle * pHandle);
	~IClientImpl();
private:
	IClientImpl(const IClientImpl&p);
	IClientImpl &operator=(const IClientImpl & p);
public:

	virtual void	Connect(char *ip, int port, IP_ADDRESS ipAddress);
	virtual void	Init(IClientHandle * pHandle);
	virtual int		Release();
	virtual void	Send(bufferevent *bev, int32_t msgID, char *pData, int nLength);
public:
	IClientHandle * GetHandle()	{return m_pHandle;	}
private:
	IClientHandle * m_pHandle; 
};

#define CLIENT singleton_t<IClientImpl>::instance()