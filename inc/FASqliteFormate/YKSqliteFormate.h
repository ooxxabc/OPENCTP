#pragma once
#include "FABase/thread.h"
#include "ctpImpl/Quote_def.h"
#include "FASqlite/ISqlite.h"
#include <vector>
#include "FABarMaker/FABarMaker.h"
#include "single.h"
#include "FALog4cplus/FALogHelper.h"
#include "FABase/safe_vector.h"
using namespace std;

struct tagT
{
	tagT(){}
	tagT(int n1, int n2)
	{
		nH = n1;
		nM = n2;
	}
	int nH;
	int nM;

	bool operator==(const tagT&t2)
	{		
		return (nH == t2.nH && nM==t2.nM);
	}

	bool operator<(const tagT&t2)
	{
		return (nH < t2.nH) || (nH == t2.nH && nM < t2.nM);
	}


	bool operator<=(const tagT&t2)
	{
		return (nH <= t2.nH) || (nH == t2.nH && nM <= t2.nM);
	}

	bool operator>(const tagT&t2)
	{
		return (nH > t2.nH) || (nH == t2.nH && nM > t2.nM);
	}

	bool operator>=(const tagT&t2)
	{
		return (nH >=t2.nH) || (nH == t2.nH && nM >= t2.nM);
	}
};

struct tagTradeTime
{
	tagT tBegin;
	tagT tEnd;

	bool IsTradeTime(tagT & t1)
	{
		if (t1 >= tBegin && t1 < tEnd)
		{
			return true;
		}
		return false;
	}
};

typedef vector<tagTradeTime> VTradeTime;
typedef VTradeTime::iterator VTradeTimeIter;

typedef vector<tagMarketData> VData;
typedef VData::iterator VDataIter;

struct tagSql
{
	char szSql[256];
	tagSql(){ szSql[0] = 0; }
	tagSql(char *sql)
	{
		sprintf(szSql, "%s", sql);
	}
};

class CTradeTime
{
public:
	CTradeTime();
	~CTradeTime(){}
	bool IsTradeTime(char *szTime);
	//void GetSysTime(char *sz);
	bool IsTradeSysTime();
private:
	VTradeTime	m_vTime;
};
#define TRADE_TIME singleton_t<CTradeTime>::instance()

class CSqliteQuoteData:public IBarCreator
{
public:
	CSqliteQuoteData();
	~CSqliteQuoteData();
public:
	void Init(ILog * pLog, ISqlite *pSql, int nRound, char*szTableName, char *szIns, char*szTickTableName);
	void Storge(tagMarketData*pData);
	virtual void OnCreateBar(CFABar *p);
	static void * _Save(const bool*isstop, void *param);

private:
	ISqlite *	m_pSql;
	int			m_nRound;		//����
	
	FABarMaker	m_barM;
	char		m_szIns[33];
	char		m_szTableName[33];
	char		m_szTickTableName[33];
	bool		m_bInit;
	ILog		*m_pLog;
	CYKThread	*m_pTh;
	CYKSafeVector<tagSql>	m_vSql;
};
