#include "ctp/ThostFtdcMdApi.h"
#include "Quote_i.h"
#include "Proxy_def.h"

class CCTPQuoteApi: public IQuote
{
public:
	CCTPQuoteApi(void);
	~CCTPQuoteApi(void);

public:
	// 初始化
	virtual bool Init(CThostFtdcMdSpi* pThostMdSpi, char* FrontAddr);
	// 清理
	virtual void Fini();
	// 登录
	virtual bool Login(char* pszBroker, char* pszInvestor, char* pszPassword);
	// 断开前置
	virtual void Disconnect();

	// 订阅行情
	virtual bool SubMarketData(char* pszInstrumentID[], int nCount);
	// 退订行情
	virtual bool UnSubMarketData(char* pszInstrumentID[], int nCount);

	// 获取当前交易日
	virtual const char* GetTradingDay();

	//变量
private:
	static int                      m_nRequestSeq;                          // 请求序列
	CThostFtdcMdApi*				m_pThostMdApi;
	int                             m_nSessionID;                           // 登录成功后分配到的Session
	char                            m_szBrokerID[BROKER_ID_LENGTH];         // 经纪ID
	char                            m_szInvestorID[INVESTOR_ID_LENGTH];     // 投资人ID
};