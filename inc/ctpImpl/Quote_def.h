#pragma once
#include "ctp/ThostFtdcMdApi.h"
#include "ctp/ThostFtdcUserApiDataType.h"
#include "ctp/ThostFtdcUserApiStruct.h"

// 行情数据
struct tagMarketData
{
	char	            szINSTRUMENT[31];         // 合约ID

	double	            dBidPrice1;                 // 申买价一
	int	                nBidVolume1;                // 申买量一
	double	            dAskPrice1;                 // 申卖价一
	int	                nAskVolume1;                // 申卖量一    

	double              dLastPrice;                 // 最新价
	double	            dAvgPrice;                  // 当日均价
	int	                nVolume;                    // 成交量
	double	            dOpenInt;                   // 持仓量

	double	            dUpperLimitPrice;           // 涨停板价
	double	            dLowerLimitPrice;           // 跌停板价

	char	            szUpdateTime[18];           // 最后修改时间(yyyyMMdd HH:mm:ss)
	int	                nUpdateMillisec;            // 最后修改毫秒
};
