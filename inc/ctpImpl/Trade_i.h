#pragma once

#include "Trade_def.h"

// 交易事件接口
class ITradeEvent
{
	// 基础事件
public:
	// 交易连接处理
	virtual void OnTradeConnected() = 0;
	// 交易断开处理
	virtual void OnTradeDisconnected(int nReason) = 0;

	// 交易用户登录成功
	virtual void OnTradeUserLoginSuccess(int nSessionID, int nFrontID, int nOrderRef) = 0;
	// 交易用户登录失败
	virtual void OnTradeUserLoginFailed(int nErrorID, const char* pszMsg) = 0;

	// 交易错误处理
	virtual void OnTradeError(int nErrorID, const char* pszMsg,int nRequestID = 0) = 0;

	// 结算单事件
public:
	// 查询结算单确认情况处理
	virtual void OnTradeQueryConfirmSettlement(bool bNeedConfirm) = 0;
	// 确认结算单成功
	virtual void OnTradeConfirmSettlementSuccess() = 0;
	// 确认结算单失败
	virtual void OnTradeConfirmSettlementFailed() = 0;
	// 查询结算单响应处理
	virtual void OnTradeQuerySettelment(tagSettlement* pstSettlement, bool bLast) = 0;

	virtual void OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) = 0;
	// 账户事件
public:
	// 查询资金账户响应事件处理
	virtual void OnTradeQueryTradingAccount(tagTradingAccount* pstTradingAccount, bool bLast) = 0;

	// 持仓事件
public:
	// 查询持仓响应事件处理
	virtual void OnTradeQueryPosition(tagPosition* pstPosition, bool bLast) = 0;
	// 查询持仓明细响应事件处理
	virtual void OnTradeQueryPositionDetail(tagPositionDetail* pstPositionDetail, bool bLast) = 0;

	// 报单事件
public:
	// 报单返回
	virtual void OnTradeRtnOrder(tagOrder* pstOrder) = 0;
	// 成交返回
	virtual void OnTradeRtnTrade(tagTrade* pstTrade) = 0;


	// 
public:
	// 查询合约响应事件处理
	virtual void OnTradeQueryInstrument(tagInstrument* pstINSTRUMENT1, bool bLast) = 0;
	// 查询报单响应事件处理
	virtual void OnTradeQueryOrder(tagOrder* pOrder, bool bLast) = 0;
	// 查询成交响应事件处理
	virtual void OnTradeQueryTrade(tagTrade* pTrade, bool bLast) = 0;


	// ITradeProtocolEvent impl - 结算单事件
public:
	// 确认结算单成功
	virtual void OnTradeSettlementConfirmSuccess() = 0;
	// 确认结算单失败
	virtual void OnTradeSettlementConfirmFailed() = 0;

};




// 交易接口
class ITrade
{
	// 基础接口
public:
	// 初始化
	virtual bool Init(CThostFtdcTraderSpi* pThostTradeSpi, char* FrontAddr) = 0;
	// 清理
	virtual void Fini() = 0;
	// 登录
	virtual bool Login(const char* pszBroker, const char* pszInvestor, const char* pszPassword) = 0;
	// 断开前置
	virtual void Disconnect() = 0;

	// 获取当前交易日
	virtual const char* GetTradingDay() = 0;

	// 结算单接口
public:
	// 确认结算单
	virtual bool ConfirmSettlement() = 0;
	// 查询结算单确认情况
	virtual bool QuerySettlementConfirm() = 0;
	// 查询结算单(不提供交易日,则默认为上一交易日)
	virtual bool QuerySettlement(const char* pszTradingDay=NULL) = 0;

	// 账户接口
public:
	// 修改账户密码
	virtual bool UpdateUserPassword(const char* pszOldPassword, const char* pszNewPassword) = 0;
	// 修改资金账户密码
	virtual bool UpdateTradingAccountPassword(const char* pszOldPassword, const char* pszNewPassword) = 0;
	// 查询资金账户
	virtual bool QueryTradingAccount() = 0;

	// 持仓接口
public:
	// 查询持仓
	virtual bool QueryPosition() = 0;
	// 查询持仓明细
	virtual bool QueryPositionDetail(const char* pszInstrumentID) = 0;

	// 报单接口
public:
	// 报单录入
	virtual bool OrderInsert(const char* pszInstrumentID, enTradeType nTradeType, \
		enTradeDir nTradeDir, enTradeOperate nTradeOperate, enTradeOrderType nOrderType, \
		double dPrice, int nVolume, char* pszOrderRefSuffix,int &nRequestID) = 0;
	// 报单操作
	virtual bool OrderAction(tagCancelOrderInsert* pOrder) = 0;
	// 查询报单
	virtual bool QueryOrder() = 0;
	// 查询成交
	virtual bool QueryTrade() = 0;


	// 基础查询
public:
	// 查询交易所
	virtual bool QueryExchange(const char* pszExchangeID) = 0;
	// 查询合约
	virtual bool QueryInstrument(const char* pszInstrumentID) = 0;
	// 查询商品
	virtual bool QueryProduct(const char* pszProductID, enProductType nProductType) = 0;

	// 预埋单接口
public:
	// 预埋单录入
	virtual bool ParkedOrderInsert() = 0;
	// 预埋单操作
	virtual bool ParketOrderAction() = 0;
	// 删除预埋单
	virtual bool RemoveParketOrder() = 0;
	// 删除预埋单操作
	virtual bool RemoveParkedOrderAction() = 0;
	// 查询预埋单
	virtual bool QueryParkedOrder(const char* pszExchangeID, const char* pszInstrumentID) = 0;
	// 查询预埋单操作
	virtual bool QueryParkedOrderAction(const char* pszExchangeID, const char* pszInstrumentID) = 0;

	// 银期接口
public:
	// 银行资金转期货
	virtual bool TransferFromBankToFuture() = 0;
	// 期货资金转银行
	virtual bool TransferFromFutureToBank() = 0;
	// 查询转账银行
	virtual bool QueryTransferBank() = 0;
	// 查询转账流水
	virtual bool QueryTransferStatement() = 0;
	// 查询银期签约关系
	virtual bool QueryBankFutureContract() = 0;
	// 查询签约银行
	virtual bool QueryContractBank() = 0;
	// 查询银行余额
	virtual bool QueryBankRemainingBalance() = 0;
};