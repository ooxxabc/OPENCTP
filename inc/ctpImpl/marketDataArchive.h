#pragma once
#include "Quote_def.h"
#include "FAJson/RapidjsonHelper.h"

//序列化的数据
class tagMarketDataArchive:public JsonBase
{
public:
	tagMarketDataArchive()
	{
		szINSTRUMENT[0]='\0';
		szUpdateTime[0]='\0';
	}
	char	            szINSTRUMENT[31];         // 合约ID

	double	            dBidPrice1;                 // 申买价一
	int	                nBidVolume1;                // 申买量一
	double	            dAskPrice1;                 // 申卖价一
	int	                nAskVolume1;                // 申卖量一    

	double              dLastPrice;                 // 最新价
	double	            dAvgPrice;                  // 当日均价
	int	                nVolume;                    // 成交量
	double	            dOpenInt;                   // 持仓量

	double	            dUpperLimitPrice;           // 涨停板价
	double	            dLowerLimitPrice;           // 跌停板价

	char	            szUpdateTime[18];           // 最后修改时间(yyyyMMdd HH:mm:ss)
	int	                nUpdateMillisec;            // 最后修改毫秒

	void Copy(tagMarketData *pData)
	{
		dBidPrice1 = pData->dBidPrice1;
		nBidVolume1 = pData->nBidVolume1;
		dAskPrice1 = pData->dAskPrice1;
		nAskVolume1 = pData->nAskVolume1;
		dLastPrice = pData->dLastPrice;
		dAvgPrice = pData->dAvgPrice;
		nVolume = pData->nVolume;
		dOpenInt = pData->dOpenInt;
		dUpperLimitPrice = pData->dUpperLimitPrice;
		dLowerLimitPrice = pData->dLowerLimitPrice;
		nUpdateMillisec = pData->nUpdateMillisec;
		strcpy(szUpdateTime,pData->szUpdateTime);
		strcpy(szINSTRUMENT,pData->szINSTRUMENT);
	}

	void ToWrite(Writer<StringBuffer> &writer);

	void ParseJson(const Value& val);

	void GetRootName(char *root);
};
