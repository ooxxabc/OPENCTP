#pragma once
#include "FABase/common.h"

//#define LOG_TRACE						0
//#define LOG_INFO                        1
//#define LOG_DEBUG                       2
//#define LOG_WARN                        3
//#define LOG_ERROR                       4
//#define LOG_FATAL						5

#define OUTPUT(x) printf(x);printf("\n");

#define LOGOUT(pLog,level, format, ...) \
	do {\
			printf("[%s|%s,%d] " format "\n", \
				level, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
			if (level >= LOG_DEBUG) {	\
			char sz[512] = { '\0' }; \
			sprintf(sz,"[%s|%s,%d] " format, \
				level, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
			if(pLog) pLog->Log(level,sz); }; \
		} while (0)


class ILogHelper
{
public:
	virtual void Bind() = 0;
	virtual void UnBind() = 0;
};

class ILog
{
public:
	virtual void Log(int level,char* szLog) = 0;
};