#pragma once
#include <vector>
using namespace std;

/************************************************************************/
/* 
	派生一个基于vector的类，该类的[0]为原始vector的最后一个元素, [1]为倒数第二个，以此类推
*/
/************************************************************************/

template<class _Ty>
class CFAVector: public vector<_Ty>
{
public:
#ifdef WIN32
	reference operator[](size_type _Pos)
	{
		return vector<_Ty>::operator[](size() - _Pos - 1);
	}
#else
	_Ty& operator[](std::size_t _Pos)
	{
		return std::vector<_Ty>::operator[](vector<_Ty>::size() - _Pos - 1);
	}
#endif	
};

// 指标算法底层库
// 注：所有指标计算中，均不考虑数组越界
class CFAAlgorithm
{
public:
	CFAAlgorithm(void);
	virtual ~CFAAlgorithm(void);

public:
  // 计算SUM（序列和）指标
  static double SUM(double* d, int M);

  // 计算MA（移动平均）指标
  static double MA(double* d, int M);

  // 计算STD（标准差）指标
  static double STD(double* d, int M);
  
  static double Lowest(double* d, int M);
  static double Highest(double* d, int M);

};


#define MIN(A,B) (A < B ? A : B)

#define MAX(A,B) (A > B ? A : B)


