#pragma once

#ifdef WIN32
	#include <windows.h>
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include <float.h>
	#include <math.h>
	#include <ObjBase.h>
	#include <stdarg.h>
#else

	#include <unistd.h>
	#include <string.h>
	#include <stdlib.h>
	#include <stdio.h>
	#include <math.h>
	#include <signal.h>
	#include <fcntl.h>
	#include <sys/param.h>
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <fcntl.h>
	#include <uuid/uuid.h>	//yum install libuuid-devel
	#include <sched.h>
	#include <dlfcn.h>
	#include <dirent.h>
	#include <time.h>
	#include <ctype.h>
	#include <utime.h>
	#include <stdint.h>
	#include <stdarg.h>
	#include <wchar.h>
	#include <ctype.h>
	#include <wctype.h>
#endif 

inline void _AssertionFail(const char * strFile, int nLine)
{
	::fflush(stdout);
	::fprintf(stderr, "Asssertion failed: file %s, line %d", strFile, nLine);
	::fflush(stderr);
	::abort();
}


#ifndef NDEBUG
	void _AssertionFail(const char * strFile, int nLine);
	#define Assert(p)	((p) ? (void)0 : (void)_AssertionFail(__FILE__, __LINE__))
#else
	#define Assert(p)	(void(0))
#endif

#define ASSERT_RETURN(p)						\
		Assert(p);								\
		if (!(p))								\
		{										\
			printf("Assert failed: %s", #p);	\
			return;								\
		}

#define ASSERT_RETURN_VALUE(p, val)				\
		Assert(p);								\
		if (!(p))								\
		{										\
			printf("Assert failed: %s", #p);	\
			return val;							\
		}

#define ASSERT_RETURN_NOLOG(p)					\
		Assert(p);								\
		if (!(p))								\
		{										\
			return;								\
		}

#define ASSERT_RETURN_VALUE_NOLOG(p, val)		\
		Assert(p);								\
		if (!(p))								\
		{										\
			return val;							\
		}
		
//#ifndef FAREDIS_EXPORTS
//	#define REDIS_API __declspec(dllimport)
//#else			
//	#define REDIS_API __declspec(dllexport)
//#endif
//
struct tagMarketData;

class IRedis
{
public:
	virtual void Storge(tagMarketData*pData) = 0;

	virtual void Start() = 0;
	virtual void Stop() = 0;
};

class IRedisHandle
{
public:
	virtual void OnErr(char *szErrCode) = 0;
};
