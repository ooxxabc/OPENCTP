#pragma once
#include "FABase/safe_vector.h"
#include "FABase/thread.h"
#include "StrategyPlatform/StrategyPlatform_Def.h"


/************************************************************************/
/* 
	作为Platform的插件
	1.StrategyPlus只需初始化参数给底层即可
*/
/************************************************************************/
typedef vector<tagOrder> VOrder;
typedef VOrder::iterator VOrderIter;

class CFAOrderMonitor
{
public:
	CFAOrderMonitor();
	/*
		platform:平台指针
		fSlippage:重报滑点
		nCancelTime:撤单时间（s）; 如果=0，则不重报单
	*/
	void Init(IStrategyPlatform*platform, const float &fSlippage, const int &nCancelTime);
	/*
		报单返回	
	*/
	void OnOrder(tagOrder*pOrder);
	//谨防成交后无Order通知
	void OnTrade(tagTrade*pTrade){}

public:
	void Start();
	void Pause(){}
	void Stop(){}

	static void * Monitor(const bool*isstop, void *param);
protected:
private:
	IStrategyPlatform * m_platform;
	float				m_fSplippage;
	int					m_nCancelTime;
	CYKMutex			m_mutex;
	VOrder				m_v;			//下单队列
	VOrder				m_vRe;			//重报队列

};