#pragma once
#ifndef WIN32
#include<netinet/in.h>  
#include<sys/socket.h>  
#include<unistd.h>  
#endif
#include "FABase/common.h"

#include<stdio.h>  
#include<string.h>  

#include "IServer.h"
#include "single.h"
#include <map>
using namespace std;

struct tagClient
{
	int32_t login_time;		//登录时间
	int32_t live_time;		//最近活动时间
};

typedef map<bufferevent*, tagClient>MClient;
typedef MClient::iterator MClientIter;

class IServerImpl:public IServer
{
public:
	IServerImpl();
	IServerImpl(IServerHandle *pHandle);
	~IServerImpl();
private:
	IServerImpl(const IServerImpl &impl);
	IServerImpl& operator=(const IServerImpl &);
public:
	virtual int Create(int port, IP_ADDRESS ipAddress);
	virtual void Init(IServerHandle * pHandle);
	virtual int Release();
	virtual void Send(bufferevent *bev,int32_t msgID, char *pData, int nLength);
	virtual void BroadCast(int32_t msgID, char *pData, int nLength);

public:

	static void listener_cb(evconnlistener *listener, evutil_socket_t fd, struct sockaddr *sock, int socklen, void *arg);
	static void socket_read_cb(bufferevent *bev, void *arg);
	static void socket_event_cb(bufferevent *bev, short events, void *arg);
public:
	IServerHandle * GetHandle(){ return m_pHandle; }
	void AddClient(bufferevent * bev);
	void RemoveClient(bufferevent * bev);	
public:
	event_base *base;
	evconnlistener *listener;
	evconnlistener *listener6;
private:
	IServerHandle	*	m_pHandle;
	MClient				m_Client;
};

#define SERVER singleton_t<IServerImpl>::instance()