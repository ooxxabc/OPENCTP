#pragma once
#include <stdio.h>
#include <string>
#include "IServer.h"
#include "dllHelper.h"

//#ifdef SOCKSERVER_EXPORTS
//#define SOCKSERVER_API __declspec(dllexport)
//#else
//#define SOCKSERVER_API __declspec(dllimport)
//#endif

extern "C" {

FA_API IServer* CreateObject(int port,IP_ADDRESS ipAddress,IServerHandle * pHandle);
FA_API void Run();
FA_API void ReleaseObject(IServer *pServer);
}
extern int32_t _port;
extern IP_ADDRESS _ipAddress;