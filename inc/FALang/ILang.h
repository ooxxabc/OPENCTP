#pragma once
#include <stdio.h>

class ILang
{
public:
	virtual void MBCS2Unicode(char *szIn,int szInLen,wchar_t *szOut) = 0;
	virtual void Unicode2MBCS(wchar_t *szIn,int szInLen,char *szOut) = 0;

	virtual void Unicode2Utf8(wchar_t *szIn,int szInLen,char *szOut) = 0;
	virtual void Utf82Unicode(char *szIn,int szInLen,wchar_t *szOut) = 0;

	virtual void MBCS2Utf8(char *szIn,int szInLen,char *szOut) = 0;
	virtual void Utf82MBCS(char *szIn,int szInLen,char *szOut) = 0;

};