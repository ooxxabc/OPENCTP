// 下列 ifdef 块是创建使从 DLL 导出更简单的
// 宏的标准方法。此 DLL 中的所有文件都是用命令行上定义的 TPRINT_EXPORTS
// 符号编译的。在使用此 DLL 的
// 任何其他项目上不应定义此符号。这样，源文件中包含此文件的任何其他项目都会将
// TPRINT_API 函数视为是从 DLL 导入的，而此 DLL 则将用此宏定义的
// 符号视为是被导出的。

#pragma once
#include "FABase/Log.h"

//#ifdef TPRINT_EXPORTS
//#define TPRINT_API __declspec(dllexport)
//#else
//#define TPRINT_API __declspec(dllimport)
//#endif
//

#define LOG1(x)
#define LOG2(x) Log(x);
#define OUTPUT(x) printf(x);printf("\n");
#define OUTPUT2(level, format, ...) \
	do {\
	printf("[%d|%s,%d] " format "\n", \
	level, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
	if (level == LOG_ERROR || level == LOG_INFO) {	\
	char sz[512] = { '\0' }; \
	sprintf(sz,"[%d|%s,%d] " format, \
	level, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
	LOG2(sz) }; \
	} while (0)


/*
	多线程打印，解决主线程输出卡
*/

//#define LOG_DEBUG "DEBUG"
//#define LOG_TRACE "TRACE"
//#define LOG_ERROR "ERROR"
//#define LOG_INFO  "INFOR"

#define LOG_TYPE 16
#define MAX_LEN 1024

class IYKPrint
{
public:
	virtual void Stop() = 0;
	virtual void Log(int level, char *format, ...) = 0;
	virtual void Print(int level, char *format,...) = 0;
};
