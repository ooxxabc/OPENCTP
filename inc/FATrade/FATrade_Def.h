#pragma once

#include "ctpImpl/Trade_i.h"

class IFATrade
{
public:
	IFATrade(){}
	explicit IFATrade(ITradeEvent*pEvent){}
	virtual ~IFATrade(){}
public:
	virtual int Connect(char *address) = 0;
	virtual int Login(char*broker,char *account,char*pwd) = 0;
	virtual int Logout() = 0;
	virtual int DisConnect() = 0;
	//����nRequestID
	virtual int Order(const char* pszInstrumentID, enTradeType nTradeType, \
		enTradeDir nTradeDir, enTradeOperate nTradeOperate, enTradeOrderType nOrderType, \
		double dPrice, int nVolume, char* pszOrderRefSuffix,int &nRequestID) = 0;
	virtual int CancelOrder(tagOrder *pOrder) = 0;
	virtual int QueryPosition() = 0;
	virtual int QuerySettlement() = 0;
	virtual int ComfirmSettlement() = 0;

	virtual int OnLoginSuccess(int nSessionID, int nFrontID, int nOrderRef) = 0;
	virtual int OnLoginFail() = 0;
	virtual int OnDisconnect() = 0;

	virtual int QueryAccountDetail() = 0;
	virtual int QueryInstrumentDetail(char*ins[],int count) = 0;

protected:
	IFATrade(const IFATrade&);
	IFATrade& operator=(const IFATrade&);

};