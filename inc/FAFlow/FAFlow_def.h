#pragma once
#include "FABase/common.h"
#include "FATrade/FATrade_Def.h"
#include "FABase/AutoPt.h"

//#ifdef FAFLOW_EXPORTS
//#define FAFLOW_API __declspec(dllexport)
//#else
//#define FAFLOW_API __declspec(dllimport)
//#endif

/*
	CTP流控：
	1,子线程处理
	2,查询间隔:1s
	3,报单频率:6单/s
*/

#define QUERY_FLOW_SLEEP 1000
#define ORDER_FLOW_SLEEP (1000/6)

//流控对象
enum FLOW_TYPE
{
	FLOW_QUOTE_CONNECT	= 1,		//行情重连
	FLOW_TRADE_CONNECT,				//行情重连
	//FLOW_CONNECT,
	FLOW_LOGIN,
	FLOW_SETTLEMENT,
	FLOW_ACCOUNT,				//账户权益
	FLOW_CONFIRM_SETTLEMENT,
	FLOW_INSTRUMENT,
	FLOW_POSITION,				//仓位


	FLOW_EXIT,					//退出线程
};

//任务处理接口回调
class ITaskHandle :public CYKCounter
{
public:
	virtual void Handle() = 0;
	virtual void Init() {}
	virtual void Uninit() {}
};

//流任务
class CTask:public CYKCounter
{
public:
	explicit CTask(int8_t type, ITaskHandle* pHandle)
		:mtype(type)
		, mpHandle(pHandle)
	{
	}
	virtual ~CTask(){}
public:
	ITaskHandle*	mpHandle;
	int8_t			mtype;		//类型
};


typedef CYKAutoPoint<CTask*> CAutoTask;

//FLow对象
class IFlow
{
public:
	virtual void RegisterQueryTask(CTask*pTask) = 0;	//添加任务
	virtual void RegisterOrderTask(CTask*pTask) = 0;	//添加任务
	virtual void RegisterAccountTask(CTask*pTask) {}	//循环任务
	virtual void RegisterPositionTask(CTask*pTask) {}	//循环任务
	virtual void Start() = 0;							//启动线程
	virtual void Stop() = 0;

	virtual bool IsContinue() = 0;						//事件顺序执行
	virtual void SetContinue(bool con) = 0;
};

