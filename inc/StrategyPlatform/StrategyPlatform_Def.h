#pragma once
#include "FABase/common.h"
#include "FAQuote/FAQuote_Def.h"
#include "FATrade/FATrade_Def.h"
#include "ctpImpl/Quote_i.h"
#include "ctpImpl/Trade_i.h"
#include "FAPrint/TPrint_def.h"
#include "FAPositionAnalyze/IPosition.h"
#include <vector>
#include <map>
using namespace std;

#define ARRAY_SIZE				128					//策略最大个数

#define PLATFORM_INS_MAX_COUNT	3					

/************************************************************************/
/* 
 近月支持市价单:郑州、大连、中金所；
 远月不支持市价单:上海、中金所。

 处理方法:
 1.当前仅支持限价单
 2.超过N秒，不成交则撤单

*/
/************************************************************************/
class IHandle
{
public:

};

//策略接口
class IStrategyPlus
{
public:
	virtual int Init(IYKPrint *p=0,IPosition *pPos=0) = 0;
	virtual int UnInit() = 0;
	virtual int OnQuote(tagMarketData * pTick) = 0;
	virtual int OnOrderStatus() = 0;
	virtual int OnTradeStatus() = 0;
	//通知账户权益
	virtual int OnAccountDetail(tagTradingAccount*pAccount) { return 0; }
	virtual int OnPositionDetail(tagPosition*pPos, bool bLast) { return 0; }
	virtual int ClearPositionDetail() { return 0; }
	//下单、撤单
	virtual int Order(ORDER_TYPE type) { return 0; }
	virtual int Cancel(int orderID = 0) { return 0; }
	//保留方法
	virtual int SetHandle(IHandle * pHandle){ return 0; }
	//成交通知
	virtual int OnTraded(tagOrder *pOrder){ return 0; }
};

typedef IStrategyPlus* LPSTRATEGYPLUS;


typedef uint32_t REQUEST_ID;
typedef uint32_t ORDER_ID;
//typedef uint32_t TRADE_ID;
typedef uint32_t POSITION_ID;

//typedef map<REQUEST_ID, ORDER_ID>	MReq2OrderID;
//typedef map<REQUEST_ID, tagOrderDelegation>	MOrderDelegation;
typedef map<ORDER_ID, tagOrder>	MOrder;
typedef map<ORDER_ID, tagTrade>	MTrade;
typedef vector<tagPosition>	VPosition;
//
//typedef MReq2OrderID::iterator MReq2OrderIDIter;
//typedef MOrderDelegation::iterator MOrderDelegationIter;
typedef MOrder::iterator MOrderIter;
typedef MTrade::iterator MTradeIter;
typedef VPosition::iterator VPositionIter;

class IStrategyPlatform
{
public:
	IStrategyPlatform(){}
	virtual ~IStrategyPlatform(){}
protected:
	IStrategyPlatform(const IStrategyPlatform&);
	IStrategyPlatform& operator=(const IStrategyPlatform&);
public:
	//初始化ctp接口
	virtual int Init() = 0;
	virtual int UnInit() = 0;

	//订阅
	virtual int SubInstrument(char*szIns[], int &nCount) = 0;

	//下单
	virtual int Order(tagOrderDelegation &orderInsert,IPosition * pPos=0) = 0;
	virtual int OrderCancel(tagOrder*pOrder) { return 0; }

	////持仓
	//virtual int GetPositionDetail() = 0;
	//virtual int GetAccountDetail() = 0;

	//sqlite
	virtual int LoadTick(char**szIns,int &nCount,char *dateTime) = 0;
	virtual int PlayHistoryTick() = 0;

	//根据策略名，获得指定策略
	virtual LPSTRATEGYPLUS  GetStrategy(char *szStrategy) = 0;

	////获得持仓
	//virtual VPosition& GetPosition() = 0;
	//virtual void ClearPosition() = 0;
};
