#pragma once

#include "ctpImpl/Proxy_def.h"
#include "StrategyPlatform/StrategyPlatform_Def.h"
#include <vector>
#include <string>
#include "single.h"
using namespace std;

#define STRATEGY_PLATFORM_CONFIG _FTA("StrategyPlatform.xml")

struct tagStratePlus
{
	int  nId;					//编号唯一
	char szDll[URL_LEN];		//plus dll 文件名
	char szTag[URL_LEN];		//标签
};

struct tagMonitorCfg
{
	double fSplippage;
	int	   nCancelTime;
};

//策略平台基础配置
class StrategyPlatformConfig
{
public:
	StrategyPlatformConfig();
	virtual ~StrategyPlatformConfig();

public:
	int Load();
	int ReLoad();
	bool IsLoad(){ return bLoad; }

public:
	char * GetQuoteURL(){ return szQuoteURL; }
	char * GetTradeURL(){ return szTradeURL; }
	char * GetBrokerID(){ return szBrokerID; }
	char * GetAccount(){ return szAccount; }
	char * GetPassword(){ return szPWD; }
	tagStratePlus *GetPlusDll(){ return &arrPlusDll[0]; }
	int GetPlusCount(){ return plusCount; }

	int GetInsCount(){ return nInsCount; }
	char **GetIns(char **p);
	void GetSubIns(char *p[]);
	tagMonitorCfg &GetMonitorCfg(){ return m_monitorCfg; }
private:
	char szQuoteURL[URL_LEN];
	char szTradeURL[URL_LEN];
	char szBrokerID[BROKER_ID_LENGTH];
	char szAccount[INVESTOR_ID_LENGTH];
	char szPWD[PASSWORD_LENGTH];

	int  nLogLevel;
	char szSQLiteFile[URL_LEN];

	int	 nInsCount;
	char szIns[INS_MAX_COUNT][INS_ID_LENGTH];

	int				plusCount;
	tagStratePlus	arrPlusDll[ARRAY_SIZE];	//最大支持128个插件

	tagMonitorCfg	m_monitorCfg;

	bool	bLoad;
};

#define CONFIG singleton_t<StrategyPlatformConfig>::instance()