#pragma once

#include <stdio.h>
#ifdef WIN32
#include <tchar.h>
#else
#include <string>
#endif
#include <iostream>
#include <assert.h>
# include <stdexcept>
# include <string>
# include <vector>
# include <list>
# include <sstream>
#include "FABase/thread.h"

class ICSV2DB
{
public:
	virtual void Storge(const char *sz) = 0;
};

class CSV2DB
{
public:
	explicit CSV2DB(ICSV2DB*pDB);
	~CSV2DB();
public:
	bool Load(char*szFileName);
	void unLoad();
public:
	static void* loadFunc(const bool *bStop, void *param);
private:
	HANDLE		hFileMap;
	char		*lpbMapAddress;
	CYKThread	*pThread; 
	ICSV2DB		*pInterface;
};
