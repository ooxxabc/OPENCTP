#pragma once
#include <string>
#include <vector>
using namespace std;

typedef vector<string> VStr;
typedef vector<wstring> VWStr;

class CStringUtil
{
public:
	CStringUtil(void){}
	~CStringUtil(void){}

	static void Split(VStr&vs,const string&inString,const string&Separator);
	static void SplitW(VWStr&vs,const wstring&inString,const wstring&Separator);
};

