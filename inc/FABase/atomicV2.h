#pragma once


#ifdef ENABLE_BOOST_ATOMIC_COUNT

#include <boost/smart_ptr/detail/atomic_count.hpp>
typedef boost::detail::atomic_count atomic_count;

#elif _MSC_VER 

    #include <windows.h>

  //atomic counter based on interlocked functions for Win32
  class atomic_count
  {
  public:
    explicit atomic_count( long v ): m_counter( v )
    {
    }

    long operator++()
    {
      return ::InterlockedIncrement( &m_counter );
    }

    long operator--()
    {
      return ::InterlockedDecrement( &m_counter );
    }

    operator long() const
    {
      return ::InterlockedExchangeAdd(const_cast<long volatile *>( &m_counter ), 0 );
    }

  private:

    atomic_count( atomic_count const & );
    atomic_count & operator=( atomic_count const & );

    long volatile m_counter;
  };

#else
  //
  //  boost/detail/atomic_count_gcc_x86.hpp
  //
  //  atomic_count for g++ on 486+/AMD64
  //
  //  Copyright 2007 Peter Dimov
  //
  //  Distributed under the Boost Software License, Version 1.0. (See
  //  accompanying file LICENSE_1_0.txt or copy at
  //  http://www.boost.org/LICENSE_1_0.txt)
  //

  class atomic_count
  {
  public:

    explicit atomic_count( long v ) : value_(static_cast<int>(v)) {}

    long operator++()
    {
      return atomic_exchange_and_add( &value_, 1 ) + 1;
    }

    long operator--()
    {
      return atomic_exchange_and_add( &value_, -1 ) - 1;
    }

    operator long() const
    {
      return atomic_exchange_and_add( &value_, 0 );
    }

  private:

    atomic_count( atomic_count const & );
    atomic_count & operator=( atomic_count const & );

    mutable int value_;

  private:

    static int atomic_exchange_and_add(int * pw, int dv)
    {
      // int r = *pw;
      // *pw += dv;
      // return r;

      int r;

      __asm__ __volatile__
        (
          "lock\n\t"
          "xadd %1, %0":
          "+m"(*pw), "=r"(r) : // outputs (%0, %1)
          "1"(dv) : // inputs (%2 == %1)
          "memory", "cc" // clobbers
        );

      return r;
    }
  };

#endif