#pragma  once
#include "common.h"

/*
	���ݰ����к� & �����л�
*/

class FACodec
{
public:
	// ����int8
	static int32_t encode_int8(char** out_ptr, uint8_t src);
	// ����int8
	static int32_t decode_int8(char** in_ptr, uint8_t* out_ptr);

		// ����int16
	static int32_t encode_int16(char** out_ptr, uint16_t src);
	// ����int16
	static int32_t decode_int16(char** in_ptr, uint16_t* out_ptr);

		// ����int32
	static int32_t encode_int32(char** out_ptr, uint32_t src);
	// ����int32
	static int32_t decode_int32(char** in_ptr, uint32_t* out_ptr);
    
	// ����int64
	static int32_t encode_int64(char** out_ptr, uint64_t src);
	// ����int64
	static int32_t decode_int64(char** in_ptr, uint64_t* out_ptr);

		// ����string(��'\0'Ҳ�����˱��룬�ַ������Ȱ���'\0')
	static int32_t encode_string(char** out_ptr, const char* src_ptr, const int16_t max_string_len);
	// ����string
	static int32_t decode_string(char** in_ptr, char* out_ptr, const int16_t max_string_len);

		// �����ڴ��
	static int32_t encode_memory(char** out_ptr, char* src_ptr, const int32_t mem_size);
	// �����ڴ��
	static int32_t decode_memory(char** in_ptr, char* out_ptr, const int32_t mem_size);
};

