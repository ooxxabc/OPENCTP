#pragma once
#include "./StrategyPlatform/StrategyPlatform_Def.h"

class IMonitor
{
public:
	void AddOrder(tagOrder * pOrder);
	void UpdateOrder(uint32_t nOrdeID, enTradeOrderStatus nStatus);

	void AddTrade(tagTrade* pTrade);

	//void DelOrderDelegation(uint32_t nRequestID);
	void DelOrder(uint32_t nOrdeID);

	void AddPosition(tagPosition * pPosition, bool bLast);
	void ClearPosition();

	VPosition &GetPosition();

	//tagTrade -> tagPosition
	void Convert(tagTrade*trade, tagPosition&position, bool bToday = true);
	void Remove(tagPosition&pos, bool bToday = true);

};