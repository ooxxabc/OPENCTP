#pragma once

/*
	全市场合约规则
*/
#include <string>
#include "single.h"
#include "ctpImpl/Trade_def.h"

//#ifdef FAMARKET_EXPORTS
//#define FAMARKET_API __declspec(dllexport)
//#else
//#define FAMARKET_API __declspec(dllimport)
//#endif

#include <map>
using namespace std;

typedef map<string, tagInstrument> mIns;

class IInstrumentManage
{
public:
	virtual void Init(tagInstrument*pIns) = 0;
	virtual const tagInstrument &GetInstrument(string szIns) = 0;
};

class CInstrumentManage:public IInstrumentManage
{
public:
	CInstrumentManage();
	~CInstrumentManage();
public:
	void Init(tagInstrument*pIns);
	const tagInstrument &GetInstrument(string szIns) ;
private:
	mIns m_mins;
};

extern IInstrumentManage * pInstrumentManage;