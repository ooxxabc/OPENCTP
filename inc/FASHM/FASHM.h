// 下列 ifdef 块是创建使从 DLL 导出更简单的
// 宏的标准方法。此 DLL 中的所有文件都是用命令行上定义的 FASHM_EXPORTS
// 符号编译的。在使用此 DLL 的
// 任何其他项目上不应定义此符号。这样，源文件中包含此文件的任何其他项目都会将
// FASHM_API 函数视为是从 DLL 导入的，而此 DLL 则将用此宏定义的
// 符号视为是被导出的。
//#ifdef FASHM_EXPORTS
//#define FASHM_API __declspec(dllexport)
//#else
//#define FASHM_API __declspec(dllimport)
//#endif

#include <stdio.h>

#define DEFAULT_SHM_SIZE		64*1024
#define DEFAULT_SHM_NAME_LEN	255
#define DEFAULT_MSG_LEN			512

struct CSerialBuffer
{
	int	m_iServerBegin;
	int m_iServerPos;
	int	m_iClientBegin;
	int m_iClientPos;
	int	m_len;
};

class ISHMServer
{
public:
	virtual void OnRecvFromSHM(char *pData, int len) = 0;
};

class ISHMClient
{
public:
	virtual char *OnHandleData(char *psrcData, int srcLen, int &dstLen) = 0;

};

class CFASHM {
public:
	CFASHM(void);
	~CFASHM(void);

	// TODO:  在此添加您的方法。
public:

	void	SetBuffer(CSerialBuffer* pBuffer, int len);
	void*	GetBuffer();

	void	push_back(bool svr, char *pData, int len);
	bool	front(bool svr, char *pData, int &len);
	void	pop_front(bool svr);
private:
	CSerialBuffer	*m_pBuffer;

	char *			m_pServer;
	char *			m_pClient;
	char *			m_pServerEnd;
	char *			m_pClientEnd;
};

class CFASHMClient :public CFASHM
{
public:
	CFASHMClient(void);
	CFASHMClient(ISHMClient *pSHM);
	~CFASHMClient(void);
public:
	bool			Bind(TCHAR *shm_name, int len);
	ISHMClient	*	GetISHMClient();
	void			Destory();

public:
	//c->s		client发送到server
	void		SendToServer(char *pData, int len);

	void		Start();
private:
	ISHMClient	*	m_pSHMClient;
	HANDLE			m_hSHM;
	TCHAR			m_szSHMName[DEFAULT_SHM_NAME_LEN];
};

class CFASHMServer :public CFASHM
{
public:
	CFASHMServer(void);
	CFASHMServer(ISHMServer *pSHM);
	~CFASHMServer(void);
public:
	bool			Create(TCHAR *shm_name, size_t len);
	void			Destory();

	ISHMServer	*	GetISHMServer();
	TCHAR		*	GetSHMName();

public:
	//s->c		server发送到client
	void		SendToClient(char *pData, int len);

	void		Start();

private:
	ISHMServer	*	m_pSHMServer;
	HANDLE			m_hSHM;
	TCHAR			m_szSHMName[DEFAULT_SHM_NAME_LEN];

};




typedef TCHAR SHM_NAME[33];
typedef int	 SHM_SIZE;

FASHM_API CFASHMServer* CreateObjectS(ISHMServer *iServer, SHM_NAME name, SHM_SIZE size);
FASHM_API int ReleaseObjectS(void);
FASHM_API CFASHMClient* CreateObjectC(ISHMClient * iClient, SHM_NAME name, SHM_SIZE size);
FASHM_API int ReleaseObjectC(void);
