// Demo2.cpp : 定义控制台应用程序的入口点。
//

#include <stdio.h>
#include <tchar.h>

/*
	策略主程序
*/


#include "dllHelper.h"
#include "FAQuoteSt/FAQuoteSt_def.h"

CDllHelper _dll("FAQuoteSt.dll");

typedef IFAQuoteSt* (*CreateObjectA)();
CreateObjectA _func = NULL;
IFAQuoteSt * _pQuoteSt = 0;
typedef void* (*ReleaseObjectA)();
ReleaseObjectA _func2 = NULL;

int _tmain(int argc, char* argv[])
{
	//init

	/*
		Platform
	*/

	_func = _dll.GetProcedure<CreateObjectA>("CreateObject");
	if (_func)
	{
		_pQuoteSt = _func();
		if(_pQuoteSt)
			_pQuoteSt->DownStockData("002507");
	}
	printf("now is finish down quote!\n");
	_func2 = _dll.GetProcedure<ReleaseObjectA>("ReleaseObject");
	getchar();
	
	//release
	if (_func2)
	{
		_func2();
	}

	return 0;
}

