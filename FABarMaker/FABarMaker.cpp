#include "FABarMaker/FABarMaker.h"
#ifdef WIN32
#include <windows.h>
#endif
#include <time.h>

#define MIN_TICK	(60)
#define MONITOR_SLEEP (1000*MIN_TICK)

FABarMaker::FABarMaker()
:m_pCreator(0)
, m_nRound(MIN_1)
, m_pTH(0)
, m_nLastCreateBarTime(0)
{

}

void * FABarMaker::_run(const bool * isstop, void *param)
{
	FABarMaker * p = (FABarMaker*)param;
	if (!p) return 0;

	while (!(*isstop))
	{
		int n = time(0);
		if (p->m_nLastCreateBarTime != 0 && n - p->m_nLastCreateBarTime > (MIN_TICK * p->m_nRound + MIN_TICK))
		{
			//可能是最后一根K
			CFABar bar;
			if (SUCCESS == p->CreateLastBar(bar))
			{
				p->m_pCreator->OnCreateBar(&bar);
				p->m_vTick.clear();
			}
		}

		Sleep(MONITOR_SLEEP);
	}

	return 0;
}

void FABarMaker::Init(IBarCreator*pCreator, int nRound)
{
	m_pCreator = pCreator;
	m_nRound = nRound;
	m_pTH = new CYKThread(_run, this);
	m_pTH->start();
}


double FABarMaker::Lowest(VTick &t, int len)
{
	double val = 0;
	int len_u = len < t.size() ? len : t.size();
	for (int i = 0; i < len_u; ++i)
	{
		if (0 == i)
			val = t[i].data.dLastPrice;
		else
			val = MIN(val, t[i].data.dLastPrice);
	}
	return val;
}

double FABarMaker::Highest(VTick &t, int len)
{
	double val = 0;
	int len_u = len < t.size() ? len : t.size();
	for (int i = 0; i < len_u; ++i)
	{
		if (0 == i)
			val = t[i].data.dLastPrice;
		else
			val = MAX(val, t[i].data.dLastPrice);
	}
	return val;
}


int FABarMaker::Volumeest(VTick &t, int len)
{
	int val = 0;
	int len_u = len < t.size() ? len : t.size();
	for (int i = 0; i < len_u; ++i)
	{
		val += t[i].data.nVolume;
	}
	return val;
}


void FABarMaker::OnTick(tagMarketData * p)
{
	tagTick a;
	memcpy(&a, p, sizeof(tagMarketData));
	a.tickcount = time(0);//GetTickCount();
	m_vTick.push_back(a);
	CFABar Bar;
	if (SUCCESS == CreateBar(Bar))
	{
		m_pCreator->OnCreateBar(&Bar);
		//一个bar形成清空
		m_vTick.clear();
	}
}

int FABarMaker::CreateLastBar(CFABar &Bar)
{
	int i = m_vTick.size();

	if (i < 2)
		return FAIL;

	if (m_vTick[0].tickcount - m_vTick[i].tickcount < (MIN_TICK * m_nRound)/2)
		return FAIL;

	i = -1;

	Bar.open = m_vTick[i].data.dLastPrice;
	Bar.close = m_vTick[0].data.dLastPrice;
	Bar.low = Lowest(m_vTick, i);
	Bar.high = Highest(m_vTick, i);
	Bar.volume = Volumeest(m_vTick, i);
	Bar.tick_counter = i + 1;
	char t[33];
#ifdef WIN32
	itoa(m_vTick[i].tickcount, t, 10);
#else
	sprintf(t, "%d", m_vTick[i].tickcount);
#endif
	Bar.st = t;
#ifdef WIN32
	itoa(m_vTick[0].tickcount, t, 10);
#else
	sprintf(t, "%d", m_vTick[0].tickcount);
#endif
	Bar.et = t;
	m_nLastCreateBarTime = time(0);
	return SUCCESS;
	
}

int FABarMaker::CreateBar(CFABar &Bar)
{
	/************************************************************************/
	/*
	tick 转 candle的算法
	1. 简略算法，时间戳差值>=60s
	*/
	/************************************************************************/
	for (int i = 1; i < m_vTick.size(); i++)
	{
		if (m_vTick[0].tickcount - m_vTick[i].tickcount >= (MIN_TICK * m_nRound))
		{
			Bar.open = m_vTick[i].data.dLastPrice;
			Bar.close = m_vTick[0].data.dLastPrice;
			Bar.low = Lowest(m_vTick, i);
			Bar.high = Highest(m_vTick, i);
			Bar.volume = Volumeest(m_vTick, i);
			Bar.tick_counter = i + 1;
			char t[33];
			//itoa(m_vTick[i].tickcount,t,10);

#ifdef WIN32
			itoa(m_vTick[i].tickcount, t, 10);
#else
			sprintf(t, "%d", m_vTick[i].tickcount);
#endif

			Bar.st = t;
			//itoa(m_vTick[0].tickcount, t, 10);
#ifdef WIN32
			itoa(m_vTick[0].tickcount, t, 10);
#else
			sprintf(t, "%d", m_vTick[0].tickcount);
#endif
			Bar.et = t;
			m_nLastCreateBarTime = time(0);
			return SUCCESS;
		}
	}


	return FAIL;
}