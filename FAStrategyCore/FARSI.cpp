#include "FAStrategyCore/FARSI.h"

CFARSI::CFARSI()
:m_short_val(6)
,m_long_val(12)
{

}

void CFARSI::OnTick(tagMarketData *p, IIndexImpl*pIndex)
{
	CFAIndex::OnTick(p, pIndex);
}

void CFARSI::OnBar(CFABar *p, IIndexImpl*pIndex)
{
	CFAIndex::OnBar(p, pIndex);

	//// 每当有新bar时，计算各序列值。 注：新bar不参与运算
	double sFcactor_short = 1.0 / m_short_val;
	double sFcactor_long = 1.0 / m_long_val;
	if (m_U1.empty())
	{
		m_U1.push_back(0);
		m_D1.push_back(0);
		m_U2.push_back(0);
		m_D2.push_back(0);
		m_RSI1.push_back(0);
		m_RSI2.push_back(0);
	}
	else
	{
		double u = 0;
		double d = 0;
		if (Close[0] > Close[1])
			u = Close[0] - Close[1];
		else
			d = Close[1] - Close[0];

		m_U1.push_back(m_U1[0] + sFcactor_short * (u - m_U1[0]));
		m_D1.push_back(m_D1[0] + sFcactor_short * (d - m_D1[0]));
		double rs = m_U1[0] / m_D1[0];
		double RSI = 100 - 100 / (1 + rs);
		m_RSI1.push_back(RSI);

		m_U2.push_back(m_U2[0] + sFcactor_long * (u - m_U2[0]));
		m_D2.push_back(m_D2[0] + sFcactor_long * (d - m_D2[0]));
		rs = m_U2[0] / m_D2[0];
		RSI = 100 - 100 / (1 + rs);
		m_RSI2.push_back(RSI);
	}

	//// output for debug
	//stringstream ss;
	//ss << "RSI1:" << m_RSI1[0] << ", RSI2:" << m_RSI2[0] << ", st:" << BarQueue[0]->st << ", et:" << BarQueue[0]->et;
	//SEND_EVENTTRACE(ss.str(), WN_REPORT_LEVEL_DEBUG);

	pIndex->OnRSI(m_RSI1, m_RSI2);
}