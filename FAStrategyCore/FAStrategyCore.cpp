// FAStrategyCore.cpp : 定义 DLL 应用程序的导出函数。
//

#include "FAStrategyCore.h"

CFAStrategyCore * gStrategyCore = NULL;


// 这是已导出类的构造函数。
// 有关类定义的信息，请参阅 FAStrategyCore.h
CFAStrategyCore::CFAStrategyCore(IIndexImpl * p)
:m_pIndexImpl(p)
, m_pKDJ(0)
, m_pRSI(0)
, m_pMACD(0)
{
	m_pKDJ = new CFAKDJ();
	m_pRSI = new CFARSI();
	m_pMACD = new CFAMACD();
}

void CFAStrategyCore::OnBar(CFABar *p)
{
	if (m_pKDJ)
	{
		m_pKDJ->OnBar(p,m_pIndexImpl);
	}
}

void CFAStrategyCore::OnTick(tagMarketData *p)
{

	if (m_pKDJ)
	{
		m_pKDJ->OnTick(p,m_pIndexImpl);
	}
}



/************************************************************************/
/* 
*/
/************************************************************************/


FA_API IStrategyCore* CreateObject(IIndexImpl*p)
{
	if (!gStrategyCore)
	{
		gStrategyCore = new CFAStrategyCore(p);
	}
	return gStrategyCore;
}


FA_API int ReleaseObject(IStrategyCore*p)
{
	if (gStrategyCore)
	{
		delete gStrategyCore;
		gStrategyCore = NULL;
	}
	return 0;
}