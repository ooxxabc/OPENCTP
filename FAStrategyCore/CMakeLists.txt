## --------------------------------------------------------------------
## ykit FAStrategyCore
## --------------------------------------------------------------------


    set(SOURCES_files_SourceFiles
      dllmain.cpp
	  FAGVIX.cpp
      FAIndex.cpp
      FAKDJ.cpp
      FAIndex.cpp
      FAMACD.cpp
      FARSI.cpp
	  FAATR.cpp
      FAStrategyBase.cpp
      FAStrategyCore.cpp
      FAStrategyCore.def
      FAStrategyCore.h
	  FABoll.cpp
	  )
    source_group("Source Files" FILES ${SOURCES_files_SourceFiles})

    set(SOURCES_SourceFiles
      ${SOURCES_files_SourceFiles}
    )


  set(SOURCES_
    ${SOURCES_SourceFiles}
  )
  
INCLUDE_DIRECTORIES(../inc/)

  if(WIN32)	
   SET(CMAKE_CXX_FLAGS_DEBUG "/MDd /Z7 /Od")
   SET(CMAKE_CXX_FLAGS_RELEASE "/MD /O2")
   SET(CMAKE_CXX_FLAGS_MINSIZEREL "/MD /O2")
   SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "/MDd /Z7 /Od")
endif(WIN32)

#SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fpermissive")

SET(ProjectName FAStrategyCore)
SET(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})
LINK_DIRECTORIES(${CMAKE_SOURCE_DIR}/lib)

ADD_LIBRARY(${ProjectName} SHARED ${SOURCES_})
#ADD_LIBRARY(${ProjectName} STATIC ${SOURCES_})
Add_Definitions(-DFA_EXPORTS)
Add_Definitions(-DUNICODE -D_UNICODE)

TARGET_LINK_LIBRARIES(${ProjectName} FACore FAAlgorithm)

if(NOT WIN32)
add_definitions(-D_PORT_VERSION -Wno-deprecated  -fPIC)
if(CMAKE_CONFIGURATION_TYPES OR CMAKE_BUILD_TYPE STREQUAL "Release" OR "MinSizeRel" OR "RelWithDebInfo")
add_definitions(-DNDEBUG)
endif(CMAKE_CONFIGURATION_TYPES OR CMAKE_BUILD_TYPE STREQUAL "Release" OR "MinSizeRel" OR "RelWithDebInfo")
endif(NOT WIN32)
