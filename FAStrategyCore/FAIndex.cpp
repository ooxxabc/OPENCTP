#include "FAStrategyCore/FAIndex.h"


CFAIndex::CFAIndex()
{
	Open.m_arr = &(BarQueue);
	High.m_arr = &(BarQueue);
	Low.m_arr = &(BarQueue);
	Close.m_arr = &(BarQueue);
	New.m_arr = &(TickQueue);
}

void CFAIndex::OnBar(CFABar *p, IIndexImpl*pIndex)
{
	BarQueue.push_back(*p);
}

void CFAIndex::OnTick(tagMarketData *p, IIndexImpl*pIndex)
{
	TickQueue.push_back(*p);
}
