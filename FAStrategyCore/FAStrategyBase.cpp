#include "FAStrategyCore/FAStrategyBase.h"
//#include "FAStrategyCore/IStrategy.h"

/************************************************************************/
/* OrderKey                                                             */
/************************************************************************/

bool COrderKey::operator< (const COrderKey& rhs) const
{
	if (m_brokerID < rhs.m_brokerID)
		return true;
	if (m_brokerID > rhs.m_brokerID)
		return false;
	if (m_investorID < rhs.m_investorID)
		return true;
	if (m_investorID > rhs.m_investorID)
		return false;
	if (m_frontID < rhs.m_frontID)
		return true;
	if (m_frontID > rhs.m_frontID)
		return false;
	if (m_sessionID < rhs.m_sessionID)
		return true;
	if (m_sessionID > rhs.m_sessionID)
		return false;
	if (m_orderRef < rhs.m_orderRef)
		return true;
	if (m_orderRef > rhs.m_orderRef)
		return false;
	if (m_instrumentID < rhs.m_instrumentID)
		return true;
	if (m_instrumentID > rhs.m_instrumentID)
		return false;
	if (m_price < rhs.m_price)
		return true;
	if (m_price > rhs.m_price)
		return false;
	return false;
}

bool COrderKey::operator == (const COrderKey& rhs) const
{
	return m_brokerID == rhs.m_brokerID
		&& m_investorID == rhs.m_investorID
		&& m_frontID == rhs.m_frontID
		&& m_sessionID == rhs.m_sessionID
		&& m_orderRef == rhs.m_orderRef
		&& m_instrumentID == rhs.m_instrumentID
		&& m_price == rhs.m_price;
}