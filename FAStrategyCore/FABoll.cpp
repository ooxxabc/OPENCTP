#include "FAStrategyCore/FABoll.h"
#include "FAAlgorithm/FAAlgorithm.h"

CFABoll::CFABoll()
{
}

CFABoll::CFABoll(tagBollCfg &cfg)
:m_cfg(cfg)
{
}

CFABoll::~CFABoll()
{
}

double MA(BAR_QUEUE &t, int N)
{
	if(N > t.size() )
		return 0;

	double val = 0;

	for (int i = 0; i < N; ++i)
	{
		val += t[i].close;
	}
	val /= N;
	return val;
}

double STD(BAR_QUEUE &t, int M)
{
	double ma = MA(t, M);
	double std = 0;
	for (int i = 0; i < M; ++i)
	{
		std += (t[i].close - ma)*(t[i].close - ma);
	}
	return sqrt(std / M);
}


void CFABoll::OnBar(CFABar *p, IIndexImpl*pIndex)
{
	CFAIndex::OnBar(p, pIndex);

	tagBollValue v;
	v.dMid = MA(BarQueue, m_cfg.N);
	if (v.dMid==0)
	{
		return;
	}
	double dTmp2= STD(BarQueue, m_cfg.M);
	v.dUp = v.dMid + m_cfg.P * dTmp2;
	v.dDown = v.dMid - m_cfg.P * dTmp2;

	m_bollValues.push_back(v);

}
