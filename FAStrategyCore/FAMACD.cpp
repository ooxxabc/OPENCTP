#include "FAStrategyCore/FAMACD.h"

CFAMACD::CFAMACD()
:m_short(14)
, m_long(5)
, m_m(4)
{

}

void CFAMACD::OnTick(tagMarketData *p, IIndexImpl*pIndex)
{
	CFAIndex::OnTick(p, pIndex);
}

void CFAMACD::OnBar(CFABar *p, IIndexImpl*pIndex)
{
	CFAIndex::OnBar(p, pIndex);

	// 每当有新bar时，计算各序列值。 注：新bar不参与运算

	// ****************** m_EMAs_short ******************
	double sFcactor = 2.0 / (m_short + 1);
	if (m_EMAs_short.empty())
	{
		m_EMAs_short.push_back(Close[0]);
	}
	else
	{
		m_EMAs_short.push_back(m_EMAs_short[0] + sFcactor * (Close[0] - m_EMAs_short[0]));
	}

	// ****************** m_EMAs_long ******************
	sFcactor = 2.0 / (m_long + 1);
	if (m_EMAs_long.empty())
	{
		m_EMAs_long.push_back(Close[0]);
	}
	else
	{
		m_EMAs_long.push_back(m_EMAs_long[0] + sFcactor * (Close[0] - m_EMAs_long[0]));
	}

	// ****************** m_DIFFs ******************
	m_DIFFs.push_back(m_EMAs_short[0] - m_EMAs_long[0]);

	// ****************** m_DEAs ******************
	sFcactor = 2.0 / (m_m + 1);
	if (m_DEAs.empty())
	{
		m_DEAs.push_back(Close[0]);
	}
	else
	{
		m_DEAs.push_back(m_DEAs[0] + sFcactor * (m_DIFFs[0] - m_DEAs[0]));
	}

	// ****************** m_MACDs ******************
	m_MACDs.push_back(2 * (m_DIFFs[0] - m_DEAs[0]));

	//// output for debug
	//stringstream ss;
	//ss << "DIFF:" << m_DIFFs[0] << ", DEA:" << m_DEAs[0] << ", MACD:" << m_MACDs[0] << ", st:" << BarQueue[0]->st << ", et:" << BarQueue[0]->et;
	//SEND_EVENTTRACE(ss.str(), WN_REPORT_LEVEL_DEBUG);

	pIndex->OnMACD(m_MACDs);
}
