#include "FAStrategyCore/FAKDJ.h"

CFAKDJ::CFAKDJ()
:m_length(14)
, m_slowLength(5)
, m_smoothLength(4)
{
}

void CFAKDJ::OnTick(tagMarketData *p, IIndexImpl*pIndex)
{
	CFAIndex::OnTick(p,pIndex);
}


void CFAKDJ::OnBar(CFABar *p, IIndexImpl*pIndex)
{
	CFAIndex::OnBar(p,pIndex);

	//// 每当有新bar时，计算各序列值。 注：新bar不参与运算

	// ****************** m_RSV ******************
	m_RSV.push_back((Close[0] - Lowest<CLow>(Low, m_length)) / (Highest<CHigh>(High, m_length) - Lowest<CLow>(Low, m_length)) * 100);

	// ****************** m_K ******************
	double sFcactor = (double)(m_slowLength - 1) / m_slowLength;
	if (m_K.empty())
	{
		m_K.push_back(50); // 初始值取中间值50
	}
	else
	{
		m_K.push_back(m_RSV[0] + sFcactor * (m_K[0] - m_RSV[0]));
	}

	// ****************** m_D ******************
	sFcactor = (double)(m_smoothLength - 1) / m_smoothLength;
	if (m_D.empty())
	{
		m_D.push_back(50); // 初始值取中间值50
	}
	else
	{
		m_D.push_back(m_K[0] + sFcactor * (m_D[0] - m_K[0]));
	}

	// ****************** m_J ******************
	m_J.push_back(3 * m_K[0] - 2 * m_D[0]);

	/************************************************************************/
	/* 
		通知
	*/
	/************************************************************************/
	pIndex->OnKDJ(m_K, m_D, m_J);
}