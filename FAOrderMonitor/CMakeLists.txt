## --------------------------------------------------------------------
## ykit FAOrderMonitor
## --------------------------------------------------------------------


    set(SOURCES_files_SourceFiles
      FAOrderMonitor.cpp
      ../inc/FAOrderMonitor/FAOrderMonitor.h
    )
    source_group("Source Files" FILES ${SOURCES_files_SourceFiles})

    set(SOURCES_SourceFiles
      ${SOURCES_files_SourceFiles}
    )


  set(SOURCES_
    ${SOURCES_SourceFiles}
  )
  
INCLUDE_DIRECTORIES(../inc/)

  if(WIN32)	
   SET(CMAKE_CXX_FLAGS_DEBUG "/MDd /Z7 /Od")
   SET(CMAKE_CXX_FLAGS_RELEASE "/MD /O2")
   SET(CMAKE_CXX_FLAGS_MINSIZEREL "/MD /O2")
   SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "/MDd /Z7 /Od")
endif(WIN32)

SET(ProjectName FAOrderMonitor)
SET(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})
LINK_DIRECTORIES(${CMAKE_SOURCE_DIR}/lib)
Add_Definitions(-DUNICODE -D_UNICODE)

ADD_LIBRARY(${ProjectName} STATIC ${SOURCES_})
ADD_DEPENDENCIES(${ProjectName} FABase)
TARGET_LINK_LIBRARIES(${ProjectName} FABase)

if(NOT WIN32)
add_definitions(-D_PORT_VERSION -Wno-deprecated  -fPIC)
if(CMAKE_CONFIGURATION_TYPES OR CMAKE_BUILD_TYPE STREQUAL "Release" OR "MinSizeRel" OR "RelWithDebInfo")
add_definitions(-DNDEBUG)
endif(CMAKE_CONFIGURATION_TYPES OR CMAKE_BUILD_TYPE STREQUAL "Release" OR "MinSizeRel" OR "RelWithDebInfo")
endif(NOT WIN32)
