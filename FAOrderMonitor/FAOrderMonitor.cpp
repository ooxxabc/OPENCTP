
#include "FAOrderMonitor/FAOrderMonitor.h"

#define MONITOR_SLEEP (1)

CFAOrderMonitor::CFAOrderMonitor()
:m_platform(0)
, m_fSplippage(0)
, m_nCancelTime(0)
{
	m_v.clear();
}

void CFAOrderMonitor::Init(IStrategyPlatform*platform, const float &fSlippage, const int &nCancelTime)
{
	m_platform = platform;
	m_fSplippage = fSlippage;
	m_nCancelTime = nCancelTime;
}

void CFAOrderMonitor::OnOrder(tagOrder*pOrder)
{
	if (!pOrder) return;

	if (pOrder->nOrderSysID == 0) return;	//服务器还未处理

	//if (pOrder->nOrderStatus != TRADE_ORDER_STATUS_CANCELED && pOrder->nOrderStatus != TRADE_ORDER_STATUS_TRADED)
	//{
	//	return;
	//}

	CYKAutoMutex _m(&m_mutex);

	for (VOrderIter it = m_v.begin(); it!= m_v.end(); ++it)
	{
		if (*it== *pOrder)
		{
			//更新状态
			if (pOrder->nOrderStatus == TRADE_ORDER_STATUS_CANCELED || pOrder->nOrderStatus == TRADE_ORDER_STATUS_TRADED)// || pOrder->nOrderStatus == TRADE_ORDER_STATUS_UNKNOW)
			{
				//可以删除了
				m_v.erase(it);
				return;
			}
			else
			{
				*it = *pOrder;
				return;
			}
		}
	}

	m_v.push_back(*pOrder);
}

void CFAOrderMonitor::Start()
{
	CYKAutoThread th(new CYKThread(Monitor, this, 0));
	th->start();
}

void * CFAOrderMonitor::Monitor(const bool*isstop, void *param)
{
	//1分钟后开始监听
	printf("monitor will work next 1 minute...");
	Sleep(60 * 1000);

	CFAOrderMonitor * p = (CFAOrderMonitor*)param;
	if(!p || !p->m_platform || p->m_nCancelTime == 0) return 0;
	while (!*isstop)
	{
		CYKAutoMutex _m(&p->m_mutex);

		for (VOrderIter it = p->m_v.begin(); it != p->m_v.end(); ++it)
		{
			if ((*it).nTimestampCreate == 0)
			{
				//不是当前程序创建，则不计算
				continue;
			}
			
			if ((*it).nTimestampCreate + p->m_nCancelTime < time(0))
			{
				//cancel
				p->m_platform->OrderCancel(&(*it));

				//重报队列添加
				if (strcmp((*it).szSoftFlag,SOFT_FLAG)==0)
				{
					p->m_vRe.push_back((*it));
				}
			}
		}

		Sleep(MONITOR_SLEEP);
	}
	return 0;
}