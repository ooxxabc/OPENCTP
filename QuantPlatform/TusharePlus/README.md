#tusharePlus

对比同期上证指数

>1. 基于tushare的A股行情加载模块。
>>

>2.TuShare是一个免费、开源的python财经数据接口包。主要实现对股票等金融数据从数据采集、清洗加工 到 数据存储的过程，能够为金融分析人员提供快速、整洁、和多样的便于分析的数据，为他们在数据获取方面极大地减轻工作量，使他们更加专注于策略和模型的研究与实现上。考虑到Python pandas包在金融量化分析中体现出的优势，TuShare返回的绝大部分的数据格式都是pandas DataFrame类型，非常便于用pandas/NumPy/Matplotlib进行数据分析和可视化。当然，如果您习惯了用Excel或者关系型数据库做分析，您也可以通过TuShare的数据存储功能，将数据全部保存到本地后进行分析。应一些用户的请求，从0.2.5版本开始，TuShare同时兼容Python 2.x和Python 3.x，对部分代码进行了重构，并优化了一些算法，确保数据获取的高效和稳定。
>>http://www.waditu.cn/
>

>3.openctp进行行情load和ui渲染
>


#环境部署
>1,Anconda-4.4.0 版本；
>

>2.pip install backtrader
>

#tushare使用
>1. 获取历史交易数据
import tushare as ts
df = ts.get_hist_data('600848')
ts.get_hist_data('600848'，ktype='W') #获取周k线数据
ts.get_hist_data('600848'，ktype='M') #获取月k线数据
ts.get_hist_data('600848'，ktype='5') #获取5分钟k线数据
ts.get_hist_data('600848'，ktype='15') #获取15分钟k线数据
ts.get_hist_data('600848'，ktype='30') #获取30分钟k线数据
ts.get_hist_data('600848'，ktype='60') #获取60分钟k线数据
ts.get_hist_data('sh'）#获取上证指数k线数据，其它参数与个股一致，下同
ts.get_hist_data('sz'）#获取深圳成指k线数据 ts.get_hist_data('hs300'）#获取沪深300指数k线数据
ts.get_hist_data('sz50'）#获取上证50指数k线数据
ts.get_hist_data('zxb'）#获取中小板指数k线数据
ts.get_hist_data('cyb'）#获取创业板指数k线数据

获取历史分笔数据
df = ts.get_tick_data('000756','2015-03-27')
df.head(10)
获取实时分笔数据
df = ts.get_realtime_quotes('000581') 
print df[['code','name','price','bid','ask','volume','amount','time']]
返回值说明：
0：name，股票名字
1：open，今日开盘价
2：pre_close，昨日收盘价
3：price，当前价格
4：high，今日最高价
5：low，今日最低价
6：bid，竞买价，即“买一”报价
7：ask，竞卖价，即“卖一”报价
8：volumn，成交量 maybe you need do volumn/100
9：amount，成交金额（元 CNY）
10：b1_v，委买一（笔数 bid volume）
11：b1_p，委买一（价格 bid price）
12：b2_v，“买二”
13：b2_p，“买二”
14：b3_v，“买三”
15：b3_p，“买三”
16：b4_v，“买四”
17：b4_p，“买四”
18：b5_v，“买五”
19：b5_p，“买五”
20：a1_v，委卖一（笔数 ask volume）
21：a1_p，委卖一（价格 ask price）
...
30：date，日期
31：time，时间

保存为csv格式
import tushare as ts
df = ts.get_hist_data('000875')#直接保存
df.to_csv('c:/day/000875.csv')#选择保存
df.to_csv('c:/day/000875.csv',columns=['open','high','low','close'])
保存为Excel格式
df = ts.get_hist_data('000875')#直接保存
df.to_excel('c:/day/000875.xlsx')#设定数据位置（从第3行，第6列开始插入数据）
df.to_excel('c:/day/000875.xlsx', startrow=2,startcol=5)
保存为HDF5文件格式
df = ts.get_hist_data('000875')
df.to_hdf('c:/day/hdf.h5','000875')
保存为JSON格式
df = ts.get_hist_data('000875')
df.to_json('c:/day/000875.json',orient='records')

MySQL数据库
pandas提供了将数据便捷存入关系型数据库的方法，在新版的pandas中，主要是已sqlalchemy方式与数据建立连接，支持MySQL、Postgresql、Oracle、MS SQLServer、SQLite等主流数据库。本例以MySQL数据库为代表，展示将获取到的股票数据存入数据库的方法,其他类型数据库请参考sqlalchemy官网文档的create_engine部分。
from sqlalchemy import create_engine
import tushare as ts
df = ts.get_tick_data('600848',date='2014-12-22')
engine = create_engine('mysql://user:passwd@127.0.0.1/db_name?charset=utf8')
#存入数据库
df.to_sql('tick_data',engine)
#追加数据到现有表
#df.to_sql('tick_data',engine,if_exists='append')

存入MongoDB
import pymongo
import json
conn = pymongo.Connection('127.0.0.1', port=27017)
df = ts.get_tick_data('600848',date='2014-12-22')
conn.db.tickdata.insert(json.loads(df.to_json(orient='records')))