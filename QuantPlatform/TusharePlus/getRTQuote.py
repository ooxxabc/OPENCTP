import requests  
import numpy as np 
from matplotlib import pyplot as plt 
from matplotlib import animation 

fig = plt.figure(figsize=(8,6), dpi=72,facecolor="white")  
ax = plt.subplot(111)  
ax.set_title(u'sotck realtime quote')  
ax.set_xlabel('time')  
line,= ax.plot([], [], linewidth=1.5, linestyle='-')  
alldata = []  

def init():   
    line.set_data([], [])   
    return line, 

def animate(i):   
    x = range(i+1)  
    y = dapan('sh600183')  
    ax.set_xlim(0, i+10)   
    line.set_data(x, y)      
    return line, 


def dapan(code):  
    url = u'http://hq.sinajs.cn/?list='+code  
    r = requests.get(url)  
    print(type(r))    
    data = r.content[21:-3].decode('gbk').split(',') 
    alldata.append(float(data[3]))  
    ax.set_ylim(float(data[5]), float(data[4]))  
    return alldata  
    
def run():
    anim=animation.FuncAnimation(fig, animate, init_func=init,  frames=10000, interval=60000) 
    plt.grid()
    plt.show() 
    return 0

if __name__ == '__main__':
    run()