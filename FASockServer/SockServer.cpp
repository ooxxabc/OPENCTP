#include "FASockServer/SockServer.h"
#include "FASockServer/IServerImpl.h"

/*
	To test the library, include "SockServer.h" from an application project
	and call SockServerTest().
	
	Do not forget to add the library to Project Dependencies in Visual Studio.
*/

int32_t _port = 0;
IP_ADDRESS _ipAddress = IPV4;

extern "C" {
FA_API IServer* CreateObject(int port, IP_ADDRESS ipAddress, IServerHandle * pHandle)
{	
	SERVER->Init(pHandle);
//	SERVER->Create(port, ipAddress);
	_port = port;
	_ipAddress = ipAddress;
	return SERVER;
}

FA_API void Run()
{
	SERVER->Create(_port, _ipAddress);
}

FA_API void Release(IServer *pServer)
{
	if (pServer)
	{
		pServer->Release();
	}
}
}