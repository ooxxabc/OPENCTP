#pragma once
#include "FAMysql/ArchiveVector.h"
#include "ctpImpl/marketDataArchive.h"
#include "FABase/safe_vector.h"
#include "FABase/thread.h"
#include "dllHelper.h"

class CMSConnect;

typedef CYKSafeVector<tagMarketData> vMarketData;

class CFAMysql:public IMysql
{
public:
	explicit CFAMysql(IMysqlHandle *pHandel);

	virtual void Storge(tagMarketData*pData);

	virtual void Start();
	virtual void Stop();

	static void*Wirte(const bool *bStop,void *param);
	void _Write();

private:
	CMSConnect * m_pConnect;
	IMysqlHandle * m_pMysqlHandle;
	vMarketData		m_recv;
	CYKThread		*m_thread;
	bool			m_bStop;

};

extern "C"
{
	FA_API IMysql* CreateObject(IMysqlHandle*pHandle);
	FA_API void ReleaseObject();
}

extern CFAMysql * gFAMysql;