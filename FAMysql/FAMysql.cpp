#include "FAMysql.h"
#include "FABase/thread.h"
#include "MSConnect.h"
#include "ctpImpl/Quote_def.h"
#include "FABase/tools.h"

CFAMysql * gFAMysql = NULL;

FA_API IMysql* CreateObject(IMysqlHandle*pHandle)
{
	if (!gFAMysql)
	{
		gFAMysql = new CFAMysql(pHandle);
	}
	return gFAMysql;
}
FA_API void ReleaseObject()
{
	SAFE_DEL(gFAMysql);

}

/************************************************************************/
/*                                                                      */
/************************************************************************/

CFAMysql::CFAMysql(IMysqlHandle *pHandel)
	:m_pConnect(0)
	,m_pMysqlHandle(pHandel)
	,m_bStop(false)
{
	tagConfig cfg;
	strcpy(cfg.addr,"127.0.0.1");
	strcpy(cfg.user,"root");
	strcpy(cfg.pwd,"root");
	cfg.port = 3306;
	strcpy(cfg.db,"tt");
	m_pConnect = new CMSConnect(cfg);
}

void*CFAMysql::Wirte(const bool *bStop,void *param)
{
	CFAMysql * pThis = (CFAMysql*)param;
	while(!pThis->m_bStop)
	{

		pThis->_Write();

#ifdef WIN32
		Sleep(100);
#else
		usleep(1000);
#endif
	}

	return 0;
}
void CFAMysql::Stop()
{
	m_bStop = true;
}

void CFAMysql::Start()
{
	m_thread = new CYKThread(Wirte,this,false);
	m_thread->start();

	m_pConnect->Start(m_pMysqlHandle);

	//tagMarketData data;
	//data.dAskPrice1 = 1;
	//data.dBidPrice1 = 2;
	//sprintf(data.szINSTRUMENT,"RM709");
	//Storge(&data);
}

void CFAMysql::Storge(tagMarketData*pData)
{
	m_recv.put(*pData);

	//Class mylclassnew;
	//Class::FromJson(&mylclassnew, str);//反序列化完成
}

void CFAMysql::_Write()
{
	if (m_recv.size() == 0)
		return;

	tagMarketData data;
	if(0!=m_recv.get(data,1))
		return;

	//转码JSON
	tagMarketDataArchive arch;
	arch.Copy(&data);
	std::string json = arch.ToJson();
	char szSql[STR_SQL_LEN]={0};

	//make node
	tm t;
	CTools::GetTime(t);
	string node;
	char root[33]={0};
	arch.GetRootName(root);
	//GetHashNode(root,t,node);
	node.append(root);
	char sz[33]={0};
	sprintf(sz,"_%d",t.tm_year+1900);
	node.append(sz);
	unsigned int tt=CTools::GetTime();
	sprintf(szSql,"insert into %s(time,data)values(%u,'%s');",node.c_str(),tt,json.c_str());
	CArchiveResult ret;
	m_pConnect->DoSql(szSql,ret);

}