#include "MSConnect.h"

CMSConnect::CMSConnect(tagConfig&cfg)
	:m_pHandle(0)
{
	m_MySQL = new MYSQL;
	m_cfg = cfg;
}


CMSConnect::~CMSConnect(void)
{
}

void CMSConnect::Stop()
{
	mysql_close(m_MySQL);

	mysql_thread_end();
}

void CMSConnect::Start(IMysqlHandle*pHandle)
{
	m_pHandle = pHandle;

	m_bConnect = false;

	mysql_init(m_MySQL);

	my_bool my_true = true;
	// 设置重连
	mysql_options(m_MySQL, MYSQL_OPT_RECONNECT, &my_true); 

	if(mysql_real_connect(m_MySQL, m_cfg.addr, m_cfg.user,
		m_cfg.pwd, "", m_cfg.port, NULL, 0))
	{
		printf("Connection db server %s:%s success", m_cfg.addr,  m_cfg.user);

		mysql_select_db(m_MySQL, m_cfg.db);

		m_bConnect = true;

		if ( mysql_set_character_set( m_MySQL, "utf8" ) ) 
		{
			m_pHandle->OnErr(mysql_error( m_MySQL));
			//printf ("set character error, %s/n" , mysql_error( m_MySQL)) ;
		}
	}
	else
	{
		m_pHandle->OnErr(mysql_error( m_MySQL));
		//printf("Connection db server %s:%s error:%s", m_cfg.addr,m_cfg.db, mysql_error(m_MySQL));
	}	
}

void CMSConnect::DoSql(char*sql,CArchiveResult &result)
{
	if (!m_bConnect)
	{
		printf("DB connect error!");
		return;
	}

	unsigned int col = 0;
	MYSQL_RES *res_ptr;
	MYSQL_ROW sqlrow; 

	int res = mysql_real_query(m_MySQL, sql, strlen(sql));

	if (res != 0) 
	{
		const char * error = mysql_error(m_MySQL);
		unsigned int err = mysql_errno(m_MySQL);

		result << tagResult("int","1");
		//printf("%s", sql);
		//printf("mysql query error! %d, %s", res, error);
		m_pHandle->OnErr(error);
		return ;
	}
	else
	{
		result << tagResult("int","0");
		res_ptr = mysql_store_result(m_MySQL);

		if (res_ptr != NULL)
		{	
			col = mysql_num_fields(res_ptr);  

			wchar_t buff[1024];

			int rows = (int)mysql_num_rows(res_ptr);
			if (rows > 10000)
			{
				printf("pls use limit page!");
				printf("%s", sql);
				mysql_free_result(res_ptr);
				return;
			}

			while ((sqlrow = mysql_fetch_row(res_ptr))) 
			{
				for (int i = 0; i < col; ++i)
				{
					if (sqlrow[i] != NULL)
					{
						result << tagResult(res_ptr->fields[i].name,sqlrow[i]);

					}
					else
					{
						result << tagResult("char","NULL");
					}
				}	
			}

			if (mysql_errno(m_MySQL)) 
			{
				//printf("%s\n", sql);
				//printf("mysql query error! %d, %s\n", res, mysql_error(m_MySQL));
				m_pHandle->OnErr(mysql_error(m_MySQL));

			}
		}

		mysql_free_result(res_ptr);
	} 
}

void CMSConnect::DoTranscation(CTranscationList&list,CArchiveResult &result)
{
	if (!m_bConnect)
	{
		printf("DB connect error!");
		return;
	}

	MYSQL_RES *res_ptr;
	MYSQL_ROW sqlrow; 

	int nRes = 1;
	int nIndex = 0;
	result << tagResult("int","1");
	mysql_query(m_MySQL, "START TRANSACTION");			// 开始事务

	while(list.Size())
	{
		tagResult sql;
		list>>sql;
		if (strlen(sql.rowValue) <= 5)
		{	
			continue;
		}

		int res = mysql_real_query(m_MySQL, sql.rowValue, strlen(sql.rowValue));

		if (res != 0)
		{
			mysql_query(m_MySQL, "ROLLBACK");	//回滚

			//const char * error = mysql_error(m_MySQL);
			//printf("%s", sql.rowValue);
			//printf("mysql query error! error id:%d, %s", error);
			m_pHandle->OnErr(mysql_error(m_MySQL));

			return;
		}
		else
		{
			if (strchr(sql.rowValue, 's') == 0 || strchr(sql.rowValue, 'S') == 0)
			{
				res_ptr = mysql_store_result(m_MySQL);

				if (res_ptr != NULL)
				{	
					int nNum = mysql_num_fields(res_ptr);  // 每一行个数（即列数）
					int rows = (int)mysql_num_rows(res_ptr);
					if (rows > 10000)
					{
						mysql_query(m_MySQL, "ROLLBACK");	//回滚
						printf("pls use limit page!");
						printf("%s", sql);
						mysql_free_result(res_ptr);
						return;
					}


					wchar_t buff[512];

					while ((sqlrow = mysql_fetch_row(res_ptr)))   //获取每一行
					{
						for (int i = 0; i < nNum; ++i)
						{
							if (sqlrow[i] != NULL)
							{
								result <<tagResult(res_ptr->fields[i].name,sqlrow[i]);
							}
							else
							{
								result << tagResult("char","NULL");
							}
						}	
					}

					if (mysql_errno(m_MySQL)) 
					{
						//printf("%s\n", sql);
						//printf("mysql query error! %d, %s\n", res, mysql_error(m_MySQL));
						m_pHandle->OnErr(mysql_error(m_MySQL));

					}

					mysql_free_result(res_ptr);
				}
			}
			else if ((strchr(sql.rowValue, 'I')==0) || (strchr(sql.rowValue, 'i')==0))
			{
				// 增删改，需要对行数进行验证
				if (mysql_affected_rows(m_MySQL) == 0)
				{
					mysql_query(m_MySQL, "ROLLBACK");	//回滚
					result << tagResult("int","0");
					printf("%s", sql);
					printf("Can't exec the sql:%s", sql);
					return;
				}
			}
		}
	}

	mysql_query(m_MySQL, "COMMIT");			// 提交事务

	strcpy(result[0].rowValue,"0");
}
