#pragma once
#include "FAMysql/ArchiveVector.h"
#ifdef WIN32
#include "mysql/win/mysql.h"
//#pragma comment(lib,"libmysql.lib")
#else
#include "mysql/linux/mysql.h"
#endif

struct tagConfig 
{	
	char	addr[STR_CONTENT_LEN];
	char	user[STR_CONTENT_LEN];
	char	pwd[STR_CONTENT_LEN];
	int		port;
	char	db[STR_CONTENT_LEN];
};

typedef CArchiveResult CTranscationList;

class IMysqlHandle;

class CMSConnect
{
public:
	CMSConnect(tagConfig&cfg);
	~CMSConnect(void);
public:
	void Start(IMysqlHandle*pHandle=0);
	void Stop();
	bool IsConnected(){return m_bConnect;}
	void DoSql(char*sql,CArchiveResult &result);
	void DoTranscation(CTranscationList&list,CArchiveResult &result);
private:
	MYSQL		*m_MySQL;			// mysql���ݿ�
	bool		m_bConnect;
	tagConfig	m_cfg;
	IMysqlHandle* m_pHandle;
};

