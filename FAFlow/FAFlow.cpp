// FAFlow.cpp : 定义 DLL 应用程序的导出函数。
//

#include "FAFlow.h"


CFAFlow * m_pFlow = NULL;

extern "C"{

	FA_API IFlow* CreateObject()
	{
		m_pFlow = new CFAFlow();

		return m_pFlow;

	}

	FA_API int ReleaseObject(void)
	{
		if (m_pFlow)
		{
			delete m_pFlow;
			m_pFlow = 0;
		}
		return 0;
	}
}