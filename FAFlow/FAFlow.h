#pragma once

#include "FAFlow/FAFlow_def.h"
#include "FABase/thread.h"
#include "FABase/safe_vector.h"
#include "dllHelper.h"
using namespace std;

class  CFAFlow :public IFlow
{
public:
	CFAFlow();
	~CFAFlow(){}
private:
	CFAFlow& operator=(const CFAFlow&);
	CFAFlow(const CFAFlow&);
public:
	virtual void RegisterQueryTask(CTask*pTask);
	virtual void RegisterOrderTask(CTask*pTask);

	//virtual void RegisterAccountTask(CTask*pTask) { m_pAccountTask = pTask; }
	//virtual void RegisterPositionTask(CTask*pTask) { m_pPositionTask = pTask; }
	virtual void Start();
	virtual void Stop();

	bool IsContinue() { if (m_bContinue)return true; return false; }
	void SetContinue(bool con) { m_bContinue = con; }

public:
	static void *FlowQuery(const bool *stop, void *param);
	//static void *FlowOrder(const bool *stop, void *param);

	//static void *FlowAccount(const bool *stop, void *param);
	//static void *FlowPosition(const bool *stop, void *param);

private:
	CYKThread	*	m_pThreadQuery;			//查询线程
	//CYKThread	*	m_pThreadOrder;			//报单线程

	//CYKThread	*	m_pThreadAccount;		//账户权益线程
	//CYKThread	*	m_pThreadPosition;		//持仓线程
	//CTask		*	m_pAccountTask;			//账户任务
	//CTask		*	m_pPositionTask;		//持仓任务
	volatile bool	m_bContinue;
public:
	CYKSafeVector<CTask*>	m_vQueryTask;
	//CYKSafeVector<CTask*>	m_vOrderTask;
};

extern "C"{
	FA_API IFlow* CreateObject();
	FA_API int ReleaseObject(void);
}

extern CFAFlow * m_pFlow;
