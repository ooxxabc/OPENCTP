#include "FAFlow.h"

CFAFlow::CFAFlow()
	:m_bContinue(false)
	//,m_pAccountTask(0)
	//, m_pPositionTask(0)
{
	m_pThreadQuery = new CYKThread(FlowQuery, this,0);
	//m_pThreadOrder = new CYKThread(FlowOrder, this, 0);

	//m_pThreadAccount = new CYKThread(FlowAccount, this, 0);
	//m_pThreadPosition = new CYKThread(FlowPosition, this, 0);
}

void *CFAFlow::FlowQuery(const bool *stop, void *param)
{
	CFAFlow * p = (CFAFlow*)param;

	while (!(*stop))
	{
		CTask * obj = 0;
		if (p->IsContinue() && 0 == p->m_vQueryTask.get(obj, QUERY_FLOW_SLEEP))
		{
			if (obj->mtype == FLOW_EXIT)
			{
				break;
			}
			else if (obj->mtype == FLOW_ACCOUNT || obj->mtype == FLOW_POSITION)
			{
				obj->mpHandle->Handle();
			}
			else
			{
				obj->mpHandle->Handle();
				obj->mpHandle->Uninit();
				SAFE_DEL(obj->mpHandle);
				SAFE_DEL(obj);
			}

			p->SetContinue(false);
		}
		else
		{
			Sleep(QUERY_FLOW_SLEEP);
		}
	}

	return 0;
}

//void *CFAFlow::FlowOrder(const bool *stop, void *param)
//{
//	CFAFlow * p = (CFAFlow*)param;
//
//	while (!(*stop))
//	{
//		CTask* obj;
//		if (0 == p->m_vOrderTask.get(obj, ORDER_FLOW_SLEEP))
//		{
//			if (obj->mtype == FLOW_EXIT)
//			{
//				break;
//			}
//
//			obj->mpHandle->Handle();
//			obj->mpHandle->Uninit();
//			SAFE_DEL(obj->mpHandle);
//			SAFE_DEL(obj);
//
//		}
//
//		//Sleep(ORDER_FLOW_SLEEP);
//	}
//
//	return 0;
//}

//void *CFAFlow::FlowAccount(const bool *stop, void *param)
//{
//	CFAFlow * p = (CFAFlow*)param;
//
//	while (!(*stop))
//	{
//		CTask * obj = p->m_pAccountTask;
//		if (obj)
//		{
//			obj->mpHandle->Handle();
//			//obj->mpHandle->Uninit();
//			//SAFE_DEL(obj->mpHandle);
//			//SAFE_DEL(obj);
//		}
//		Sleep(QUERY_FLOW_SLEEP);
//	}
//
//	return 0;
//}

//void *CFAFlow::FlowPosition(const bool *stop, void *param)
//{
//	CFAFlow * p = (CFAFlow*)param;
//
//	while (!(*stop))
//	{
//		CTask * obj = p->m_pPositionTask;
//		if (obj)
//		{
//			obj->mpHandle->Handle();
//			//obj->mpHandle->Uninit();
//			//SAFE_DEL(obj->mpHandle);
//			//SAFE_DEL(obj);
//		}
//
//		Sleep(QUERY_FLOW_SLEEP);
//	}
//
//	return 0;
//}

void CFAFlow::RegisterQueryTask(CTask*pTask)
{
	m_vQueryTask.put(pTask);
}

void CFAFlow::RegisterOrderTask(CTask*pTask)
{
	//m_vOrderTask.put(pTask);
}


void CFAFlow::Start()
{
	SetContinue(true);

	m_pThreadQuery->start();
	//m_pThreadOrder->start();

	//m_pThreadAccount->start();
	//m_pThreadPosition->start();
}

void CFAFlow::Stop()
{
	m_pThreadQuery->stop();
	//m_pThreadOrder->stop();
	//m_pThreadAccount->stop();
}