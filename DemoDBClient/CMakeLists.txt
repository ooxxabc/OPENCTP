## --------------------------------------------------------------------
## ykit DemoDBClient
## --------------------------------------------------------------------


    set(SOURCES_files_SourceFiles
      CTPClient.cpp	  
      CTPClient.h	  
      DBSockClient.cpp	  
      DBSockClient.h	   
	  )
    source_group("Source Files" FILES ${SOURCES_files_SourceFiles})

    set(SOURCES_SourceFiles
      ${SOURCES_files_SourceFiles}
    )


  set(SOURCES_
    ${SOURCES_SourceFiles}
  )
    
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/inc)
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/inc/libevent)

  if(WIN32)	
   SET(CMAKE_CXX_FLAGS_DEBUG "/MDd /Z7 /Od")
   SET(CMAKE_CXX_FLAGS_RELEASE "/MD /O2")
   SET(CMAKE_CXX_FLAGS_MINSIZEREL "/MD /O2")
   SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "/MDd /Z7 /Od")
endif(WIN32)

SET(ProjectName DemoDBClient)

MESSAGE(STATUS "This is BINARY dir " ${${ProjectName}_BINARY_DIR}) 
MESSAGE(STATUS "This is SOURCE dir " ${${ProjectName}_SOURCE_DIR}) 
#SET(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})
LINK_DIRECTORIES(${CMAKE_SOURCE_DIR}/lib)
SET(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})

ADD_EXECUTABLE(${ProjectName} ${SOURCES_})
#Add_Definitions(-DSTRATEGYARBITRAGEA_EXPORTS)
Add_Definitions(-DUNICODE -D_UNICODE)

ADD_DEPENDENCIES(${ProjectName} FASockClient)
TARGET_LINK_LIBRARIES(${ProjectName} ws2_32 event FABase)

if(NOT WIN32)
add_definitions(-D_PORT_VERSION -Wno-deprecated  -fPIC)
if(CMAKE_CONFIGURATION_TYPES OR CMAKE_BUILD_TYPE STREQUAL "Release" OR "MinSizeRel" OR "RelWithDebInfo")
add_definitions(-DNDEBUG)
endif(CMAKE_CONFIGURATION_TYPES OR CMAKE_BUILD_TYPE STREQUAL "Release" OR "MinSizeRel" OR "RelWithDebInfo")
endif(NOT WIN32)
