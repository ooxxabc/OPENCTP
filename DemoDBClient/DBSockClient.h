#pragma once
#include "FASockClient/SockClient.h"
#include "dllHelper.h"
class CDBSockClient :public IClientHandle
{
public:
	CDBSockClient();
	~CDBSockClient();
public:
	virtual void Recv(bufferevent *bev, int32_t msgID, char *pData, int nLength);
	virtual void OnDisconnect();
	virtual void OnErr();
};

