#include <string>
#include <iostream>
#include <signal.h>
#include "FABase/thread.h"
#include "CTPClient.h"

using namespace std;

IClient * _pClient = 0;

typedef IClient* (*SockClientCreateObject)(char *ip, int port, IP_ADDRESS ipAddress, IClientHandle * pHandle);
typedef void(*SockClientRun)();

#ifdef WIN32
	#define SOCKCLIENT_SO ("FASockClient.dll")
#else
	#define SOCKCLIENT_SO ("FASockClient.so")
#endif

#define IP "127.0.0.1"
#define PORT 9999

CDBSockClient * ykClient = new CDBSockClient();
CDllHelperAuto dll(new CDllHelper(SOCKCLIENT_SO));
SockClientCreateObject	_func = 0;
SockClientRun			_funcRun = 0;	



void *SockThread(const bool *bStop, void *param)
{
	_func = dll.get()->GetProcedure<SockClientCreateObject>("CreateObject");	
	if (!_func)
		return 0;
	_funcRun = dll.get()->GetProcedure<SockClientRun>("Run");
	if (!_funcRun)
		return 0;
	_pClient = _func(IP, PORT, IPV4, ykClient);
	printf("[Info][%s]client connecting...\n",__FUNCTION__);
	_funcRun();

	return 0;
}

void Release()
{

}

int main(int argc, char *argv[])
{		
	InitSocket();
	//Init Sock
	CYKThread _thread(SockThread);
	_thread.start();

	printf("input any key to release\n");
	getchar();
	Release();
	return 0;

	return 0;
}