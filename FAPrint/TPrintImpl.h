#pragma once
#include "TPrint.h"
#include "FABase/thread_pool.h"
#include "FALog4cplus/FALogHelper.h"


class CYKPrint;

//#ifdef WIN32
//struct timezone
//{
//	int  tz_minuteswest; /* minutes W of Greenwich */
//	int  tz_dsttime;     /* type of dst correction */
//};
//
////int gettimeofday(struct ::timeval *tv, struct timezone *tz);
//#endif

class tagOutput :public CYKCounter
{
public:
	char mtype[LOG_TYPE];
	char minfo[MAX_LEN];
};

typedef tagOutput* LPOUTPUT;

typedef CYKAutoPoint<tagOutput> OutputAuto;

class PrintRunner :public IRunnable
{
public:
public:
	PrintRunner(LPOUTPUT p) :mp(p){}
	virtual int32_t operator()(const bool* isstopped, void* param = NULL);
	virtual ~PrintRunner() { }
private:
	OutputAuto mp;
};

class LogRunner :public IRunnable
{
public:
	LogRunner(LPOUTPUT p) :mp(p){}

public:
	virtual int32_t operator()(const bool* isstopped, void* param = NULL);
	virtual ~LogRunner() { }
private:
	OutputAuto mp;
};


//log4cplus
typedef  ILog* (*YKLogCreateObject)();
typedef  void(*YKLogReleaseObject)();
#define LOG_SO ("FALog4cplus.dll")

class CYKPrint :public IYKPrint{
public:
	CYKPrint(void);
	// TODO:  在此添加您的方法。
public:
	virtual void Stop();
	virtual void Log(int level, char *format, ...);
	virtual void Print(int level, char *format, ...);
private:
	CYKThreadPool	th;
};

extern ILog * m_pLog4cplus;