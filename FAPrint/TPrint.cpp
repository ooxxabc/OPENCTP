// TPrint.cpp : 定义 DLL 应用程序的导出函数。
//

#include "TPrint.h"
#include "TPrintImpl.h"

IYKPrint * p = 0; 

FA_API IYKPrint* CreateObject()
{
	if (!p)
		p = new CYKPrint();

	return p;
}

FA_API void ReleaseObject()
{
	if (p)
	{
		delete p;
		p = 0;
	}
}
