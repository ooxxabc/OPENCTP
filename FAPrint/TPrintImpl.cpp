#include "TPrintImpl.h"
#include "time.h"


//log4cplus
CDllHelper dll_log(LOG_SO);
#define CREATE_API ("CreateObject")
#define RELEASE_API ("ReleaseObject")
YKLogCreateObject _funcLogCreate = 0;
YKLogReleaseObject _funcLogRelease = 0;
ILog * m_pLog4cplus = 0;

#define LOG(msg)

#define PRINT(mtype,minfo,ntime) do{printf("[%s|%s,%d]%s----[%lld]\n",mtype,__FUNCTION__,__LINE__,minfo,ntime);}while(0)

#define OUTPUT(level, format, ...) \
	do {\
	printf("[%s|%s,%d] " format "\n", \
	level, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
	if (level == LOG_ERROR || level == LOG_INFO) {\
	char sz[MAX_LEN] = { '\0' }; \
	sprintf(sz, "[%s|%s,%d] " format, \
	level, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
		LOG(sz)}; \
	} while (0)

//////////////////////////////////////////////////////////////////////////


	inline int32_t gettimeofday(struct ::timeval *tv, struct timezone *tz)
	{
#ifndef WIN32
		return ::gettimeofday(tv, tz);
#else
		FILETIME ft;
		FILETIME _ft = { 0 };
		LARGE_INTEGER _pform = { 0 };

		unsigned __int64 tmpres = 0;
		unsigned __int64 tmpres2 = 0;
		static int tzflag = 0;
		LARGE_INTEGER pform = { 0 };
		QueryPerformanceCounter(&pform);

		if (NULL != tv)
		{
			GetSystemTimeAsFileTime(&ft);
			if (ft.dwLowDateTime == _ft.dwLowDateTime && ft.dwHighDateTime == _ft.dwHighDateTime)
			{
				//时间相等时，计算时间偏移
				LARGE_INTEGER pfreq = { 0 };
				QueryPerformanceFrequency(&pfreq);
				tmpres2 = ((pform.QuadPart - _pform.QuadPart) * 1000000UL) / pfreq.QuadPart;
			}
			else
			{
				//时间不等时，更新标准时间
				_ft = ft;
				QueryPerformanceCounter(&_pform);
			}

			tmpres |= ft.dwHighDateTime;
			tmpres <<= 32;
			tmpres |= ft.dwLowDateTime;

			tmpres /= 10;
			tmpres -= DELTA_EPOCH_IN_MICROSECS;
			tmpres += tmpres2;	//增加修正偏移


			tv->tv_sec = (long)(tmpres / 1000000UL);
			tv->tv_usec = (long)(tmpres % 1000000UL);
		}

		if (NULL != tz)
		{
			if (!tzflag)
			{
				_tzset();
				tzflag++;
			}

			tz->tz_minuteswest = _timezone / 60;
			tz->tz_dsttime = _daylight;
		}

		return 0;
#endif
	}


int32_t PrintRunner::operator()(const bool* isstopped, void* param)
{
	if (!mp.isValid())
	{
		return 0;
	}


	struct ::timeval tv = { 0, 0 };
	gettimeofday(&tv, NULL);
	uint64_t ms =  (uint64_t)tv.tv_sec * 1000 + tv.tv_usec / 1000;
	PRINT(mp.get()->mtype, mp->minfo,ms);

	return 0;
}

int32_t LogRunner::operator()(const bool* isstopped, void* param )
{
	if (!m_pLog4cplus)
		return 0;

	if (!mp.isValid())
	{
		return 0;
	}

	m_pLog4cplus->Log(LOG_INFO, mp->minfo);

	return 0;
}



//////////////////////////////////////////////////////////////////////////


CYKPrint::CYKPrint()
{
	th.init(2);

	_funcLogCreate = dll_log.GetProcedure<YKLogCreateObject>(CREATE_API);
	_funcLogRelease = dll_log.GetProcedure<YKLogReleaseObject>(RELEASE_API);
	printf("wait for init\n");
	//load so
	if (!_funcLogCreate)
	{
		printf("[%s]libLog open fail\n", __FUNCTION__);
	}

	m_pLog4cplus = _funcLogCreate();
	if (!m_pLog4cplus)
	{
		printf("%s,%d\n", __FUNCTION__, __LINE__);
	}
}

void CYKPrint::Log(int level, char *format,...)
{
#ifdef WIN32
	LPOUTPUT p = new tagOutput;
	va_list args;
	va_start(args, format);
	vsnprintf(p->minfo, MAX_LEN, format, args);
	va_end(args);

	sprintf(p->mtype, "%d", level);

	RunnableAutoPt task(new LogRunner(p));
	th.addTask(task);
#endif
}

void CYKPrint::Print(int level, char *format,...)
{	
#ifdef WIN32

	LPOUTPUT p = new tagOutput;
	va_list args;
	va_start(args, format);
	vsnprintf(p->minfo, MAX_LEN, format, args);
	va_end(args);

	sprintf(p->mtype, "%d", level);

	RunnableAutoPt task(new PrintRunner(p));
	th.addTask(task);
#endif

}

void CYKPrint::Stop()
{
	th.terminate();
}