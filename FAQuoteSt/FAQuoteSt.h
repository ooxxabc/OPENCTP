#pragma once

#include "FAQuoteSt/FAQuoteSt_def.h"
#include "dllHelper.h"
using namespace std;

class  CFAQuoteSt :public IFAQuoteSt
{
public:
	CFAQuoteSt();
	~CFAQuoteSt(){}
private:
	CFAQuoteSt& operator=(const CFAQuoteSt&);
	CFAQuoteSt(const CFAQuoteSt&);
public:
	virtual void DownStockData(char *szCode);
};

extern "C"{
	FA_API IFAQuoteSt* CreateObject();
	FA_API int ReleaseObject(void);
}

extern CFAQuoteSt * m_pFlow;
