// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "windows.h"


/*
	CTP基本流控制：主要针对Trade部分的查询： 1s/笔
	1.login
	2.settlement
	3.instrument_detail
	4.position
	5.order
*/

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

