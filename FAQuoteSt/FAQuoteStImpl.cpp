#include "FAQuoteSt.h"

#include <boost/python.hpp>
#include <boost/detail/lightweight_test.hpp>
#include <boost/bind.hpp>
#include <iostream>
#include <sstream>


using namespace boost::python;

CFAQuoteSt::CFAQuoteSt()
{
}

void CFAQuoteSt::DownStockData(char *szCode)
{
	Py_Initialize();

	object main_module = import("__main__");
	object main_namespace = main_module.attr("__dict__");
	char sz[128];
	sprintf(sz,"import tushare as ts\ndf=ts.get_hist_data('%s')\nszFile='%s'+'.csv'\ndf.to_csv(szFile)\n",szCode,szCode);
	exec(sz,main_namespace);
	
	//Py_Finalize();
}

//#include "module_tail.cpp"