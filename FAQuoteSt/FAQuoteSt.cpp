// FAFlow.cpp : 定义 DLL 应用程序的导出函数。
//

#include "FAQuoteSt.h"


CFAQuoteSt * m_pFlow = NULL;

extern "C"{

	FA_API IFAQuoteSt* CreateObject()
	{
		m_pFlow = new CFAQuoteSt();

		return m_pFlow;

	}

	FA_API int ReleaseObject(void)
	{
		if (m_pFlow)
		{
			delete m_pFlow;
			m_pFlow = 0;
		}
		return 0;
	}
}

