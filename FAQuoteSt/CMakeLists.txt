## --------------------------------------------------------------------
## ykit FAQuoteSt
## --------------------------------------------------------------------

	cmake_minimum_required(VERSION 2.6)
	PROJECT(COQuant_Impl)
    set(BUILD_USE_64BITS on)
    
    set(SOURCES_files_SourceFiles
      dllmain.cpp
      FAQuoteSt.cpp
	  FAQuoteSt.def
      FAQuoteSt.h	  
      FAQuoteStImpl.cpp
	  module_tail.cpp
    )
    source_group("Source Files" FILES ${SOURCES_files_SourceFiles})

    set(SOURCES_SourceFiles
      ${SOURCES_files_SourceFiles}
    )


  set(SOURCES_
    ${SOURCES_SourceFiles}
  )
  
INCLUDE_DIRECTORIES(../inc/ "E:\boost_1_65_1" "C:\Program Files (x86)\Windows Kits\10\Include\10.0.15063.0\ucrt" "C:\Anaconda3\include" )
LINK_DIRECTORIES("E:\boost_1_65_1\stage\lib" "C:\Program Files (x86)\Windows Kits\10\Lib\10.0.15063.0\ucrt\x64" "C:\Anaconda3\libs")

  if(WIN32)	
   SET(CMAKE_CXX_FLAGS_DEBUG "/MDd /Z7 /Od")
   SET(CMAKE_CXX_FLAGS_RELEASE "/MD /O2")
   SET(CMAKE_CXX_FLAGS_MINSIZEREL "/MD /O2")
   SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "/MDd /Z7 /Od")
endif(WIN32)

SET(ProjectName FAQuoteSt)
SET(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})

ADD_LIBRARY(${ProjectName} SHARED ${SOURCES_})
Add_Definitions(-DFA_EXPORTS)
Add_Definitions(-DUNICODE -D_UNICODE)

#TARGET_LINK_LIBRARIES(${ProjectName} FABase)

if(NOT WIN32)
add_definitions(-D_PORT_VERSION -Wno-deprecated  -fPIC)
if(CMAKE_CONFIGURATION_TYPES OR CMAKE_BUILD_TYPE STREQUAL "Release" OR "MinSizeRel" OR "RelWithDebInfo")
add_definitions(-DNDEBUG)
endif(CMAKE_CONFIGURATION_TYPES OR CMAKE_BUILD_TYPE STREQUAL "Release" OR "MinSizeRel" OR "RelWithDebInfo")
endif(NOT WIN32)
