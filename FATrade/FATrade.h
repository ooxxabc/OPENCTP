#pragma once
#include "FATrade/FATrade_Def.h"
#include "ctpImpl/CTPTradeApi.h"
#include "ctpImpl/CTPTradeSpi.h"
#include "dllHelper.h"

//#ifdef _DEBUG
//#include "vld.h"
//#endif


//#ifdef FATRADE_EXPORTS
//#define FATRADE_API __declspec(dllexport)
//#else
//#define FATRADE_API __declspec(dllimport)
//#endif
//
// 此类是从 FATrade.dll 导出的
class CFATrade :public IFATrade{
public:
	CFATrade(void){}
	explicit CFATrade(ITradeEvent*pEvent);
	~CFATrade(){}
	// TODO:  在此添加您的方法。

	virtual int Connect(char *address);
	virtual int Login(char*broker, char *account, char*pwd);
	virtual int Logout();
	virtual int DisConnect();

	virtual int Order(const char* pszInstrumentID, enTradeType nTradeType,\
		enTradeDir nTradeDir, enTradeOperate nTradeOperate, enTradeOrderType nOrderType, \
		double dPrice, int nVolume, char* pszOrderRefSuffix,int &nRequestID);
	virtual int CancelOrder(tagOrder *pOrder);
	virtual int QueryPosition();
	virtual int QuerySettlement();
	virtual int ComfirmSettlement();	

	virtual int OnLoginSuccess(int nSessionID, int nFrontID, int nOrderRef);
	virtual int OnLoginFail();
	virtual int OnDisconnect();

	virtual int QueryAccountDetail();
	virtual int QueryInstrumentDetail(char*ins[], int count);
	
private:

	CCTPTradeSpi	m_tradeSpi;
	CCTPTradeApi	m_tradeApi;
};

FA_API IFATrade* CreateObject(ITradeEvent*pEvent);
FA_API int ReleaseObject(void);