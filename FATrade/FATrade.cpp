// FATrade.cpp : 定义 DLL 应用程序的导出函数。
//

#include "FATrade.h"

CFATrade * _pTrade = NULL;

CFATrade::CFATrade(ITradeEvent*pEvent)
{
	m_tradeSpi.SetITrade(pEvent);
	return;
}

int CFATrade::Connect(char *address)
{
	return true == m_tradeApi.Init((CThostFtdcTraderSpi*)&m_tradeSpi, address);
}
int CFATrade::Login(char*broker, char *account, char*pwd)
{
	return true == m_tradeApi.Login(broker,account,pwd);
}
int CFATrade::Logout()
{
	return 0;
}
int CFATrade::DisConnect()
{
	m_tradeApi.Fini();
	return 0;
}
int CFATrade::Order(const char* pszInstrumentID, enTradeType nTradeType, enTradeDir nTradeDir, \
	enTradeOperate nTradeOperate, enTradeOrderType nOrderType, double dPrice,\
	int nVolume, char* pszOrderRefSuffix,int&nRequestID)
{
	bool ret = m_tradeApi.OrderInsert(pszInstrumentID, nTradeType, nTradeDir, \
		nTradeOperate, nOrderType, dPrice, nVolume, pszOrderRefSuffix, nRequestID);
	//return ret ? 1:0;
	return nRequestID;
}
int CFATrade::CancelOrder(tagOrder *pOrder)
{
	tagCancelOrderInsert c;
	c.lOrderLocalID = pOrder->nOrderID;
	c.lOrderSysID = pOrder->nOrderSysID;
	strcpy_s(c.szINSTRUMENT, sizeof(pOrder->szINSTRUMENT), pOrder->szINSTRUMENT);
	strcpy_s(c.szExchangeID, sizeof(pOrder->szExchangeID), pOrder->szExchangeID);
	strcpy_s(c.OrderRef, sizeof(pOrder->szOrderRefCustom), pOrder->szOrderRefCustom);
	c.nSession = pOrder->nSessionID;
	m_tradeApi.OrderAction(&c);
	return 0;
}


int CFATrade::QueryPosition()
{
	m_tradeApi.QueryPosition();
	return 0;
}
int CFATrade::QuerySettlement()
{
	m_tradeApi.QuerySettlementConfirm();	//查询结算单
	return 0;
}
int CFATrade::ComfirmSettlement()
{
	m_tradeApi.ConfirmSettlement();
	return 0;
}

int CFATrade::OnLoginSuccess(int nSessionID, int nFrontID, int nOrderRef)
{
	m_tradeApi.SetSessionID(nSessionID, nFrontID, nOrderRef);

	//QuerySettlement();

	return 0;
}
int CFATrade::OnLoginFail()
{
	m_tradeApi.SetSessionID(0, 0, 0);
	return 0;
}
int CFATrade::OnDisconnect()
{
	m_tradeApi.SetSessionID(0, 0, 0);
	return 0;
}

int CFATrade::QueryAccountDetail()
{
	m_tradeApi.QueryTradingAccount();
	return 0;
}

int CFATrade::QueryInstrumentDetail(char*ins[], int count)
{
	if (count == 0)
	{
		//全市场查询
		m_tradeApi.QueryInstrument(0);
		return 0;
	}


	for(int i=0;i<count;i++)
		m_tradeApi.QueryInstrument(ins[i]);

	return 0;
}

/*

*/

FA_API IFATrade* CreateObject(ITradeEvent*pEvent)
{
	if (!_pTrade)
		_pTrade = new CFATrade(pEvent);

	return _pTrade;
}

FA_API int ReleaseObject(void)
{
	if (_pTrade)
	{
		_pTrade->DisConnect();
		delete _pTrade;
		_pTrade = NULL;
	}

	return 0;
}
