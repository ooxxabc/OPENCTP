/*
Copyright © Bubi Technologies Co., Ltd. 2017 All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
		 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef WIN32
#include <sys/prctl.h>
#endif

#include "thread.h"

#ifdef WIN32
const HANDLE OCUtils::Thread::INVALID_HANDLE = NULL;
#else
const pthread_t OCUtils::Thread::INVALID_HANDLE = (pthread_t)-1;
#endif

#ifdef WIN32
DWORD WINAPI OCUtils::Thread::threadProc(LPVOID param)
#else
void *OCUtils::Thread::threadProc(void *param)
#endif
{
	Thread *this_thread = reinterpret_cast<Thread *>(param);

	this_thread->Run();
	this_thread->thread_id_ = 0;

#ifdef WIN32
	CloseHandle(this_thread->handle_);
#endif // WIN32

#ifdef WIN32
	_endthreadex(0);
	return 0;
#else
	return NULL;
#endif
}

#undef ReadLockGuard
#undef WriteLockGuard

bool OCUtils::Thread::Start(std::string name) {
	name_ = name;
	if (kInit != state_) {
		return false;
	}

	bool result = false;
#ifdef WIN32
	handle_ = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)threadProc, (LPVOID)this, 0, (LPDWORD)&thread_id_);
	result = (NULL != handle_);
#else
	int ret = pthread_create(&handle_, NULL, threadProc, (void *)this);
	result = (0 == ret);
	thread_id_ = handle_;
#endif
	state_ = kStart;
	enabled_ = true;
	return result;
}

OCUtils::Thread::Thread(){
	state_ = kInit;
	enabled_ = false;
	handle_ = INVALID_HANDLE;
	thread_id_ = 0;
}

OCUtils::Thread::Thread(Runnable *target){
    target_ = target;
	state_ = kInit;
    enabled_ = false;
    handle_ = INVALID_HANDLE;
    thread_id_ = 0;
}

OCUtils::Thread::~Thread() {
	state_ = kStop;
}

bool OCUtils::Thread::enabled() const { 
	return enabled_; 
}

size_t OCUtils::Thread::thread_id() const { 
	return thread_id_; 
}

bool OCUtils::Thread::IsRunning() const { 
	return state_ == kStart; 
}

bool OCUtils::Thread::IsObjectValid() const { 
	return Thread::INVALID_HANDLE != handle_; 
}

const std::string &OCUtils::Thread::GetName() const {
	return name_; 
}

bool OCUtils::Thread::Stop() {
	if (!IsObjectValid() || !enabled_) {
		return false;
	}

	enabled_ = false;
	return true;
}


bool OCUtils::Thread::Terminate() {
	if (kStop == state_ || kInit == state_) {
		return true;
	}

	bool result = true;
#ifdef WIN32
	if (0 == ::TerminateThread(handle_, 0)) {
		result = false;
	}
#else
	if (0 != pthread_cancel(thread_id_)) {
		result = false;
	}
#endif
	if (result) {
		state_ = kStop;
	}

	enabled_ = false;

	return result;
}

bool OCUtils::Thread::JoinWithStop() {
	if (kStart != state_) {
		return false;
	}

	enabled_ = false;

	bool result = false;

#ifdef WIN32
	if (INVALID_HANDLE != handle_) {
		DWORD ret = ::WaitForSingleObject(handle_, INFINITE);
		if (WAIT_OBJECT_0 == ret || WAIT_ABANDONED == ret) {
			result = true;
			handle_ = NULL;
		}
	}
#else
	if (INVALID_HANDLE != handle_) {
		int ret = pthread_join(handle_, NULL);
		if (0 == ret) {
			result = true;
			handle_ = INVALID_HANDLE;
		}
	}
#endif
	state_ = kJoined;
	return result;
}

void OCUtils::Thread::Run() {
	assert(target_ != NULL);

	SetCurrentThreadName(name_);

	target_->Run(this);
}

bool OCUtils::Thread::SetCurrentThreadName(std::string name) {
#ifdef WIN32
	//not supported
	return true;
#else
	return 0 == prctl(PR_SET_NAME, name.c_str(), 0, 0, 0);
#endif //WIN32
}

size_t OCUtils::Thread::current_thread_id() {
#ifdef WIN32
	return (size_t)::GetCurrentThreadId();
#else
	return (size_t)pthread_self();
#endif
}

OCUtils::ThreadGroup::ThreadGroup() {}

OCUtils::ThreadGroup::~ThreadGroup() {
	JoinAll();
	for (size_t i = 0; i < threads_.size(); ++i) {
		delete threads_[i];
	}
	threads_.clear();
}

void OCUtils::ThreadGroup::AddThread(Thread *thread) {
	threads_.push_back(thread);
}

void OCUtils::ThreadGroup::StartAll() {
	for (size_t i = 0; i < threads_.size(); ++i) {
		threads_[i]->Start();
	}
}

void OCUtils::ThreadGroup::JoinAll() {
	for (size_t i = 0; i < threads_.size(); ++i) {
		threads_[i]->JoinWithStop();
	}
}

void OCUtils::ThreadGroup::StopAll() {
	for (size_t i = 0; i < threads_.size(); ++i) {
		threads_[i]->Stop();
	}
}

size_t OCUtils::ThreadGroup::size() const {
	return threads_.size();
}

OCUtils::Mutex::Mutex()
	: thread_id_(0) {
#ifdef WIN32
	InitializeCriticalSection(&mutex_);
#else
	pthread_mutexattr_t mattr;
	pthread_mutexattr_init(&mattr);
	pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&mutex_, &mattr);
	pthread_mutexattr_destroy(&mattr);
#endif
}

OCUtils::Mutex::~Mutex() {
#ifdef WIN32
	DeleteCriticalSection(&mutex_);
#else
	pthread_mutex_destroy(&mutex_);
#endif
}

void OCUtils::Mutex::Lock() {
#ifdef WIN32
	EnterCriticalSection(&mutex_);
#ifdef _DEBUG
	thread_id_ = static_cast<uint32_t>(GetCurrentThreadId());
#endif
#else
	pthread_mutex_lock(&mutex_);
#ifdef _DEBUG
	thread_id_ = static_cast<uint32_t>(pthread_self());
#endif
#endif
}

void OCUtils::Mutex::Unlock() {
#ifdef _DEBUG
	thread_id_ = 0;
#endif
#ifdef WIN32
	LeaveCriticalSection(&mutex_);
#else
	pthread_mutex_unlock(&mutex_);
#endif
}

pthread_mutex_t *OCUtils::Mutex::mutex_pointer() { 
	return &mutex_;
}

OCUtils::MutexGuard::MutexGuard(Mutex &mutex):mutex_(mutex) {
	mutex_.Lock();
}

OCUtils::MutexGuard::~MutexGuard() {
	mutex_.Unlock();
}

OCUtils::ReadWriteLock::ReadWriteLock()
	: reads_(0) {}

OCUtils::ReadWriteLock::~ReadWriteLock() {}

void OCUtils::ReadWriteLock::ReadLock() {
	enterLock_.Lock();
	AtomicInc(&reads_);
	enterLock_.Unlock();
}

void OCUtils::ReadWriteLock::ReadUnlock() {
	AtomicDec(&reads_);
}

void OCUtils::ReadWriteLock::WriteLock() {
	enterLock_.Lock();
	while (reads_ > 0) {
		Sleep(0);
	}
}

OCUtils::WriteLockGuard::WriteLockGuard(ReadWriteLock &lock): lock_(lock) {
	lock_.WriteLock();
}

OCUtils::WriteLockGuard::~WriteLockGuard() { 
	lock_.WriteUnlock(); 
}

OCUtils::ReadLockGuard::ReadLockGuard(ReadWriteLock &lock): lock_(lock) {
	lock_.ReadLock();
}

OCUtils::ReadLockGuard::~ReadLockGuard() { 
	lock_.ReadUnlock();
}

void OCUtils::ReadWriteLock::WriteUnlock() {
	enterLock_.Unlock();
}

OCUtils::SpinLock::SpinLock() :m_busy(SPINLOCK_FREE) {
}

OCUtils::SpinLock::~SpinLock() {
}

void OCUtils::SpinLock::Lock() {
	while (SPINLOCK_BUSY == BUBI_CAS(&m_busy, SPINLOCK_BUSY, SPINLOCK_FREE)) {
		BUBI_YIELD();
	}
}

void OCUtils::SpinLock::Unlock() {
	BUBI_CAS(&m_busy, SPINLOCK_FREE, SPINLOCK_BUSY);
}

size_t OCUtils::ThreadPool::Size() const {
	return threads_.size(); 
}

OCUtils::Semaphore::Semaphore(int32_t num) {
#ifdef _WIN32
	sem_ = ::CreateSemaphore(NULL, num, LONG_MAX, NULL);
#else
	sem_init(&sem_, 0, num);
#endif    
}

OCUtils::Semaphore::~Semaphore() {
#ifdef _WIN32
	if (NULL != sem_) {
		if (0 != ::CloseHandle(sem_)) {
			sem_ = NULL;
		}
	}
#else
	sem_destroy(&sem_);
#endif    
}

bool OCUtils::Semaphore::Wait(uint32_t millisecond) {
#ifdef _WIN32
	if (NULL == sem_)
		return false;

	DWORD ret = ::WaitForSingleObject(sem_, millisecond);
	if (WAIT_OBJECT_0 == ret || WAIT_ABANDONED == ret) {
		return true;
	}
	else {
		return false;
	}
#else
	int32_t ret = 0;

	if (kInfinite == millisecond) {
		ret = sem_wait(&sem_);
	}
	else {
		struct timespec ts = { 0, 0 };
		//TimeUtil::getAbsTimespec(&ts, millisecond);

		ts.tv_sec = millisecond / 1000;
		ts.tv_nsec = millisecond % 1000;

		ret = sem_timedwait(&sem_, &ts);
	}

	return -1 != ret;
#endif
}

bool OCUtils::Semaphore::Signal() {
#ifdef _WIN32
	BOOL ret = FALSE;

	if (NULL != sem_) {
		ret = ::ReleaseSemaphore(sem_, 1, NULL);
	}
	return TRUE == ret;
#else
	return -1 != sem_post(&sem_);
#endif
}

OCUtils::ThreadTaskQueue::ThreadTaskQueue() {}

OCUtils::ThreadTaskQueue::~ThreadTaskQueue() {}

int OCUtils::ThreadTaskQueue::PutFront(Runnable *task) {
	int ret = 0;
	spinLock_.Lock();
	if (task) tasks_.push_front(task);
	ret = tasks_.size();
	spinLock_.Unlock();
	return ret;
}

int OCUtils::ThreadTaskQueue::Put(Runnable *task) {
	int ret = 0;
	spinLock_.Lock();
	if (task) tasks_.push_back(task);
	ret = tasks_.size();
	spinLock_.Unlock();
	return ret;
}


int OCUtils::ThreadTaskQueue::Size() {
	int ret = 0;
	spinLock_.Lock();
	ret = tasks_.size();
	spinLock_.Unlock();
	return ret;
};

OCUtils::Runnable *OCUtils::ThreadTaskQueue::Get() {
	Runnable *task = NULL;
	spinLock_.Lock();
	if (tasks_.size() > 0) {
		task = tasks_.front();
		tasks_.pop_front();
	}
	spinLock_.Unlock();
	return task;
}

OCUtils::ThreadPool::ThreadPool() : enabled_(false) {}

OCUtils::ThreadPool::~ThreadPool() {
	for (size_t i = 0; i < threads_.size(); i++) {
		if (threads_[i]) delete threads_[i];
	}
}

bool OCUtils::ThreadPool::Init(int threadNum) {

	enabled_ = true;
	AddWorker(threadNum);
	return enabled_;
}

bool OCUtils::ThreadPool::Exit() {
	enabled_ = false;
	for (size_t i = 0; i < threads_.size(); i++) {
		if (threads_[i]) threads_[i]->JoinWithStop();
	}

	return true;
}

void OCUtils::ThreadPool::AddTask(Runnable *task) {
	tasks_.Put(task);
}

void OCUtils::ThreadPool::JoinwWithStop() {
	enabled_ = false;
	for (ThreadVector::const_iterator it = threads_.begin(); it != threads_.end(); ++it) {
		(*it)->JoinWithStop();
	}
	threads_.clear();
}

bool OCUtils::ThreadPool::WaitAndJoin() {

	while (tasks_.Size() > 0)
		Sleep(1);

	enabled_ = false;
	for (size_t i = 0; i < threads_.size(); i++) {
		if (threads_[i]) threads_[i]->JoinWithStop();
	}

	return true;
}

void OCUtils::ThreadPool::Terminate() {
	for (ThreadVector::const_iterator it = threads_.begin(); it != threads_.end(); ++it) {
		(*it)->Terminate();
	}
}

void OCUtils::ThreadPool::AddWorker(int threadNum) {
	for (int i = 0; i < threadNum; ++i) {

		Thread *thread = new Thread(this);
		threads_.push_back(thread);
		thread->Start();
	}
}

void OCUtils::ThreadPool::Run(Thread *this_thread) {
	while (enabled_) {
		OCUtils::Runnable *task = tasks_.Get();
		if (task) task->Run(this_thread);
		else OCUtils::Sleep(1);
	}
}

