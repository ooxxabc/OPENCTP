#include "cdwStrategy.h"
#include "tinyxml/tinyxml.h"

CStrategyPlusACfg::CStrategyPlusACfg()
{
}


CStrategyPlusACfg::~CStrategyPlusACfg()
{
}

int CStrategyPlusACfg::Load()
{
	TiXmlDocument xmlDoc;
	if (!xmlDoc.LoadFile(PLUS_CFG_XML, TIXML_ENCODING_UTF8))
		return FAIL;

	TiXmlElement * node = xmlDoc.FirstChildElement("StrategyCDW");
	if (!node)
		return FATAL;

	TiXmlElement *PlusIndex = node->FirstChildElement("PlusIndex");
	if (!PlusIndex)
		return FATAL;
	if (!PlusIndex->Attribute("key"))
		return FAIL;
	m_cfg.nPlusIndex = atoi(PlusIndex->Attribute("key"));

	TiXmlElement *node1 = node->FirstChildElement("Instrument");
	if (!node1)
		return FATAL;

	if (!node1->Attribute("num"))
		return FAIL;
	m_cfg.nInsCount = atoi(node1->Attribute("num"));
	string sz(node1->Attribute("num"));
	string sz2(node1->Attribute("Ins1"));
	for (int i = 0; i < m_cfg.nInsCount && i < PLATFORM_INS_MAX_COUNT; i++)
	{
		char sz[33];
		sprintf_s(sz, 33, "Ins%d", i + 1);
		sprintf_s(m_cfg.szIns[i], INS_ID_LENGTH, node1->Attribute(sz));
	}

	node1 = node->FirstChildElement("Open");
	if (!node1)
		return FATAL;
	m_cfg.tOpen.nPosition = atoi(node1->Attribute("position"));
	m_cfg.tOpen.fJumpPoint = atof(node1->Attribute("jump"));
	m_cfg.tOpen.nPointRMB = atoi(node1->Attribute("PointRMB"));
	m_cfg.tOpen.fSlippage = atof(node1->Attribute("Slippage"));
	m_cfg.tOpen.nCancelTime = atoi(node1->Attribute("CancelTime"));

	node1 = node->FirstChildElement("Cover");
	if (!node1)
		return FATAL;
	m_cfg.tCover.nWin = atoi(node1->Attribute("win"));
	m_cfg.tCover.nLose = atoi(node1->Attribute("lose"));

	return SUCCESS;
}

char **CStrategyPlusACfg::GetIns(char *p[])
{
	for (size_t i = 0; i < m_cfg.nInsCount; i++)
	{
		p[i] = m_cfg.szIns[i];
	}

	return p;
}

//////////////////////////////////////////////////////////////////////////


CcdwStrategy::CcdwStrategy()
: m_pPlatform(0)
, m_print(0)
, m_pCDW(0)
,m_pPostion(0)
{
}

CcdwStrategy::CcdwStrategy(IStrategyPlatform*pPlatform)
:m_pPlatform(pPlatform)
, m_print(0)
, m_pCDW(0)
{
}

int CcdwStrategy::Init(IYKPrint *print,IPosition *pPos)
{
	m_print = print;
	m_cfg.Load();
	char *p[INS_MAX_COUNT] = { 0 };
	m_cfg.GetIns(p);
	int nCount = m_cfg.GetInsCount();
	if (!m_pPlatform)
		return FAIL;

	memset(&m_tickDataLatest, 0, sizeof(m_tickDataLatest));

	m_pPlatform->SubInstrument(p, nCount);

	m_pPostion = pPos;
		
	return SUCCESS;
}

int CcdwStrategy::OnQuote(tagMarketData * pTick)
{
	//对第一个合约进行头寸操作
	if (m_pCDW && strcmp(pTick->szINSTRUMENT, m_cfg.GetCfg().szIns[0]) == 0)
	{
		//回调给UI进行渲染
		m_pCDW->OnQuote(pTick);		

		m_tickDataLatest = *pTick;
	}

	return SUCCESS;
}


int CcdwStrategy::OnAccountDetail(tagTradingAccount*pAccount)
{
	if (m_pCDW)
	{
		m_pCDW->OnAccountDetail(pAccount);
	}
	return 0;
}

int CcdwStrategy::ClearPositionDetail()
{
	m_pCDW->ClearPositionDetail();
	return 0;
}

int CcdwStrategy::OnPositionDetail(tagPosition*pPos, bool bLast)
{
	//是我关注的合约吗？
	if (m_pCDW && strcmp(pPos->szINSTRUMENT, m_cfg.GetCfg().szIns[0]) == 0)
	{
		//回调给UI进行渲染
		m_pCDW->OnPositionDetail(pPos,bLast);
	}
	return 0;
}

float CcdwStrategy::Price(ORDER_TYPE type,bool delayTest)
{
	float Slippage = 0;	//滑点
	float price = 0;	//对手价

	switch (type)
	{
	case BUY:
		price = m_tickDataLatest.dAskPrice1;
		Slippage =  delayTest?0 - m_cfg.GetCfg().tOpen.fSlippage :m_cfg.GetCfg().tOpen.fSlippage;
		break;
	case SELLSHORT:
		price = m_tickDataLatest.dBidPrice1;
		Slippage = delayTest ? m_cfg.GetCfg().tOpen.fSlippage : 0 - m_cfg.GetCfg().tOpen.fSlippage;
		break;
	case SELL:
		price = m_tickDataLatest.dBidPrice1;
		Slippage = delayTest ? m_cfg.GetCfg().tOpen.fSlippage : 0 - m_cfg.GetCfg().tOpen.fSlippage;
		break;
	case BUYTOCOVER:
		price = m_tickDataLatest.dAskPrice1;
		Slippage = delayTest ? 0 - m_cfg.GetCfg().tOpen.fSlippage : m_cfg.GetCfg().tOpen.fSlippage;
		break;
	}
	
	return price + Slippage;
}

int CcdwStrategy::Order(ORDER_TYPE type)
{
	switch (type)
	{	
	case BUY:
	case SELLSHORT:
	case SELL:
	case BUYTOCOVER:
	{
		//根据配置文件仓位上限
		if (!CanNewOrder(type))
		{
			break;
		}

		tagOrderDelegation order;
		sprintf(order.szIns, "%s", m_cfg.GetCfg().szIns[0]);
		order.price = Price(type,/*true*/false);
		order.volume = m_cfg.GetCfg().tOpen.nPosition;
		order.typePrice = LIMIT;
		order.typeOrder = type;
		m_pPlatform->Order(order,m_pPostion);
		break;

	}
	case LOCK:
	{
		//获得当前持仓
	}
		break;
	case CLEAR:
	{
		////获得当前持仓
		//VPosition vp = m_pPlatform->GetPosition();
		//for (VPositionIter it = vp.begin(); it != vp.end(); ++it)
		//{
		//	if (strcmp(m_cfg.GetCfg().szIns[0], it->szINSTRUMENT) != 0)
		//	{
		//		continue;
		//	}


		//	tagOrderDelegation order;
		//	sprintf(order.szIns, "%s", m_cfg.GetCfg().szIns[0]);

		//	switch(it->nTradeDir)
		//	{
		//	case TRADE_DIR_BUY:
		//		break;
		//	case TRADE_DIR_SELL:
		//		break;
		//	}

		//	if (it->nYesterdayPosition > 0)
		//	{
		//	}
		//	
		//	if (it->nTodayPosition > 0)
		//	{

		//	}

		//	order.price = Price(type,/*true*/false);
		//	order.volume = m_cfg.GetCfg().tOpen.nPosition;
		//	order.typePrice = LIMIT;
		//	order.typeOrder = type;
		//	m_pPlatform->Order(order,m_pPostion);
		//}
	}
		break;
	case OPPOSE:
	{
		////仅1手的时候可以反手
		//VPosition vp = m_pPlatform->GetPosition();
		//if (vp.size() == 1)
		//{
		//	VPositionIter it = vp.begin();
		//}
	}
		break;

	}

	return 0;
}

int CcdwStrategy::Cancel(int orderID)
{
	if (orderID == 0)
	{
		//即撤单上个委托，如上个已经撤单，则不处理
	}

	return 0;
}


bool CcdwStrategy::CanNewOrder(ORDER_TYPE type)
{
	

	return false;
}
