// 下列 ifdef 块是创建使从 DLL 导出更简单的
// 宏的标准方法。此 DLL 中的所有文件都是用命令行上定义的 CDWSTRATEGY_EXPORTS
// 符号编译的。在使用此 DLL 的
// 任何其他项目上不应定义此符号。这样，源文件中包含此文件的任何其他项目都会将
// CDWSTRATEGY_API 函数视为是从 DLL 导入的，而此 DLL 则将用此宏定义的
// 符号视为是被导出的。
//#ifdef CDWSTRATEGY_EXPORTS
//#define CDWSTRATEGY_API __declspec(dllexport)
//#else
//#define CDWSTRATEGY_API __declspec(dllimport)
//#endif
//
#include "StrategyPlatform/StrategyPlatform_Def.h"
#include "FAStrategyCore/IStrategyCore_Def.h"


#define PLUS_CFG_XML	_FTA("StrategyPlus.xml")

struct tagOpen
{
	int		nPosition;
	float	fJumpPoint;
	int		nPointRMB;
	float	fSlippage;
	int		nCancelTime;

};

struct tagCover
{
	int		nWin;
	int		nLose;
};

struct tagPlusCfgA
{
	int nPlusIndex;		//策略编号，全局唯一
	int nInsCount;
	char szIns[PLATFORM_INS_MAX_COUNT][INS_ID_LENGTH];
	tagOpen tOpen;
	tagCover tCover;
};

class CStrategyPlusACfg
{
public:
	CStrategyPlusACfg();
	~CStrategyPlusACfg();

	tagPlusCfgA&GetCfg() { return m_cfg; }

	char **GetIns(char *p[]);
	int GetInsCount(){ return m_cfg.nInsCount; }

public:
	int Load();
	int  ReLoad();

private:
	tagPlusCfgA m_cfg;
};


class ICDW:public IHandle
{
public:
	virtual void OnQuote(tagMarketData*pData) = 0;
	virtual void OnAccountDetail(tagTradingAccount*pAccount) = 0;
	virtual void OnPositionDetail(tagPosition*pPos, bool bLast) = 0;
	virtual void ClearPositionDetail() = 0;

};


//策略实例
class CcdwStrategy :public IStrategyPlus{
public:
	CcdwStrategy();
	CcdwStrategy(IStrategyPlatform*pPlatform);
	// TODO:  在此添加您的方法。

	virtual int Init(IYKPrint * p = 0,IPosition *pPos=0);
	virtual int UnInit() { return SUCCESS; }
	virtual int OnQuote(tagMarketData * pTick);
	virtual int OnOrderStatus() { return SUCCESS; }
	virtual int OnTradeStatus() { return SUCCESS; }
	virtual int OnAccountDetail(tagTradingAccount*pAccount);
	virtual int OnPositionDetail(tagPosition*pPos, bool bLast);
	virtual int ClearPositionDetail();

	virtual int Order(ORDER_TYPE type);
	virtual int Cancel(int orderID = 0);

	
	//保留方法
	virtual int SetHandle(IHandle * p1) { m_pCDW = (ICDW*)p1; return 0; }
	
public:
	float Price(ORDER_TYPE type,bool delayTest=false);
	bool CanNewOrder(ORDER_TYPE type);
private:
	CStrategyPlusACfg m_cfg;
	IStrategyPlatform * m_pPlatform;
	IStrategyCore * m_pCore;
private:
	tagMarketData		m_tickDataLatest;		//最新的tick
	IYKPrint		 *	m_print;
	ICDW			 *	m_pCDW;
	IPosition		 *  m_pPostion;

};

FA_API IStrategyPlus* CreateObject(IStrategyPlatform*pPlatform);
FA_API int ReleaseObject(void);

extern CcdwStrategy * _pPlusA;