#include "FASockClient/IClientImpl.h"
#ifdef WIN32
	#include <io.h>
#endif

void cmd_msg_cb(int fd, short events, void* arg)
{
	char msg[1024];

	int ret = read(fd, msg, sizeof(msg));
	if (ret < 0)
	{
		perror("read fail ");
		exit(1);
	}

	struct bufferevent* bev = (struct bufferevent*)arg;

	////把终端的消息发送给服务器端
	//bufferevent_write(bev, msg, ret);
	
	
	char msg2[SOCK_BUFFER];
	HEAD head;
	head.id = 0;
	head.len = ret;
	memcpy(msg2, &head, sizeof(head));
	if (ret > 0)
		memcpy(msg2 + sizeof(head), msg, ret);
	bufferevent_write(bev, msg2, sizeof(head)+ret);

}


void server_msg_cb(struct bufferevent* bev, void* arg)
{
	//char msg[1024];
	//size_t len = bufferevent_read(bev, msg, sizeof(msg));
	//msg[len] = '\0';

	int32_t len = 0;
	int32_t alen = evbuffer_get_length(OUTPUT_BUFFER(bev));

	HEAD head;
	char msg[SOCK_BUFFER];
	len = bufferevent_read(bev, &head, sizeof(head));
	alen -= len;

	while (len == sizeof(head))
	{
		bool handle = false;
		if (head.len == 0)
		{
			handle = true;
		}
		else
		{
			len = bufferevent_read(bev, msg, head.len * sizeof(char));
			if (len == head.len)
			{
				handle = true;
			}
		}


		if (handle == true)
		{
			CLIENT->GetHandle()->Recv(bev, head.id, msg, head.len);
//			printf("recv %s from server[0]\n", msg);
		}

		len = bufferevent_read(bev, &head, sizeof(head));
		alen -= len;

		if (alen <= 0)
		{
			return;
		}
	}

//	printf("recv %s from server[1]\n", msg);
}


void event_cb(struct bufferevent *bev, short event, void *arg)
{

	if (event & BEV_EVENT_EOF)
		printf("connection closed\n");
	else if (event & BEV_EVENT_ERROR)
		printf("some other error\n");
	else if (event & BEV_EVENT_CONNECTED)
	{
		printf("the client has connected to server\n");
		return;
	}

	//这将自动close套接字和free读写缓冲区
	bufferevent_free(bev);

	struct event *ev = (struct event*)arg;
	event_free(ev);
}


//////////////////////////////////////////////////////////////////////////
IClientImpl::IClientImpl()
		: m_pHandle(0)
{
	
}

IClientImpl::IClientImpl(IClientHandle * pHandle)
		: m_pHandle(pHandle)
{
}


IClientImpl::~IClientImpl()
{
}

void IClientImpl::Connect(char *ip, int port, IP_ADDRESS ipAddress)
{
	/*client 暂支持ipv4*/

	if (ipAddress != IPV4)
	{
		printf("[Err][%s,%d]\n", __FUNCTION__, __LINE__);
		return;
	}

	struct event_base *base = event_base_new();

	struct bufferevent* bev = bufferevent_socket_new(base,
		-1,
		BEV_OPT_CLOSE_ON_FREE);

	//监听终端输入事件
	struct event* ev_cmd = 0;/*event_new(base,
		0,//STDIN_FILENO,		
		EV_READ | EV_PERSIST,
		cmd_msg_cb,
		(void*)bev);*/

	event_add(ev_cmd, NULL);

	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);
#ifndef WIN32
	inet_aton(ip, &server_addr.sin_addr);
#else
	server_addr.sin_addr.s_addr = inet_addr(ip);

#endif
	bufferevent_socket_connect(bev,
		(struct sockaddr *)&server_addr,
		sizeof(server_addr));
	
	bufferevent_setcb(bev, server_msg_cb, NULL, event_cb, (void*)ev_cmd);
	bufferevent_enable(bev, EV_READ | EV_PERSIST);

	event_base_dispatch(base);
}
int IClientImpl::Release()
{
	return SUCCESS;
}

void IClientImpl::Init(IClientHandle * pHandle)
{
	m_pHandle = pHandle;
}

void IClientImpl::Send(bufferevent *bev, int32_t msgID, char *pData, int nLength)
{
	char msg[SOCK_BUFFER];
	HEAD head;
	head.id = msgID;
	head.len = nLength;
	memcpy(msg, &head, sizeof(head));
	if (nLength > 0)	
		memcpy(msg + sizeof(head), pData, nLength);
	bufferevent_write(bev, msg, sizeof(head) + nLength);
}
