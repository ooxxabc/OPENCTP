#include "FASockClient/SockClient.h"
#include "FASockClient/IClientImpl.h"

/*
	To test the library, include "SockClient.h" from an application project
	and call SockClientTest().
	
	Do not forget to add the library to Project Dependencies in Visual Studio.
*/

char		_ip[33];
int32_t		_port = 0;
IP_ADDRESS _ipAddress = IPV4;

extern "C"{
FA_API IClient * CreateObject(char *ip, int port, IP_ADDRESS ipAddress, IClientHandle * pHandle)
{
	sprintf(_ip, "%s", ip);
	_port = port;
	_ipAddress = ipAddress;
	CLIENT->Init(pHandle);
	return CLIENT;
}

FA_API void Run()
{
	CLIENT->Connect(_ip, _port, _ipAddress);	
}

FA_API void ReleaseObject(IClient * pClient)
{
	if (pClient)
	{
		pClient->Release();
	}
}
}