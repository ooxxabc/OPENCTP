#include "FASqliteFormate/YKSqliteFormate.h"
#ifdef WIN32
#include <windows.h>
#include <xstring>
#else
#include <string>
#endif
#include <assert.h>
#include "FABase/date.h"
#include "FAPrint/TPrint_def.h"

#define QUOTE_SLEEP 100


/************************************************************************/
/*
RM 交易时间
*/
/************************************************************************/
const char *szTradeTime[] = { "09:00", "10:15", "10:30", "11:30", "13:30", "15:00", "21:00", "23:30" };
const int nTime[] = { 9, 0, 10, 15,
10, 30, 11, 30,
13, 30, 15, 0,
21, 0, 23, 30 };


CTradeTime::CTradeTime()
{
	m_vTime.clear();
	for (int i = 0; i < 4; i++)
	{
		tagTradeTime time;
		time.tBegin.nH = nTime[i * 4];
		time.tBegin.nM = nTime[i * 4 + 1];
		time.tEnd.nH = nTime[i * 4 + 2];
		time.tEnd.nM = nTime[i * 4 + 3];
		m_vTime.push_back(time);
	}
}

bool CTradeTime::IsTradeTime(char *szTime)
{
	string st(szTime);
	st = st.substr(9, 8);
	string h = st.substr(0, 2);
	string m = st.substr(3, 2);
	int nH = atoi(h.c_str());
	int nM = atoi(m.c_str());
	tagT t(nH, nM);
	for (int i = 0; i < m_vTime.size(); i++)
	{
		if (m_vTime[i].IsTradeTime(t))
		{
			return true;
		}
	}

	return false;
}

bool CTradeTime::IsTradeSysTime()
{
	char sz[33];
	Date date(time(0));
	sprintf(sz, "%s", date.toString().c_str());
	return IsTradeTime(sz);
}

//////////////////////////////////////////////////////////////////////////

CSqliteQuoteData::CSqliteQuoteData()
:m_nRound(MIN_15)
, m_pSql(0)
, m_bInit(0)
, m_pLog(0)
{
	m_szTableName[0] = 0;
	m_szTickTableName[0] = 0;
	m_szIns[0] = 0;
}

CSqliteQuoteData::~CSqliteQuoteData()
{
}

void CSqliteQuoteData::Init(ILog * pLog, ISqlite *pSql, int nRound, char*szTableName, char *szIns, char*szTickTableName)
{	
	if (m_bInit) return;
	assert(nRound <= 30 && nRound >= 1);
	assert(szTableName != 0);
	m_pSql = pSql;
	m_nRound = nRound;
	sprintf(m_szTableName, "%s", szTableName);
	sprintf(m_szIns, "%s", szIns);
	if (szTickTableName && strlen(szTickTableName) > 1)
		sprintf(m_szTickTableName, "%s", szTickTableName);
	m_barM.Init(this, m_nRound);
	m_bInit = true;
	m_pLog = pLog;
	m_pTh = new CYKThread(_Save, this, 0);
	m_pTh->start();
}

void CSqliteQuoteData::Storge(tagMarketData*pData)
{
	if (!m_pSql || !m_bInit) return;
	if (!TRADE_TIME->IsTradeTime(pData->szUpdateTime))
		return;
	if (!TRADE_TIME->IsTradeSysTime())
		return;
	if (strcmp(m_szIns, pData->szINSTRUMENT) != 0)
		return;

	if (strlen(m_szTickTableName) >1)
	{
		char szSql[128] = { 0 };
		sprintf(szSql, "insert into %s(time,ask,bid,lastPrice) values(%d,%.1f,%.1f,%.1f)", m_szTickTableName, (int)time(0), (double)pData->dAskPrice1, pData->dBidPrice1, pData->dLastPrice);
		//sprintf(szSql, "insert into %s(time,ask,bid,lastPrice) values(%d,%f,%f,%f)", m_szTickTableName, (int)time(0), (double)pData->dAskPrice1, pData->dBidPrice1, pData->dLastPrice);
		tagSql sql(szSql);
		m_vSql.put(sql);
		//int nErrCode = 0;
		//if (!m_pSql->Execute(szSql, 0, 0, nErrCode))
		//{
		//	printf("[tick]sql insert err:%d\n", nErrCode);
		//	if (m_pLog)
		//		m_pLog->Log(LOG_ERROR, szSql);
		//	OUTPUT2(LOG_ERROR, "%s[errCode:%d]", szSql, nErrCode);
		//}
	}
	m_barM.OnTick(pData);
}

void CSqliteQuoteData::OnCreateBar(CFABar *p)
{
	//加入sqlite
	char szSql[128] = { 0 };
	Date da(atoi(p->st.c_str()));	
	sprintf(szSql, "insert into %s(time,open,close,low,high,creattime) values(%d,%.1f,%.1f,%.1f,%.1f,'%s')", m_szTableName, atoi(p->st.c_str()), p->open, p->close, p->low, p->high, da.toString().c_str());
	tagSql sql(szSql);
	m_vSql.put(sql);
	/*int nErrCode = 0;
	if (!m_pSql->Execute(szSql, 0, 0, nErrCode))
	{
	printf("sql insert err:%d\n", nErrCode);
	if (m_pLog)
	m_pLog->Log(LOG_ERROR, szSql);
	OUTPUT2(LOG_ERROR, "%s[errCode:%d]", szSql, nErrCode);
	}*/
}

void * CSqliteQuoteData::_Save(const bool*isstop, void *param)
{
	CSqliteQuoteData * p = (CSqliteQuoteData*)param;

	while (!*isstop)
	{
		tagSql sql;
		if (0==p->m_vSql.get(sql,1000) && strlen(sql.szSql) > 1)
		{
			int nErrCode = 0;
			if (!p->m_pSql->Execute(sql.szSql, 0, 0, nErrCode))
			{
				printf("sql insert err:%d\n", nErrCode);
				if (p->m_pLog)
					p->m_pLog->Log(LOG_ERROR, sql.szSql);
				OUTPUT2(LOG_ERROR, "%s[errCode:%d]", sql.szSql, nErrCode);
			}
		}

		Sleep(QUOTE_SLEEP);
	}

	return 0;
}