#include "stdafx.h"
#include "QuoteDlg.h"

void CQuoteDlg::OnQuoteConnected()
{
	_pQuote->Login(BROKER_ID, ACCOUNT, PASSWORD);
}


void CQuoteDlg::OnQuoteUserLoginSuccess()
{
	char * sz[] = { INS_NAME };
	_pQuote->SubIns(sz, 1);
}
void CQuoteDlg::OnQuoteUserLoginFailed(int nErrorID, const char* pszMsg)
{

}
void CQuoteDlg::OnQuoteDisconnected(int nReason)
{

}
void CQuoteDlg::OnQuoteError(int nErrorID, const char* pszMsg)
{

}
void CQuoteDlg::OnQuoteSubMarketDataSuccess(char* pszInstrumentID)
{

}
void CQuoteDlg::OnQuoteSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg)
{

}
void CQuoteDlg::OnQuoteMarketData(tagMarketData* pstMarketData)
{
	if (theQuoteView)
		theQuoteView->PushTick(*pstMarketData);
}
void CQuoteDlg::OnQuoteUnSubMarketDataSuccess(char* pszInstrumentID)
{

}
void CQuoteDlg::OnQuoteUnSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg)
{

}