#include "stdafx.h"
#include "ResourceProtect.h"

CResourceProtect::CResourceProtect(CString sDllName)
{
	m_sDllName = sDllName;
	m_hClientExe = AfxGetResourceHandle();
	m_hDll = AfxLoadLibrary(sDllName);
	AfxSetResourceHandle(m_hDll);
}
CResourceProtect::~CResourceProtect(void)
{
	AfxSetResourceHandle(m_hClientExe);
	AfxFreeLibrary(m_hDll);
}