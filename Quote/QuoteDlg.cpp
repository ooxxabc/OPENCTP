
// QuoteDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Quote.h"
#include "QuoteDlg.h"
#include "afxdialogex.h"
#include "dllHelper.h"
#include "ResourceProtect.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CQuoteDlg 对话框

//////////////////////////////////////////////////////////////////////////


#ifdef _DEBUG
CDllHelper _dllQuote("FAQuoteD.dll");
#else
CDllHelper _dllQuote("FAQuote.dll");
#endif

typedef IFAQuote* (*CreateQuote)(IQuoteEvent*pEvent);
CreateQuote _funcQuote= NULL;
IFAQuote * _pQuote= NULL;
typedef void* (*ReleaseQuote)();
ReleaseQuote _funcQuote2= NULL;

//////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
CDllHelper _dll("QuoteUID.dll");
#else
CDllHelper _dll("QuoteUI.dll");
#endif

typedef CQuoteView* (*CreateQuoteView)(HWND hwnd);
CreateQuoteView _func = NULL;
typedef void* (*ReleaseQuoteUI)();
ReleaseQuoteUI _func2 = NULL;
CQuoteView * theQuoteView = NULL;


CQuoteDlg::CQuoteDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CQuoteDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CQuoteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CQuoteDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CQuoteDlg 消息处理程序

BOOL CQuoteDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	//ShowWindow(SW_MINIMIZE);

	// TODO:  在此添加额外的初始化代码
	//////////////////////////////////////////////////////////////////////////
	//设置全屏窗口
	int offset = 20;
	int w = GetSystemMetrics(SM_CXSCREEN) * 3 / 4; //屏幕宽度
	int h = GetSystemMetrics(SM_CYSCREEN) * 3 / 4 ; //屏幕高度
	SetWindowPos(&wndTop, offset, offset, w, h, SWP_NOMOVE);
	GetDlgItem(IDC_QUOTE)->MoveWindow(0, 0, w, h);

	//////////////////////////////////////////////////////////////////////////
	CResourceProtect resourceProtect(L"QuoteUID.dll");

	//////////////////////////////////////////////////////////////////////////
	_funcQuote = _dllQuote.GetProcedure<CreateQuote>("CreateObject");
	if (_funcQuote)
	{
		_pQuote = _funcQuote(this);
	}
	int ret = _pQuote->Connect(URL_QUOTE);
	if (ret != SUCCESS)
	{
		return TRUE;
	}
	_funcQuote2 = _dllQuote.GetProcedure<ReleaseQuote>("ReleaseObject");

	//////////////////////////////////////////////////////////////////////////
	_func = _dll.GetProcedure<CreateQuoteView>("CreateObject");
	if (_func)
	{
		theQuoteView = _func(GetDlgItem(IDC_QUOTE)->GetSafeHwnd());
	}
	_func2 = _dll.GetProcedure<ReleaseQuoteUI>("ReleaseObject");


	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CQuoteDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CQuoteDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CQuoteDlg::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值

	__super::OnClose();
}


void CQuoteDlg::OnDestroy()
{

	// TODO:  在此处添加消息处理程序代码
	if (_funcQuote2)
		_funcQuote2();

	if (_func2)
		_func2();


	__super::OnDestroy();
}
