
// QuoteDlg.h : 头文件
//

#pragma once
#include "resource.h"

#include "FAQuote/FAQuote_Def.h"
#include "quoteui/QuoteView.h"

// CQuoteDlg 对话框
class CQuoteDlg : public CDialogEx,public IQuoteEvent
{
// 构造
public:
	CQuoteDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_QUOTE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	//////////////////////////////////////////////////////////////////////////
public:
	// 行情连接处理
	virtual void OnQuoteConnected();
	// 行情用户登录成功
	virtual void OnQuoteUserLoginSuccess();
	// 行情用户登录失败
	virtual void OnQuoteUserLoginFailed(int nErrorID, const char* pszMsg);
	// 行情断开处理
	virtual void OnQuoteDisconnected(int nReason);
	// 行情错误处理
	virtual void OnQuoteError(int nErrorID, const char* pszMsg);

	// 行情订阅成功
	virtual void OnQuoteSubMarketDataSuccess(char* pszInstrumentID);
	// 行情订阅失败
	virtual void OnQuoteSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg);

	// 行情数据处理
	virtual void OnQuoteMarketData(tagMarketData* pstMarketData);

	// 行情退订成功
	virtual void OnQuoteUnSubMarketDataSuccess(char* pszInstrumentID);
	// 行情退订失败
	virtual void OnQuoteUnSubMarketDataFailed(char* pszInstrumentID, int nErrorID, const char* pszMsg) ;
	afx_msg void OnClose();
	afx_msg void OnDestroy();
};


//////////////////////////////////////////////////////////////////////////
#define INS_NAME ("ag1612")
#define URL_QUOTE ("tcp://180.168.146.187:10011")
#define BROKER_ID ("9999")
#define ACCOUNT ("025458")
#define PASSWORD ("test123")

extern IFAQuote * _pQuote;
extern CQuoteView * theQuoteView;