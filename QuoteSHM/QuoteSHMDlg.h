
// QuoteSHMDlg.h : 头文件
//

#pragma once

/************************************************************************/
/* 
	通过共享内存的行情进行渲染
	1.适用于策略转发的行情
*/
/************************************************************************/


// CQuoteSHMDlg 对话框
class CQuoteSHMDlg : public CDialogEx
{
// 构造
public:
	CQuoteSHMDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_QUOTESHM_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
};
