#pragma once
#include "FAQuote/FAQuote_Def.h"
#include "ctpImpl/CTPQuoteApi.h"
#include "ctpImpl/CTPQuoteSpi.h"
#include "dllHelper.h"

//#ifdef _DEBUG
//#include "vld.h"
//#endif

//#ifdef FAQUOTE_EXPORTS
//#define FAQUOTE_API __declspec(dllexport)
//#else
//#define FAQUOTE_API __declspec(dllimport)
//#endif
//
class CFAQuote :public IFAQuote{
public:
	CFAQuote(void) :m_pEvent(0){}
	explicit  CFAQuote(IQuoteEvent*pEvent);
	// TODO:  在此添加您的方法。

	virtual int Connect(char *address);
	virtual int Login(char*broker, char *account, char*pwd);
	virtual int Logout();
	virtual int DisConnect();
	//virtual int SubIns(vIns &ins);
	virtual int SubIns(char*szIns[], const int nCont);
	virtual int UnSubIns(vIns &ins) { return 0; }
public:

	CCTPQuoteSpi	m_quoteSpi;
	CCTPQuoteApi	m_quoteApi;
	IQuoteEvent		*m_pEvent;
};

FA_API IFAQuote* CreateObject(IQuoteEvent*pEvent);
FA_API int ReleaseObject(void);
