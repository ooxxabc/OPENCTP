// FAQuote.cpp : 定义 DLL 应用程序的导出函数。
//

#include "FAQuote.h"

CFAQuote * _FAQuote = NULL;

CFAQuote::CFAQuote(IQuoteEvent*pEvent) :m_pEvent(pEvent)
{
	m_quoteSpi.SetIQutoe(pEvent);
	return;
}

int CFAQuote::Connect(char *address)
{
	return  true == m_quoteApi.Init(&m_quoteSpi, address) ? SUCCESS:FAIL;
}
int CFAQuote::Login(char*broker, char *account, char*pwd)
{
	return true == m_quoteApi.Login(broker, account, pwd);
}
int CFAQuote::Logout()
{
	return 0;
}
int CFAQuote::DisConnect()
{
	m_quoteApi.Fini();
	return 0;
}

//
//int  CFAQuote::SubIns(vIns &ins)
//{
//	int nCount = ins.size();
//	char *inss[INS_MAX_COUNT];
//	for (size_t i = 0; i < nCount; i++)
//	{
//		inss[i] = new char[INS_ID_LENGTH];
//		sprintf_s(inss[i], INS_ID_LENGTH, ins[i].c_str());
//	}
//	//m_quoteApi.SubMarketData(inss, ins.size());
//	//return SUCCESS;
//
//	return SubIns(inss, nCount);
//}

int  CFAQuote::SubIns(char*szIns[], const int nCont)
{
	m_quoteApi.SubMarketData(szIns, nCont);
	return SUCCESS;
}


/*

*/

FA_API IFAQuote* CreateObject(IQuoteEvent*pEvent)
{
	if (!_FAQuote)
	{
		_FAQuote = new CFAQuote(pEvent);
	}

	return _FAQuote;
}
FA_API int ReleaseObject(void)
{
	if (_FAQuote)
	{
		_FAQuote->DisConnect();
		delete _FAQuote;
		_FAQuote = NULL;
	}

	return 0;
}