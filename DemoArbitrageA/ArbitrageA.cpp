// Demo2.cpp : 定义控制台应用程序的入口点。
//

#include <stdio.h>
//#include <tchar.h>
#include <string>

/*
策略主程序
*/

#include "FAPrint/TPrint.h"
#include "dllHelper.h"
#include "FAQuote/FAQuote_Def.h"
#include "FATrade/FATrade_Def.h"
#include "StrategyPlatform/StrategyPlatform_Def.h"


CDllHelperAuto _dll(new CDllHelper("FAStrategyPlatform.dll"));
CDllHelperAuto _dllPlusA(new CDllHelper("StrategyArbitrageA.dll"));
CDllHelperAuto _dllPrint(new CDllHelper("FAPrint.dll"));


typedef IStrategyPlatform* (*CreateStrategyPlatform)();
CreateStrategyPlatform _func = NULL;
IStrategyPlatform * _pPlatForm = NULL;
typedef void* (*ReleaseStrategyPlatform)();
ReleaseStrategyPlatform _func2 = NULL;

typedef IStrategyPlus* (*CreateStrategyArbitrageA)(IStrategyPlatform*p);
CreateStrategyArbitrageA _funcA = NULL;
IStrategyPlus * _pStrategyPlusA = NULL;
typedef void* (*ReleaseStrategyArbitrageA)();
ReleaseStrategyArbitrageA _funcA2 = NULL;

typedef IYKPrint* (*CreatePrint)();
typedef void* (*ReleasePrint)();

int main()
{
	//init	

	/*
	Platform
	*/

	_func = _dll->GetProcedure<CreateStrategyPlatform>("CreateObject");
	if (_func)
	{
		_pPlatForm = _func();
		printf("Init StrategyPlatform\n");
	}
	int ret = _pPlatForm->Init();
	if (ret != SUCCESS)
	{
		printf("Init StrategyPlatform Fail\n");
	}
	_func2 = _dll->GetProcedure<ReleaseStrategyPlatform>("ReleaseObject");
	getchar();

	//release
	if (_func2)
	{
		_func2();
		printf("UnInit StrategyPlatform\n");
	}

	if (_funcA2)
	{
		_funcA2();
		printf("UnInit StrategyArbitrageA\n");

	}

	return 0;
}

