// Copyright Stefan Seefeld 2007.
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include "stdafx.h"

#include <boost/python.hpp>

#include <boost/detail/lightweight_test.hpp>
#include <boost/bind.hpp>
#include <iostream>
#include <sstream>

namespace bpl = boost::python;

void import_test(char const *py_name_without_pix)
{
	////[1]固定py语句

	//////[2]动态文件
	//bpl::object import_ = bpl::import(py_name_without_pix);
	//bpl::object global(import_.attr("__dict__"));
	//std::string sz(py_name_without_pix);
	//sz.append(".py");
	//bpl::object result = bpl::exec_file(sz.c_str(), global, global);

	////[3]指定文件名
	//// Retrieve the main module
	//bpl::object import_ = bpl::import("import_");
	//int value = bpl::extract<int>(import_.attr("value")) BOOST_EXTRACT_WORKAROUND;
	//std::cout << value << std::endl;
	//BOOST_TEST(value == 42);

	//[4]指定函数
	bpl::object import_ = bpl::import(py_name_without_pix);
	bpl::object global(import_.attr("__dict__"));
	bpl::object foo = global["runstrat"];
	bpl::object val = bpl::extract<bpl::object>(foo());
	
}

int main(int argc, char **argv)
{
	//BOOST_TEST(argc == 2);

	// Initialize the interpreter
	Py_Initialize();

	if (bpl::handle_exception(boost::bind(import_test, argv[1])))
	{
		if (PyErr_Occurred())
		{
			BOOST_ERROR("Python Error detected");
			PyErr_Print();
		}
		else
		{
			BOOST_ERROR("A C++ exception was thrown  for which "
				"there was no exception handler registered.");
		}
	}

	// Boost.Python doesn't support Py_Finalize yet.
	// Py_Finalize();
	return boost::report_errors();
}

// Including this file makes sure
// that on Windows, any crashes (e.g. null pointer dereferences) invoke
// the debugger immediately, rather than being translated into structured
// exceptions that can interfere with debugging.
#include "module_tail.cpp"
