// Demo2.cpp : 定义控制台应用程序的入口点。
//

#include <stdio.h>
#ifdef WIN32
#include <tchar.h>
#else
#include <string>
#endif
/*
	策略主程序
*/


#include "dllHelper.h"
#include "StrategyPlatform/StrategyPlatform_Def.h"

CDllHelper _dll("FAStrategyPlatform.dll");

typedef IStrategyPlatform* (*CreateStrategyPlatform)();
CreateStrategyPlatform _func = NULL;
IStrategyPlatform * _pPlatForm = NULL;
typedef void* (*ReleaseStrategyPlatform)();
ReleaseStrategyPlatform _func2 = NULL;

int main()
{
	//init

	/*
		Platform
	*/

	_func = _dll.GetProcedure<CreateStrategyPlatform>("CreateObject");
	if (_func)
	{
		_pPlatForm = _func();
		printf("Init StrategyPlatform\n");
	}
	int ret = _pPlatForm->Init();
	if (ret != SUCCESS)
	{
		printf("Init StrategyPlusA Fail\n");
	}
	_func2 = _dll.GetProcedure<ReleaseStrategyPlatform>("ReleaseObject");
	getchar();


	//release
	if (_func2)
	{
		_func2();
		printf("UnInit StrategyPlatform\n");
	}

	return 0;
}

