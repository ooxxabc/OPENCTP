﻿#pragma once
#include "openctp.h"
#include "cdwStrategy.h"

#define CLOSE_STRATEGY(p,r) {if(p)return r;}
#define CLOSE_STRATEGY(p) {if(p)return;}

class CDWApp : public IFApplication,public ICDW
{
	FDECLARE_CLASS(CDWApp);
public:
	FIDFONT fnt_id;             // 自定义字体ID
	FIDTEXTURE tex_id;
	float sprx, spry;	
	FUI_System uiSys;
	openctp m_cdwUI;
	FStringA			szImei;
	
public:
	CDWApp();
	virtual bool Create();
	virtual void Destroy();
	virtual void Render();
	virtual bool OnInput(FINPUT_PACKAGE& ipk);	
	virtual void Update();
	virtual void Reset();
	/*
		java imple
	*/
	void OnIMEI(FStringA imei){ szImei = imei; }

public:
	/************************************************************************/
	/* 
		CDW回调
	*/
	/************************************************************************/
	virtual void OnQuote(tagMarketData * data);
	virtual void OnAccountDetail(tagTradingAccount*pAccount);
	virtual void OnPositionDetail(tagPosition*pPos, bool bLast);
	virtual void ClearPositionDetail();



	/************************************************************************/
	/* 
		按钮操作
	*/
	/************************************************************************/
	void Duo();
	void Kong();
	void Clear();
	void Fan();
	void Lock();

private:
	LPSTRATEGYPLUS cdwStrategy;
	bool m_bClose;
};