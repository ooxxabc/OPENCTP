//----------------------------------------------------------------
// Desc: 
// Edit: 此文件的代码由“Fuel4D UI界面编辑器”自动生成
//
// Date Created: 
//
// Update Date : 
//
// Copyright (c) Fuel4D (http://www.fuel4d.com), All rights reserved.
//
//----------------------------------------------------------------
#pragma once


#include "UI/FUI_Include.h"

enum 
{
	DIF_NULL = 0,
	DIF_NAME,
	DIF_DIRCTOR,
	DIF_YESTODAY,
	DIF_TODAY,
	DIF_PRICE,
};

class openctp : public FUI_Layout, public IFUI_Instance< openctp >
{
    FUI_DECLARE_CLASS(openctp)

public:
    // 控件ID枚举表
    enum ID_CONTROL
    {
        ID_FRAME_MAIN = 0x1C0E5D12,
        ID_CONTAINER_INS_PRICE = 0x791EAB7F,
        ID_TEXT_INS = 0x57C6A7F0,
        ID_TEXT_ASK_VOL = 0x85544681,
        ID_TEXT_BID_VOL = 0xD880FF02,
        ID_TEXT_ASK = 0x89AE8A59,
        ID_TEXT_BID = 0x45CED015,
        ID_CONTAINER_BTN = 0xD499B82A,
        ID_BUTTON_DUO = 0x7CB27CF6,
        ID_BUTTON_KONG = 0x237D6CEE,
        ID_BUTTON_FAN = 0x89B06F3F,
        ID_BUTTON_SUO = 0x246D73F2,
        ID_BUTTON_QING = 0x025AD59A,
        ID_CONTAINER_Status = 0xDB9259E8,
        ID_TEXT_jtqy = 0xD79CC6CF,
        ID_TEXT_zyk = 0xEF72D9E6,
        ID_TEXT_jtqy_v = 0x8E0A4A34,
        ID_TEXT_zyk_v = 0x78666528,
        ID_TEXT_sxf = 0xAE242DE5,
        ID_TEXT_sxf_v = 0x5B061C02,
        ID_TEXT_dtqy = 0xEC37F890,
        ID_TEXT_zybzj = 0x1039FF01,
        ID_TEXT_fxd = 0x791DD693,
        ID_TEXT_dtqy_v = 0xAE6059B6,
        ID_TEXT_zybzj_v = 0x5D0C9A73,
        ID_TEXT_fxd_v = 0x0E1BD34C,
        ID_CONTAINER_POS = 0xB1FFCABF,
        ID_REPORT_POS = 0x650DD397,
        ID_CONTAINER_TXT = 0x57813940,
        ID_TEXT_Instrument = 0x9A4F408F,
        ID_TEXT_Director = 0x2E454A1A,
        ID_TEXT_Today = 0xA0D0836D,
        ID_TEXT_Yestody = 0x8410589A,
        ID_TEXT_Win = 0x4045EBAF,
        ID_TEXT_Avage = 0x43D9EF8D,
        ID_TEXT_Win1 = 0x821BF7AF,
        ID_TEXT_Avage1 = 0x9B447062,
        ID_TEXT_Today1 = 0x5043DBAF,
        ID_TEXT_Yestody1 = 0x758D631D,
        ID_TEXT_Director1 = 0x1FC3F6B6,
        ID_TEXT_Instrument1 = 0x5D63609A,
    };

    // 构造函数
    openctp();
    // 析构函数
    virtual~openctp();

protected:
    // UI数据加载成功以后，UI系统调用此通知函数
    virtual bool OnCreate();
    // UI正常卸载前或OnCreate调用失败时，调用此通知函数
    virtual void OnDestroy();
    // UI系统在绘制处理前、后，调用此通知函数
    virtual void OnRender(bool before);
    // UI事件回调 (框架ID, 控件ID, 消息ID, 参数1, 参数2)， 不拦截消息返回 (false)
    virtual bool OnMessage(FIDHASH frame_id, FIDHASH widget_id, EF_UI_EVENT_TYPE message, INT64 wparam, INT64 lparam);

    // UI加载结束，且各控件绑定结束时自动调用此函数
    bool OnInitLayout();

public:
    FUI_Container* pCONTAINER_INS_PRICE;
    FUI_Text* pTEXT_INS;
    FUI_Text* pTEXT_ASK_VOL;
    FUI_Text* pTEXT_BID_VOL;
    FUI_Text* pTEXT_ASK;
    FUI_Text* pTEXT_BID;
    FUI_Container* pCONTAINER_BTN;
    FUI_Button* pBUTTON_DUO;
    FUI_Button* pBUTTON_KONG;
    FUI_Button* pBUTTON_FAN;
    FUI_Button* pBUTTON_SUO;
    FUI_Button* pBUTTON_QING;
    FUI_Container* pCONTAINER_Status;
    FUI_Text* pTEXT_jtqy;
    FUI_Text* pTEXT_zyk;
    FUI_Text* pTEXT_jtqy_v;
    FUI_Text* pTEXT_zyk_v;
    FUI_Text* pTEXT_sxf;
    FUI_Text* pTEXT_sxf_v;
    FUI_Text* pTEXT_dtqy;
    FUI_Text* pTEXT_zybzj;
    FUI_Text* pTEXT_fxd;
    FUI_Text* pTEXT_dtqy_v;
    FUI_Text* pTEXT_zybzj_v;
    FUI_Text* pTEXT_fxd_v;
    FUI_Container* pCONTAINER_POS;
    FUI_Report* pREPORT_POS;
    FUI_Container* pCONTAINER_TXT;
    FUI_Text* pTEXT_Instrument;
    FUI_Text* pTEXT_Director;
    FUI_Text* pTEXT_Today;
    FUI_Text* pTEXT_Yestody;
    FUI_Text* pTEXT_Win;
    FUI_Text* pTEXT_Avage;
    FUI_Text* pTEXT_Win1;
    FUI_Text* pTEXT_Avage1;
    FUI_Text* pTEXT_Today1;
    FUI_Text* pTEXT_Yestody1;
    FUI_Text* pTEXT_Director1;
    FUI_Text* pTEXT_Instrument1;

public:
	void SetQuote(tagMarketData*pData);
	void SetAccountDetail(tagTradingAccount*pAccount);
	void SetPositionDetail(tagPosition*pPos);
	void ClearPositionDetail();


public:
	tagPosition* PutPosition(tagPosition*pPos);
	int Check(const tagPosition*pa, const tagPosition *pb);
private:
	std::vector<tagPosition*>m_vPos;
};