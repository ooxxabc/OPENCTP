﻿
#include "common.h"
#include "openctp.h"


openctp::openctp()
    :FUI_Layout(_FTA("openctp"))
{
}

openctp::~openctp()
{
}

bool openctp::OnCreate()
{
    pCONTAINER_INS_PRICE = (FUI_Container*)pFRAME->FindChild(ID_CONTAINER_INS_PRICE);
    pTEXT_INS = (FUI_Text*)pFRAME->FindChild(ID_TEXT_INS);
    pTEXT_ASK_VOL = (FUI_Text*)pFRAME->FindChild(ID_TEXT_ASK_VOL);
    pTEXT_BID_VOL = (FUI_Text*)pFRAME->FindChild(ID_TEXT_BID_VOL);
    pTEXT_ASK = (FUI_Text*)pFRAME->FindChild(ID_TEXT_ASK);
    pTEXT_BID = (FUI_Text*)pFRAME->FindChild(ID_TEXT_BID);
    pCONTAINER_BTN = (FUI_Container*)pFRAME->FindChild(ID_CONTAINER_BTN);
    pBUTTON_DUO = (FUI_Button*)pFRAME->FindChild(ID_BUTTON_DUO);
    pBUTTON_KONG = (FUI_Button*)pFRAME->FindChild(ID_BUTTON_KONG);
    pBUTTON_FAN = (FUI_Button*)pFRAME->FindChild(ID_BUTTON_FAN);
    pBUTTON_SUO = (FUI_Button*)pFRAME->FindChild(ID_BUTTON_SUO);
    pBUTTON_QING = (FUI_Button*)pFRAME->FindChild(ID_BUTTON_QING);
    pCONTAINER_Status = (FUI_Container*)pFRAME->FindChild(ID_CONTAINER_Status);
    pTEXT_jtqy = (FUI_Text*)pFRAME->FindChild(ID_TEXT_jtqy);
    pTEXT_zyk = (FUI_Text*)pFRAME->FindChild(ID_TEXT_zyk);
    pTEXT_jtqy_v = (FUI_Text*)pFRAME->FindChild(ID_TEXT_jtqy_v);
    pTEXT_zyk_v = (FUI_Text*)pFRAME->FindChild(ID_TEXT_zyk_v);
    pTEXT_sxf = (FUI_Text*)pFRAME->FindChild(ID_TEXT_sxf);
    pTEXT_sxf_v = (FUI_Text*)pFRAME->FindChild(ID_TEXT_sxf_v);
    pTEXT_dtqy = (FUI_Text*)pFRAME->FindChild(ID_TEXT_dtqy);
    pTEXT_zybzj = (FUI_Text*)pFRAME->FindChild(ID_TEXT_zybzj);
    pTEXT_fxd = (FUI_Text*)pFRAME->FindChild(ID_TEXT_fxd);
    pTEXT_dtqy_v = (FUI_Text*)pFRAME->FindChild(ID_TEXT_dtqy_v);
    pTEXT_zybzj_v = (FUI_Text*)pFRAME->FindChild(ID_TEXT_zybzj_v);
    pTEXT_fxd_v = (FUI_Text*)pFRAME->FindChild(ID_TEXT_fxd_v);
    pCONTAINER_POS = (FUI_Container*)pFRAME->FindChild(ID_CONTAINER_POS);
    pREPORT_POS = (FUI_Report*)pFRAME->FindChild(ID_REPORT_POS);
    pCONTAINER_TXT = (FUI_Container*)pFRAME->FindChild(ID_CONTAINER_TXT);
    pTEXT_Instrument = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Instrument);
    pTEXT_Director = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Director);
    pTEXT_Today = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Today);
    pTEXT_Yestody = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Yestody);
    pTEXT_Win = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Win);
    pTEXT_Avage = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Avage);
    pTEXT_Win1 = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Win1);
    pTEXT_Avage1 = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Avage1);
    pTEXT_Today1 = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Today1);
    pTEXT_Yestody1 = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Yestody1);
    pTEXT_Director1 = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Director1);
    pTEXT_Instrument1 = (FUI_Text*)pFRAME->FindChild(ID_TEXT_Instrument1);

    return OnInitLayout();
}

void openctp::OnDestroy()
{
}

void openctp::OnRender(bool before)
{
    if (before)
    {
    }
    else
    {
    }
}

bool openctp::OnMessage(FIDHASH frame_id, FIDHASH widget_id, EF_UI_EVENT_TYPE message, INT64 wparam, INT64 lparam)
{
    if (frame_id != ID_FRAME_MAIN)
        return false;

    if (message == eUET_LButtonUp)
    {
        switch (widget_id)
        {
		case ID_BUTTON_DUO:
			sampleApp.Duo();
            return true;
		case ID_BUTTON_KONG:
			sampleApp.Kong();
			return true;
		case ID_BUTTON_FAN:
			sampleApp.Fan();
            return true;
		case ID_BUTTON_SUO:
			sampleApp.Lock();
            return true;
		case ID_BUTTON_QING:
			sampleApp.Clear();
            return true;
        };
    }

    return false;
}

bool openctp::OnInitLayout()
{
	pTEXT_ASK->SetText(_FTA("卖："));
	pTEXT_BID->SetText(_FTA("买："));
	pTEXT_ASK_VOL->SetText(_FTA("FB:"));
	pTEXT_BID_VOL->SetText(_FTA("FB:"));
	pTEXT_INS->SetText(_FTA("合约"));

	pTEXT_jtqy->SetText(_FTA("静态权益"));
	pTEXT_zyk->SetText(_FTA("持仓盈亏"));
	pTEXT_sxf->SetText(_FTA("手续费"));
	pTEXT_dtqy->SetText(_FTA("动态权益"));
	pTEXT_zybzj->SetText(_FTA("占用保证金"));
	pTEXT_fxd->SetText(_FTA("风险度"));

	//持仓
	pTEXT_Instrument->SetText(_FTA("IF1709"));
	pTEXT_Director->SetText(_FTA("买"));
	pTEXT_Today->SetText(_FTA("1"));
	pTEXT_Yestody->SetText(_FTA("0"));
	pTEXT_Win->SetText(_FTA("8888"));
	pTEXT_Avage->SetText(_FTA("1000"));


	pTEXT_Instrument1->SetText(_FTA("IF1712"));
	pTEXT_Director1->SetText(_FTA("卖"));
	pTEXT_Today1->SetText(_FTA("1"));
	pTEXT_Yestody1->SetText(_FTA("0"));
	pTEXT_Win1->SetText(_FTA("8888"));
	pTEXT_Avage1->SetText(_FTA("1000"));

	FV_UI_RT_HEAD head;
	head.push_back(FU_RT_HEAD(_FTA("合约"), 100, 0));
	head.push_back(FU_RT_HEAD(_FTA("买卖"), 100, 0));
	head.push_back(FU_RT_HEAD(_FTA("昨仓"), 100, 0));
	head.push_back(FU_RT_HEAD(_FTA("今仓"), 100, 0));
	head.push_back(FU_RT_HEAD(_FTA("持仓均价"), 100, 0));
	head.push_back(FU_RT_HEAD(_FTA("持仓盈亏"), 100, 0));

	pREPORT_POS->InitHead(head);

	///*
	//	测试数据
	//*/
	//FUI_VStringA va;
	//va.push_back("1");
	//va.push_back("2");
	//va.push_back("3");
	//va.push_back("4");
	//va.push_back("5");
	//va.push_back("6");
	//pREPORT_POS->InsertLine(va);

    return true;
}

/************************************************************************/
/* 
	OPENCTP
*/
/************************************************************************/
void openctp::SetQuote(tagMarketData*pData)
{
	pTEXT_INS->SetText(FAssertFmtA(_FTA("%s"), pData->szINSTRUMENT));
	//pTEXT_INS->SetText(pData->szINSTRUMENT);
	pTEXT_ASK->SetText(FAssertFmtA(_FTA("%.1f"), pData->dAskPrice1));
	pTEXT_BID->SetText(FAssertFmtA(_FTA("%.1f"), pData->dBidPrice1));

	pTEXT_ASK_VOL->SetText(FAssertFmtA(_FTA("%d"), pData->nAskVolume1));
	pTEXT_BID_VOL->SetText(FAssertFmtA(_FTA("%d"), pData->nBidVolume1));

}

void openctp::SetAccountDetail(tagTradingAccount*pAccount)
{
	pTEXT_jtqy->SetText(FAssertFmtA(_FTA("静态权益:%d"),(int)pAccount->dPreBalance));
	pTEXT_zyk->SetText(FAssertFmtA(_FTA("持仓盈亏:%d"), (int)pAccount->dPositionProfit));
	pTEXT_sxf->SetText(FAssertFmtA(_FTA("手续费:%d"), (int)pAccount->dCommission));
	pTEXT_dtqy->SetText(FAssertFmtA(_FTA("动态权益:%d"), (int)pAccount->dDynamicEquity));
	pTEXT_zybzj->SetText(FAssertFmtA(_FTA("占用保证金:%d"), (int)pAccount->dFrozenCapital));
	pTEXT_fxd->SetText(FAssertFmtA(_FTA("风险度:%.1f"),0));
}

int openctp::Check(const tagPosition*pa, const tagPosition *pb)
{
	if (strcmp(pa->szINSTRUMENT, pb->szINSTRUMENT) != 0)
	{
		return DIF_NAME;
	}

	return DIF_NULL;
}

tagPosition* openctp::PutPosition(tagPosition*pPos)
{
	/*
		暂不支持持仓信息查看，请根据其他软件查看情况。	
	*/
	return 0;

	tagPosition * p = pPos;

	if (m_vPos.empty())
	{
		m_vPos.push_back(pPos);
	}
	else
	{
		for (size_t i = 0; i < m_vPos.size(); i++)
		{
			if (DIF_NULL == Check(m_vPos[i], pPos))
			{
				m_vPos[i] = pPos;
			}
		}
	}

	return p;
}

void openctp::ClearPositionDetail()
{

}

void openctp::SetPositionDetail(tagPosition*p)
{
	tagPosition * pPos = PutPosition(p);

	if (!pPos)
		return;

	FStringA szTmp(_FTA(""));
	szTmp = FAssertFmtA(_FTA("%s"), pPos->szINSTRUMENT);
	pTEXT_Instrument->SetText(szTmp);
	szTmp = _FTA("买");
	if (pPos->nTradeDir == 1)
	{
		szTmp = _FTA("卖");
	}
	pTEXT_Director->SetText(szTmp);
	//昨仓
	if (pPos->nYesterdayPosition > 0)
	{
		szTmp = FAssertFmtA(_FTA("%d"), pPos->nYesterdayPosition);
	}
	else
	{
		szTmp = _FTA("0");
	}
	pTEXT_Yestody->SetText(szTmp);
	//今仓
	if (pPos->nTodayPosition > 0)
	{
		szTmp = FAssertFmtA(_FTA("%d"), pPos->nTodayPosition);
	}
	else
	{
		szTmp = _FTA("0");
	}
	pTEXT_Today->SetText(szTmp);

	//持仓均价
	szTmp = FAssertFmtA(_FTA("%.1f"), pPos->dAvgPrice);
	pTEXT_Avage->SetText(szTmp);


	//持仓盈亏
	szTmp = FAssertFmtA(_FTA("%.2f"), pPos->dPositionProfit);
	pTEXT_Win->SetText(szTmp);


#ifndef MULIT_INS
	return;
#endif

	/*
		添加持仓信息

	FStringA szTmp(_FTA(""));
	FUI_VStringA va;
	va.push_back(pPos->szINSTRUMENT);
	szTmp = _FTA("买");
	if (pPos->nTradeDir == 1)
	{
		szTmp = _FTA("卖");
	}
	va.push_back(szTmp.c_str());
	//昨仓
	if (pPos->nYesterdayPosition > 0)
	{
		szTmp = FAssertFmtA(_FTA("%d"), pPos->nYesterdayPosition);
	}
	else
	{
		szTmp = _FTA("0");
	}
	
	va.push_back(szTmp.c_str());

	//今仓
	if (pPos->nTodayPosition > 0)
	{
		szTmp = FAssertFmtA(_FTA("%d"), pPos->nTodayPosition);
	}
	else
	{
		szTmp = _FTA("0");
	}
	va.push_back(szTmp.c_str());

	//持仓均价
	szTmp = FAssertFmtA(_FTA("%.1f"), pPos->dAvgPrice);
	va.push_back(szTmp.c_str());

	//持仓盈亏
	szTmp = FAssertFmtA(_FTA("%.2f"), pPos->dPositionProfit);
	va.push_back(szTmp.c_str());

	pREPORT_POS->InsertLine(va);	
	
	*/
}

