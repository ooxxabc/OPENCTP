﻿//----------------------------------------------------------------
// Desc: 预编译头文件
// Edit: 
//
// Date Created: 2016-04-09
//
// Update Date : 
//
// Copyright (c) createDevice, All rights reserved.
//
//----------------------------------------------------------------
#pragma once
//#pragma comment(linker, "/SUBSYSTEM:CONSOLE")

//#ifdef _MSC_VER
//#    ifdef NDEBUG
//#        pragma comment(linker, "/SUBSYSTEM:WINDOWS /ENTRY:mainCRTStartup")
//#    else
//#        pragma comment(linker, "/SUBSYSTEM:CONSOLE")
//#    endif
//#endif

#pragma comment(linker, "/SUBSYSTEM:Windows")

#include "FuelApp.h"
#include "FuelUI.h"
#include "FuelCore.h"
#include "Core/FBsc_JNI.h"

#include "openctp_common.h"

#define _APP_W 606
#define _APP_H 500


// 在此行后面添加其它的头文件包含
#include "CDWApp.h"
extern CDWApp sampleApp;
