﻿
#include "common.h"

// 我们的应用程序对象
CDWApp sampleApp;

// Fuel4D 程序入口，每个应用程序都必须有此函数
extern IFApplication* FNewApplication(FEngineParameter& p)
{
    // 设置窗口参数
    //     只在有窗口体系的操作系统平台有效，如安卓此设置没用
	p.SetWindow(_APP_W, _APP_H, true,_FTA("OPENCTP炒单王"));
    // 设置默认字体，此项必须设置，本例设置为黑体+14号
    //     FAppGetAssetsA 将自动转换各操作系统平台的路径
    //                    具体设置见应用程序框架中FApplication.cpp中FAPP_MODULE类中构造的地方
    //     设置字体只能使用ANSI字符集
	p.SetFont(FAppGetAssetsA(_FTA("font/dsf.ttf")), 24, true);
    // 设置为2D渲染体系，减少非2D环境的初始过程与内存，默认为2D环境
    //     Set2DEnv 为设置2D环境参数，Set3DEnv 为设置3D环境，两者选其一，默认2D
    //     eDT_D3D9 默认采用D3D9渲染设备，当此设备不存在时，自动选择其它已支持的渲染设备
    //              如：在安卓平台自动切换到eDT_GLES2渲染设备
    p.Set2DEnv(eDT_D3D9);
    // 关闭垂直同步，获得最大FPS（D3D环境下有效，但其它操作系统仍可以设置，不影响）
    //     也可以用p.graph.nLockFps来锁定FPS在一定值以内
    p.graph.bVerticalSync = false;
    // 锁定FPS在30帧及以下
    p.graph.nLockFps = 45;

    return &sampleApp;
}

#if FUEL_OS_ANDROID
extern void FAppIMEI_android(jstring imei)
{
	FStringA szImei;
	FJavaStringToUTF8(imei,szImei);
	sampleApp.OnIMEI(szImei);
}

#endif