﻿#include "common.h"
#include "Src/FUI_InputEvent.h"
#include "Src/FUI_FuncInterface.h"
#include <assert.h>

CDllHelperAuto _dll(new CDllHelper("FAStrategyPlatform.dll"));
CDllHelperAuto _dllPlusA(new CDllHelper("StrategyCDW.dll"));

typedef IStrategyPlatform* (*CreateStrategyPlatform)();
CreateStrategyPlatform _func = NULL;
IStrategyPlatform * _pPlatForm = NULL;
typedef void* (*ReleaseStrategyPlatform)();
ReleaseStrategyPlatform _func2 = NULL;


CDWApp::CDWApp()
:szImei("123456789012345")
, m_bClose(false)
{

}

bool CDWApp::Create()
{
	//DebugBreak();

	FUI_INIT it;
	it.hwnd  = fge->GetHwnd();
	it.wnd_w = fge->GetWidthF();
	it.wnd_h = fge->GetHeightF();
	it.base_dir = FAppGetAssetsRootA();
	it.userPTR  = (DWORD_PTR)fge;
	FUI_InitFuncInterface(it);
	uiSys.Initialize(_FTA("cdw.fud"), &it);
	m_cdwUI.LoadUI(&uiSys);

	/************************************************************************/
	/* 
		init openctp
	*/
	/************************************************************************/
	

	//init	
	/*
	Platform
	*/

	_func = _dll->GetProcedure<CreateStrategyPlatform>("CreateObject");
	if (_func)
	{
		_pPlatForm = _func();
		printf("Init StrategyPlatform\n");
	}
	assert(_pPlatForm);
	int ret = _pPlatForm->Init();
	if (ret != SUCCESS)
	{
		printf("Init StrategyPlatform Fail\n");
		return false;
	}
	//_func2 = _dll->GetProcedure<ReleaseStrategyPlatform>("ReleaseObject");

	/************************************************************************/
	/* 
		启动cdw,检查StrategyPlatform.xml 是否添加cdw plus
			
		<Strategys num="1" ID1="1" Strategy1="StrategyTBMonitor" Tag1="标准" ID2="2" Strategy2="StrategyPlusBD" Tag2="套利"/> 
	*/
	/************************************************************************/
	
	cdwStrategy = _pPlatForm->GetStrategy("StrategyCDW");
	
	if (!cdwStrategy) {
		MessageBox(0, L"检查StrategyPlatform.xml 是否添加cdw plus", L"WARNING", MB_OK); exit(1); return false;
	}

	cdwStrategy->SetHandle(this);

	return true;
}
void CDWApp::Destroy()
{	
	FPF_FuncAuto;

	m_bClose = true;
	_pPlatForm->UnInit();
	m_cdwUI.UnLoad();
	uiSys.Release();	
}
void CDWApp::Render()
{
    // 对渲染逻辑进行性能统计，性能日志存在log\[date-time]\profile - [app_name].log中
	FPF_FuncAuto;	    
	uiSys.Render();
	uiSys.RenderTips();
}
bool CDWApp::OnInput(FINPUT_PACKAGE& ipk)
{
	static bool is_down = false;
	switch (ipk.nType)
	{
	case eIT_KEYUP:
		if (ipk.p1 == VK_ESCAPE)
			fge->Quit();
		else if (ipk.p1 == 'A')
			fge->GetFont()->__OutputFntTex(eCD_DefFontID, _FTA("d:\\aaa.png"));
		break;
	case eIT_LBUTTON_DOWN:
		sprx = ipk.p2.v.v2.x;
		spry = ipk.p2.v.v2.y;
		is_down = true;
		break;
	case eIT_MOUSEMOVE:
		if (is_down)
		{
			sprx = ipk.p2.v.v2.x;
			spry = ipk.p2.v.v2.y;
		}
		break;
	case eIT_LBUTTON_UP:
		is_down = false;
		sprx = ipk.p2.v.v2.x;
		spry = ipk.p2.v.v2.y;
		break;
	}
	return FInputTransmitToUI(ipk, fge, &uiSys);
	return false;
}
	
void CDWApp::Update()
{
	FPF_FuncAuto;
	uiSys.Update();
}

void CDWApp::OnQuote(tagMarketData * data)
{
	CLOSE_STRATEGY(m_bClose)
	m_cdwUI.SetQuote(data);
}

void CDWApp::OnAccountDetail(tagTradingAccount*pAccount)
{
	CLOSE_STRATEGY(m_bClose)

	if (!pAccount)
		return;
	
	m_cdwUI.SetAccountDetail(pAccount);
}

void CDWApp::ClearPositionDetail()
{
	//clear
	m_cdwUI.ClearPositionDetail();
}

void CDWApp::OnPositionDetail(tagPosition*pPos, bool bLast)
{
	CLOSE_STRATEGY(m_bClose)

	if (!pPos)
		return;

	m_cdwUI.SetPositionDetail(pPos);
}

void CDWApp::Reset()
{
	uiSys.Reset(fge->GetWidthF(), fge->GetHeightF());
}