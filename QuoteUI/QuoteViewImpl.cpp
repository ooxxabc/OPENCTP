#include "stdafx.h"
#include "resource.h"
#include "QuoteUI/QuoteView.h"

static CQuoteView * pView = NULL;

void CQuoteView::PostNcDestroy()
{
	CDialog::PostNcDestroy();
	delete this;
}

UI_API CQuoteView*CreateObject(HWND hwnd)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if (!pView)
	{
		CWnd* pParent = CWnd::FromHandle(hwnd);

		if (!pParent)
		{
			return 0;
		}

		pView = new CQuoteView(pParent);

		if (pView->Create(IDD_CHART_VIEW, pParent))
		{
			pView->ShowWindow(SW_SHOW);
			return pView;
		}
	}

	return pView;
}

UI_API void ReleaseObject()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if (pView)
	{
		pView->DestroyWindow();
	}
}