#pragma once
#include "FABase/thread.h"
#include "xredis/xRedisClient.h"
#include "ctpImpl/Quote_def.h"
#include "FABase/safe_vector.h"
#include "ctpImpl/marketDataArchive.h"
#include "dllHelper.h"
using namespace std;

#define CACHE_TYPE_WR 1
#define CACHE_TYPE_RD 2

//��д����
struct tagRedisCfg
{
	char ip[33];
	char pwd[33];
	int  nPortW;	//д�˿�
	int  nPortR;	//���˿�
	int  nPoolSize;
};

typedef CYKSafeVector<tagMarketData> vMarketData;

class CYKRedis:public IRedis
{
public:
	explicit CYKRedis(IRedisHandle *pHandel);
	~CYKRedis(void);
	
	void Load();
	
	virtual void Start();
	virtual void Stop();

	virtual void Storge(tagMarketData*pData);

	void GetHashNode(char*rootName,tm&t,string&node);

public:
	static void*Wirte(const bool *bStop,void *param);
	void _Write();

private:
	bool			m_bInit;
	int				m_iCacheMum;
	RedisNode*		m_pWRRedisList;
	RedisNode*		m_pRDRedisList;
	xRedisClient*	m_pRedisClient;
	IRedisHandle	* m_pHandle;
private:
	vMarketData		m_recv;
	CYKThread		*m_thread;
	bool			m_bStop;
};

extern CYKRedis * g_pRedis;


extern "C"
{
	FA_API IRedis* CreateObject(IRedisHandle*pHandle);
	FA_API void ReleaseObject();
}