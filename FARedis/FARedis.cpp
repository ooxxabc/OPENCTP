#include "FARedis.h"
#include "FABase/tools.h"
#ifdef WIN32
#endif

#define CACHE_TYPE_WR 1
#define CACHE_TYPE_RD 2
#define MAX_CACHE_SIZE 16

unsigned int DoHash(const char *str)
{
	unsigned int hash = 0;
	int i;

	for (i = 0; *str; i++)
	{
		if ((i & 1) == 0)
		{
			hash ^= ((hash << 7) ^ (*str++) ^ (hash >> 3));
		}
		else
		{
			hash ^= (~((hash << 11) ^ (*str++) ^ (hash >> 5)));
		}
	}

	return (hash & 0x7FFFFFFF);
}

CYKRedis *g_pRedis = NULL;

extern "C"
{
	FA_API IRedis* CreateObject(IRedisHandle*pHandle)
	{
		if (!g_pRedis)
		{
			g_pRedis = new CYKRedis(pHandle);
		}
		return g_pRedis;
	}
	FA_API void ReleaseObject()
	{
		SAFE_DEL(g_pRedis);
	}

}

CYKRedis::CYKRedis(IRedisHandle *pR)
	:m_pRedisClient(NULL)
	,m_iCacheMum(1)
	,m_bInit(false)
	,m_pHandle(pR)
	,m_bStop(false)
{
	m_pRedisClient = new xRedisClient();
	m_pWRRedisList = (RedisNode *)malloc(sizeof(RedisNode)*MAX_CACHE_SIZE);
	m_pRDRedisList = (RedisNode *)malloc(sizeof(RedisNode)*MAX_CACHE_SIZE);
	memset(m_pWRRedisList, 0, sizeof(RedisNode)*MAX_CACHE_SIZE);
	memset(m_pRDRedisList, 0, sizeof(RedisNode)*MAX_CACHE_SIZE);
}


CYKRedis::~CYKRedis()
{
	if (m_pRedisClient)
	{
		delete m_pRedisClient;
		m_pRedisClient = NULL;
	}
}

void CYKRedis::Stop()
{
	m_bStop = true;
	m_pRedisClient->release();
}

void*CYKRedis::Wirte(const bool *bStop,void *param)
{
	CYKRedis * pThis = (CYKRedis*)param;
	while(!pThis->m_bStop)
	{

		pThis->_Write();

#ifdef WIN32
		Sleep(100);
#else
		usleep(1000);
#endif
	}

	return 0;
}

void CYKRedis::Start()
{
	Load();

	m_bStop = false;

	if(!m_bInit)
	{	
		printf("[%s]not load cfg.\n",__FUNCTION__);
		return;
	}

	m_pRedisClient->Init(MAX_REDIS_CACHE_TYPE);
	bool bRet = m_pRedisClient->ConnectRedisCache(m_pWRRedisList, m_iCacheMum, CACHE_TYPE_WR);
	if (!bRet)
		return;
	bRet = m_pRedisClient->ConnectRedisCache(m_pRDRedisList, m_iCacheMum, CACHE_TYPE_RD);
	if (!bRet)
		return;

	m_thread = new CYKThread(Wirte,this,false);
	m_thread->start();

	printf("[%s]\n",__FUNCTION__);

}

void CYKRedis::Load()
{
	int nIndex = 0;

	RedisNode redis_nd;
	redis_nd.dbindex = nIndex;
	// ip
	sprintf_s(redis_nd.host, sizeof(redis_nd.host), "127.0.0.1");		
	//�˿�
	redis_nd.port = 6379;
	//password
	sprintf_s(redis_nd.passwd, sizeof(redis_nd.passwd), "");
	//PoolSize
	redis_nd.poolsize = 8;	
	//TimeOut
	redis_nd.timeout = 1;
	memcpy(&m_pWRRedisList[0], &redis_nd, sizeof(redis_nd));
	redis_nd.port = 6380;
	memcpy(&m_pRDRedisList[0], &redis_nd, sizeof(redis_nd));
	
	m_bInit = true;
}

void CYKRedis::Storge(tagMarketData*pData)
{
	m_recv.put(*pData);

	////test
	//_Write();
}

void CYKRedis::GetHashNode(char*rootName,tm&t,string&node)
{
	node.append(rootName);
	char sz[33]={0};
	sprintf(sz,":%d",t.tm_year+1900);
	node.append(sz);
}


void CYKRedis::_Write()
{
	if (m_recv.size() == 0)
		return;

	tagMarketData data;
	if(0!=m_recv.get(data,1))
		return;

	tagMarketDataArchive arch;
	arch.Copy(&data);
	std::string sz =arch.ToJson();

	//make node
	tm t;
	CTools::GetTime(t);
	string node;
	char root[33]={0};
	arch.GetRootName(root);
	GetHashNode(root,t,node);

	////use hash
	//VDATA vdata;
	//RedisDBIdx dbi(m_pRedisClient);		
	//if (!dbi.CreateDBIndex(node.c_str(), DoHash, CACHE_TYPE_WR))
	//{
	//	m_pHandle->OnErr("create dbi err");
	//	return;
	//}
	////key
	//time_t tt=CTools::GetTime();
	//char key[33];
	//sprintf(key,"%d",tt);
	//vdata.push_back(key);
	//vdata.push_back(sz.c_str());
	//if(m_pRedisClient->hmset(dbi, node.c_str(), vdata))
	//{
	//	//
	//}

	//use zset
	int64_t nv = 0;
	VALUES vdata;
	RedisDBIdx dbi(m_pRedisClient);		
	if (!dbi.CreateDBIndex(node.c_str(), DoHash, CACHE_TYPE_WR))
	{
		m_pHandle->OnErr("create dbi err");
		return;
	}
	//key
	time_t tt=CTools::GetTime();
	char key[33];
	sprintf(key,"%d",tt);
	vdata.push_back(key);
	vdata.push_back(sz.c_str());
	if(m_pRedisClient->zadd(dbi, node.c_str(), vdata,nv))
	{

	}

}