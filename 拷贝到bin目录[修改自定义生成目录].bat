@echo ----------------------------------------------
@echo 修改cmake的生成路径，替换下面的build
@echo ----------------------------------------------

xcopy  /y /e /s ".\build\Debug\*.dll" ".\bin\Debug"  
xcopy  /y /e /s ".\build\Debug\*.exe" ".\bin\Debug"  
xcopy  /y /e /s ".\lib\*.dll" ".\bin\Debug"  
xcopy  /y /e /s ".\lib\*.xml" ".\bin\Debug"  
xcopy  /y /e /s ".\FGE26\dll\*.dll" ".\bin\Debug"  

#xcopy  /y /e /s ".\build\Release\*.dll" ".\bin\Release"  
#xcopy  /y /e /s ".\build\Release\*.exe" ".\bin\Release"  
#xcopy  /y /e /s ".\lib\*.dll" ".\bin\Release"  
#xcopy  /y /e /s ".\lib\*.xml" ".\bin\Release"  
#xcopy  /y /e /s ".\FGE26\dll\*.dll" ".\bin\Release"

#pause