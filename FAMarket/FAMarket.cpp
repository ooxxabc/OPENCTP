// FAMarket.cpp : 定义 DLL 应用程序的导出函数。
//
#include "FAMarket.h"
#include "FABase/AutoPt.h"


CInstrumentManage::CInstrumentManage()
{

}

CInstrumentManage::~CInstrumentManage()
{

}


void CInstrumentManage::Init(tagInstrument*pIns)
{
	tagInstrument ins;
	memcpy(&ins, pIns, sizeof(tagInstrument));
	m_mins.insert(std::make_pair(pIns->szINSTRUMENT,ins));
}

const tagInstrument & CInstrumentManage::GetInstrument(string szIns)
{
	return m_mins[szIns];
}

////////////////////////////////////////////

IInstrumentManage * pInstrumentManage = 0;

// 这是导出函数的一个示例。
FA_API IInstrumentManage*  CreateObject()
{
	if (!pInstrumentManage)
	{
		pInstrumentManage = new CInstrumentManage();
	}


    return pInstrumentManage;
}


FA_API int ReleaseObject(void)
{
	SAFE_DEL(pInstrumentManage);
	return 0;
}
