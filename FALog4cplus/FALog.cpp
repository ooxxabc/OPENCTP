// FALog.cpp : 定义 DLL 应用程序的导出函数。
//

#include "FALog.h"
#include "log4cplus/logger.h"
#include "log4cplus/fileappender.h"
#include "log4cplus/loggingmacros.h"

using namespace log4cplus;

CFALog * gFALog = 0;
SharedAppenderPtr _append;
Logger _logger;

// 这是已导出类的构造函数。
// 有关类定义的信息，请参阅 FALog.h
CFALog::CFALog()
{
	return;
}

// 这是导出函数的一个示例。
FA_API ILog* CreateObject(void)
{
	if (!gFALog)
	{
		gFALog = new CFALog();

		//init log4cplus
		log4cplus::initialize ();
		_append = new FileAppender(FASTR("Test.log"));
		_append->setName(L"filelogtest");
		/*step4:Instantiatealoggerobject*/
		_logger = Logger::getInstance(L"test.subtestof_filelog");
		/*step5:Attachtheappenderobjecttothelogger*/
		_logger.addAppender(_append);
	}

	return gFALog;
}

FA_API void ReleaseObject(void)
{
	SAFE_DEL(gFALog);
}

void CFALog::Log(int level,char* sz)
{
	switch (level)
	{
	case LOG_TRACE:
		LOG4CPLUS_TRACE(_logger,sz);
		break;
	case LOG_INFO:
		LOG4CPLUS_INFO(_logger,sz);
		break;
	case LOG_DEBUG:
		LOG4CPLUS_DEBUG(_logger,sz);
		break;
	case LOG_FATAL:
		LOG4CPLUS_FATAL(_logger,sz);
		break;
	case LOG_WARN:
		LOG4CPLUS_WARN(_logger,sz);
		break;
	case LOG_ERROR:
		LOG4CPLUS_ERROR(_logger,sz);
		break;

	}

	////test
	//LOG4CPLUS_DEBUG(_logger, FASTR("This is a short test...")<<90);

}