#include "FACSV2DB/load.h"

CSV2DB::CSV2DB(ICSV2DB*pDB)
	:lpbMapAddress(0)
	,pThread(0)
	,pInterface(pDB)
{

}

CSV2DB::~CSV2DB()
{

}

void FormateString(char *sz,const char*szCSV)
{

}

void*CSV2DB::loadFunc(const bool *bStop, void *param)
{
	CSV2DB * pThis = (CSV2DB*)(param);
	if (!pThis || !pThis->pInterface)
	{
		return 0;
	}

	std::string line;
	std::istringstream stream(pThis->lpbMapAddress);
	while (std::getline(stream, line))
	{
		if (line == "")
			break;

		char sz[256]={0};
		FormateString(sz,line.c_str());

		pThis->pInterface->Storge(sz);
	}

	pThis->unLoad();

	return 0;
}

bool CSV2DB::Load(char* szFileName)
{
	try
	{

		//load form share memory file
		// 创建文件对象
		HANDLE hFile = ::CreateFileA(szFileName, GENERIC_READ,FILE_SHARE_READ, NULL,OPEN_EXISTING, FILE_FLAG_RANDOM_ACCESS, NULL);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			throw GetLastError();
		}
		// 创建文件映射对象
		hFileMap = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, NULL);
		if (hFileMap == NULL)
		{
			throw GetLastError();
		}
		// 得到文件尺寸
		DWORD dwFileSizeHigh;
		__int64 qwFileSize = GetFileSize(hFile, &dwFileSizeHigh);
		qwFileSize += ((__int64)dwFileSizeHigh <<32);
		if (dwFileSizeHigh > 0)
		{
			//文件大于2G，暂不支持；以后可以优化循环内存映射加载（换行）
			throw 0;
		}
		// 映射视图
		lpbMapAddress = (char *)MapViewOfFile(hFileMap,FILE_MAP_READ,0,0,qwFileSize);
		if (lpbMapAddress == NULL)
		{
			throw GetLastError();
		} 
		// 关闭文件对象
		CloseHandle(hFile); 
		//////////////////////////////////////////////////////////////////////////


		pThread = new CYKThread(loadFunc,this);

	}
	catch(DWORD &errCode)
	{
		printf("错误代码:%d \n", errCode);
		return false;
	}

	return true;
}

void CSV2DB::unLoad()
{
	//release
	//释放最后数据块映射
	UnmapViewOfFile(lpbMapAddress);
	// 关闭文件映射对象句柄
	CloseHandle(hFileMap); 
}