#include "FAAlgorithm/FAAlgorithm.h"
#include <cmath>
using namespace std;

CFAAlgorithm::CFAAlgorithm(void)
{
}


CFAAlgorithm::~CFAAlgorithm(void)
{
}

double CFAAlgorithm::MA(double* d, int M)
{
  double ma = 0;
  for(int i = 0; i < M; ++i)
    ma += d[i];
  return ma /= M;
}

double CFAAlgorithm::STD(double* d, int M)
{
  double ma = MA(d, M);
  double std = 0;
  for(int i = 0; i < M; ++i)
  {
    std += (d[i]-ma)*(d[i]-ma);
  }
  return sqrt(std/M);
}

double CFAAlgorithm::SUM(double* d, int M)
{
  double sum = 0;
  for(int i = 0; i < M; ++i)
    sum += d[i];
  return sum;
}

double CFAAlgorithm::Lowest(double* d, int M)
{
	double rt = 0;
	for (int i = 0; i < M; ++i)
	{
		if (i == 0)
			rt = d[i];
		else
			rt = MIN(rt, d[i]);
	}
	return rt;
}

double CFAAlgorithm::Highest(double* d, int M)
{
	double rt = 0;
	for (int i = 0; i < M; ++i)
	{
		if (i == 0)
			rt = d[i];
		else
			rt = MAX(rt, d[i]);
	}
	return rt;
}

