#include "ctpImpl/CTPTradeSpi.h"
#include <time.h>

#define OutPutQuote(a)	m_pListCtrl->InsertItem(m_pListCtrl->GetItemCount(), a)

CCTPTradeSpi::CCTPTradeSpi(void)
:m_fPointMoney(1.0f)
, m_pITrade(NULL)
{
}


CCTPTradeSpi::~CCTPTradeSpi(void)
{
}


//----------------------------------------------------------------------------------------
// CThostFtdcTraderSpi impl - 连接登录响应
//----------------------------------------------------------------------------------------

// 当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
void CCTPTradeSpi::OnFrontConnected()
{
	m_pITrade->OnTradeConnected();
}

// 当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理
void CCTPTradeSpi::OnFrontDisconnected(int nReason)
{
	m_pITrade->OnTradeDisconnected(nReason);
}

// 登录请求响应
void CCTPTradeSpi::OnRspUserLogin(CThostFtdcRspUserLoginField* pRspUserLogin, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
	if (pRspInfo == NULL)
	{
		m_pITrade->OnTradeUserLoginFailed(0, "未知错误");
	}
	else
	{
		m_pITrade->OnTradeUserLoginSuccess(pRspUserLogin->SessionID, pRspUserLogin->FrontID,atoi(pRspUserLogin->MaxOrderRef));
	}
}


//----------------------------------------------------------------------------------------------------------------------
// CThostFtdcTraderSpi impl - 交易错误响应
//----------------------------------------------------------------------------------------------------------------------

// 错误应答
void CCTPTradeSpi::OnRspError(CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    if (pRspInfo != NULL)
    {
		m_pITrade->OnTradeError(pRspInfo->ErrorID, pRspInfo->ErrorMsg);
    }
}

//----------------------------------------------------------------------------------------------------------------------
// CThostFtdcTraderSpi impl - 结算单响应
//----------------------------------------------------------------------------------------------------------------------

// 投资者结算结果确认响应
void CCTPTradeSpi::OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField* pSettlementInfoConfirm, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    if (pSettlementInfoConfirm == NULL)
    {
		m_pITrade->OnTradeSettlementConfirmFailed();
    }
    else
    {
		m_pITrade->OnTradeSettlementConfirmSuccess();
    }
}

// 请求查询投资者结算结果响应
void CCTPTradeSpi::OnRspQrySettlementInfo(CThostFtdcSettlementInfoField* pSettlementInfo, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    if (pSettlementInfo == NULL)
    {
		m_pITrade->OnTradeQuerySettelment((tagSettlement*)NULL, bIsLast);
    }
    else
    {
        tagSettlement stSettlement;
        memset(&stSettlement, 0, sizeof(stSettlement));

        strcpy_s(stSettlement.szTradingDay, sizeof(stSettlement.szTradingDay), pSettlementInfo->TradingDay);
        stSettlement.nSettlementID = pSettlementInfo->SettlementID;
        strcpy_s(stSettlement.szBrokerID, sizeof(stSettlement.szBrokerID), pSettlementInfo->BrokerID);
        strcpy_s(stSettlement.szInvestorID, sizeof(stSettlement.szInvestorID), pSettlementInfo->InvestorID);
        stSettlement.nSequenceNo = pSettlementInfo->SequenceNo;
        strcpy_s(stSettlement.szContent, sizeof(stSettlement.szContent), pSettlementInfo->Content);

		m_pITrade->OnTradeQuerySettelment(&stSettlement, bIsLast);
    }
}

// 请求查询结算信息确认响应
void CCTPTradeSpi::OnRspQrySettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField* pSettlementInfoConfirm, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    // 无记录表示当天未确认结算单
	m_pITrade->OnTradeQueryConfirmSettlement(pSettlementInfoConfirm == NULL ? true : false);
}


//----------------------------------------------------------------------------------------------------------------------
// CThostFtdcTraderSpi impl - 报单响应
//----------------------------------------------------------------------------------------------------------------------

// 报单录入请求响应
void CCTPTradeSpi::OnRspOrderInsert(CThostFtdcInputOrderField* pInputOrder, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    // 报单不能通过CTP校验时被调用
    
    if (pRspInfo != NULL)
    {
		m_pITrade->OnTradeError(pRspInfo->ErrorID, pRspInfo->ErrorMsg, pInputOrder->RequestID);
    }
	else
	{

	}
}


// 报单通知
void CCTPTradeSpi::OnRtnOrder(CThostFtdcOrderField* pOrder)
{
    // 每次报单状态发生变化时被调用
    // 一次报单过程中会被触发多次: 1. CTP将报单向交易所提交时; 
    //                             2. 交易所撤销或接受报单时; 
    //                             3. 该报单提交时;
    if (pOrder != NULL)
    {
        tagOrder stOrder;
        memset(&stOrder, 0, sizeof(stOrder));

		stOrder.nSessionID = pOrder->SessionID;
        stOrder.nOrderID = atol(pOrder->OrderLocalID);
		stOrder.nOrderSysID = atol(pOrder->OrderSysID);
        strcpy_s(stOrder.szINSTRUMENT, sizeof(stOrder.szINSTRUMENT), pOrder->InstrumentID);
		strcpy_s(stOrder.szOrderRefCustom, sizeof(stOrder.szOrderRefCustom), pOrder->OrderRef);
		strcpy_s(stOrder.szExchangeID, sizeof(stOrder.szExchangeID), pOrder->ExchangeID);

        // 交易类型(投机, 套利, 套保)  
        switch (pOrder->CombHedgeFlag[0])
        {
        case THOST_FTDC_HF_Speculation:         stOrder.nTradeType = TRADE_TYPE_SPECULATION; break;
        case THOST_FTDC_HF_Arbitrage:           stOrder.nTradeType = TRADE_TYPE_ARBITRAGE; break;
        case THOST_FTDC_HF_Hedge:               stOrder.nTradeType = TRADE_TYPE_HEDGE; break;
        }

        // 交易买卖方向
        switch (pOrder->Direction)
        {
        case THOST_FTDC_D_Buy:                  stOrder.nTradeDir = TRADE_DIR_BUY; break;
        case THOST_FTDC_D_Sell:                 stOrder.nTradeDir = TRADE_DIR_SELL; break;
        }

        // 交易开平类型
        switch (pOrder->CombOffsetFlag[0])
        {
        case THOST_FTDC_OF_Open:                stOrder.nTradeOperate = TRADE_OPERATE_OPEN; break;
        case THOST_FTDC_OF_CloseToday:          stOrder.nTradeOperate = TRADE_OPERATE_CLOSE_TODAY; break;
        case THOST_FTDC_OF_Close:               stOrder.nTradeOperate = TRADE_OPERATE_CLOSE; break;
        }

        // 报单状态
        switch (pOrder->OrderStatus)
        {
        case THOST_FTDC_OST_AllTraded:          
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_TRADED; 
			break;
        case THOST_FTDC_OST_PartTradedQueueing: 
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_PARTIAL; 
			break;
        case THOST_FTDC_OST_Canceled:           
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_CANCELED; 
			break;
		case THOST_FTDC_OST_PartTradedNotQueueing:			
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_NOTPARTIAL; 
			break;
		case THOST_FTDC_OST_NoTradeQueueing:			
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_WAIT; 
			break;
		case THOST_FTDC_OST_NoTradeNotQueueing:
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_NOTWAIT; 
			break;
        default:                                
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_UNKNOW;
			break;
        }

        stOrder.dLimitPrice = pOrder->LimitPrice;
        //stOrder.dAvgPrice = ;
        stOrder.nVolume = pOrder->VolumeTotalOriginal;
        stOrder.nTradeVolume = pOrder->VolumeTotalOriginal;
        stOrder.nTradeVolumeLeft = pOrder->VolumeTotalOriginal;

        strcpy_s(stOrder.szInsertDateTime, sizeof(stOrder.szInsertDateTime), pOrder->InsertTime);
        //strcpy_s(stOrder.szTradeDateTime, sizeof(stOrder.szTradeDateTime), pOrder->UpdateTime);
		strcpy_s(stOrder.szSoftFlag, sizeof(stOrder.szSoftFlag), pOrder->UserProductInfo);

        //int nLen = strlen(pOrder->OrderRef);
        //if (nLen > strlen(PROXY_PRODUCT_FLAG)) for (int i=0; i<6; ++i) stOrder.szOrderRefCustom[i] = pOrder->OrderRef[nLen-6+i];        

        // 报单被拒绝
        if (stOrder.nOrderStatus == TRADE_ORDER_STATUS_CANCELED)
        {
			//m_pITrade->OnTradeError(-1, pOrder->StatusMsg);@ykit
        }

		stOrder.nRequestID = pOrder->RequestID;
		if (stOrder.nTimestampCreate == 0)
			stOrder.nTimestampCreate = time(0);
		else
			stOrder.nTimestampUpdate = time(0);

        // 通知报单情况
		m_pITrade->OnTradeRtnOrder(&stOrder);
    }
}

// 成交通知
void CCTPTradeSpi::OnRtnTrade(CThostFtdcTradeField* pTrade)
{
    // 报单通过交易所撮合成交, 通过此函数返回该笔成交
    // 成交之后, 一个OnRtnOrder和一个OnRtnTrade会被调用
    if (pTrade != NULL)
    {
        tagTrade stTrade;
        memset(&stTrade, 0, sizeof(stTrade));

        strcpy_s(stTrade.szTradeID, sizeof(stTrade.szTradeID), pTrade->TradeID);
        stTrade.nOrderID = atol(pTrade->OrderLocalID);
		stTrade.nSystemID = atol(pTrade->OrderSysID);
        strcpy_s(stTrade.szINSTRUMENT, sizeof(stTrade.szINSTRUMENT), pTrade->InstrumentID);
        strcpy_s(stTrade.szExchangeID, sizeof(stTrade.szExchangeID), pTrade->ExchangeID);

        //// 交易类型(投机, 套利, 套保)  
        //switch (pTrade->HedgeFlag)
        //{
        //case THOST_FTDC_HF_Speculation:         stTrade.nTradeType = TRADE_TYPE_SPECULATION; break;
        //case THOST_FTDC_HF_Arbitrage:           stTrade.nTradeType = TRADE_TYPE_ARBITRAGE; break;
        //case THOST_FTDC_HF_Hedge:               stTrade.nTradeType = TRADE_TYPE_HEDGE; break;
        //}

        // 交易买卖方向
        switch (pTrade->Direction)
        {
        case THOST_FTDC_D_Buy:                  stTrade.nTradeDir = TRADE_DIR_BUY; break;
        case THOST_FTDC_D_Sell:                 stTrade.nTradeDir = TRADE_DIR_SELL; break;
        }

        // 交易开平类型
        switch (pTrade->OffsetFlag)
        {
        case THOST_FTDC_OF_Open:                stTrade.nTradeOperate = TRADE_OPERATE_OPEN; break;
        case THOST_FTDC_OF_CloseToday:          stTrade.nTradeOperate = TRADE_OPERATE_CLOSE_TODAY; break;
        case THOST_FTDC_OF_Close:               stTrade.nTradeOperate = TRADE_OPERATE_CLOSE; break;
		case THOST_FTDC_OF_CloseYesterday:      stTrade.nTradeOperate = TRADE_OPERATE_CLOSE; break;
        }

        stTrade.dPrice = pTrade->Price;
        stTrade.nVolume = pTrade->Volume;

        strcpy_s(stTrade.szTradeTime, sizeof(stTrade.szTradeTime), pTrade->TradeTime);
        strcpy_s(stTrade.szTradingDay, sizeof(stTrade.szTradingDay), pTrade->TradingDay);

		m_pITrade->OnTradeRtnTrade(&stTrade);
    }
}


// 报单录入错误回报
void CCTPTradeSpi::OnErrRtnOrderInsert(CThostFtdcInputOrderField* pInputOrder, CThostFtdcRspInfoField* pRspInfo)
{
    // 报单不能通过交易所校验时使用OnRtnOrder, OnErrRtnOrderInsert响应
    if (pRspInfo != NULL)
    {
		m_pITrade->OnTradeError(pRspInfo->ErrorID, pRspInfo->ErrorMsg,pInputOrder->RequestID);
    }
}

// 请求查询报单响应
void CCTPTradeSpi::OnRspQryOrder(CThostFtdcOrderField* pOrder, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    if (pOrder != NULL)
    {
        tagOrder stOrder;
        memset(&stOrder, 0, sizeof(stOrder));

		stOrder.nOrderSysID = atoi(pOrder->OrderSysID);
        stOrder.nOrderID = atoi(pOrder->OrderLocalID);
        strcpy_s(stOrder.szINSTRUMENT, sizeof(stOrder.szINSTRUMENT), pOrder->InstrumentID);

        // 交易类型(投机, 套利, 套保)  
        switch (pOrder->CombHedgeFlag[0])
        {
        case THOST_FTDC_HF_Speculation:         stOrder.nTradeType = TRADE_TYPE_SPECULATION; break;
        case THOST_FTDC_HF_Arbitrage:           stOrder.nTradeType = TRADE_TYPE_ARBITRAGE; break;
        case THOST_FTDC_HF_Hedge:               stOrder.nTradeType = TRADE_TYPE_HEDGE; break;
        }

        // 交易买卖方向
        switch (pOrder->Direction)
        {
        case THOST_FTDC_D_Buy:                  stOrder.nTradeDir = TRADE_DIR_BUY; break;
        case THOST_FTDC_D_Sell:                 stOrder.nTradeDir = TRADE_DIR_SELL; break;
        }

        // 交易开平类型
        switch (pOrder->CombOffsetFlag[0])
        {
        case THOST_FTDC_OF_Open:                stOrder.nTradeOperate = TRADE_OPERATE_OPEN; break;
        case THOST_FTDC_OF_CloseToday:          stOrder.nTradeOperate = TRADE_OPERATE_CLOSE_TODAY; break;
        case THOST_FTDC_OF_Close:               stOrder.nTradeOperate = TRADE_OPERATE_CLOSE; break;
        }

        // 报单状态

		switch (pOrder->OrderStatus)
		{
		case THOST_FTDC_OST_AllTraded:
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_TRADED;
			break;
		case THOST_FTDC_OST_PartTradedQueueing:
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_PARTIAL;
			break;
		case THOST_FTDC_OST_Canceled:
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_CANCELED;
			break;
		case THOST_FTDC_OST_PartTradedNotQueueing:
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_NOTPARTIAL;
			break;
		case THOST_FTDC_OST_NoTradeQueueing:
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_WAIT;
			break;
		case THOST_FTDC_OST_NoTradeNotQueueing:
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_NOTWAIT;
			break;
		default:
			stOrder.nOrderStatus = TRADE_ORDER_STATUS_UNKNOW;
			break;
		}


        stOrder.dLimitPrice = pOrder->LimitPrice;
        //stOrder.dAvgPrice = ;
        stOrder.nVolume = pOrder->VolumeTotalOriginal;
        stOrder.nTradeVolume = pOrder->VolumeTotalOriginal;
        stOrder.nTradeVolumeLeft = pOrder->VolumeTotalOriginal;

        strcpy_s(stOrder.szInsertDateTime, sizeof(stOrder.szInsertDateTime), pOrder->InsertTime);
        strcpy_s(stOrder.szExchangeID, sizeof(stOrder.szExchangeID), pOrder->ExchangeID);

        //int nLen = strlen(pOrder->OrderRef);
        //if (nLen > strlen(PROXY_PRODUCT_FLAG)) for (int i=0; i<6; ++i) stOrder.szOrderRefCustom[i] = pOrder->OrderRef[nLen-6+i];
		stOrder.dAvgPrice = pOrder->LimitPrice;

		m_pITrade->OnTradeQueryOrder(&stOrder, bIsLast);
    }
}

// 请求查询成交响应
void CCTPTradeSpi::OnRspQryTrade(CThostFtdcTradeField* pTrade, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    if (pTrade != NULL)
    {
        tagTrade stTrade;
        memset(&stTrade, 0, sizeof(stTrade));

		stTrade.nSystemID = atoi(pTrade->OrderSysID);
        strcpy_s(stTrade.szTradeID, sizeof(stTrade.szTradeID), pTrade->TradeID);
        stTrade.nOrderID = atol(pTrade->OrderLocalID);
        strcpy_s(stTrade.szINSTRUMENT, sizeof(stTrade.szINSTRUMENT), pTrade->InstrumentID);
        strcpy_s(stTrade.szExchangeID, sizeof(stTrade.szExchangeID), pTrade->ExchangeID);

        //// 交易类型(投机, 套利, 套保)  
        //switch (pTrade->HedgeFlag)
        //{
        //case THOST_FTDC_HF_Speculation:         stTrade.nTradeType = TRADE_TYPE_SPECULATION; break;
        //case THOST_FTDC_HF_Arbitrage:           stTrade.nTradeType = TRADE_TYPE_ARBITRAGE; break;
        //case THOST_FTDC_HF_Hedge:               stTrade.nTradeType = TRADE_TYPE_HEDGE; break;
        //}

        // 交易买卖方向
        switch (pTrade->Direction)
        {
        case THOST_FTDC_D_Buy:                  stTrade.nTradeDir = TRADE_DIR_BUY; break;
        case THOST_FTDC_D_Sell:                 stTrade.nTradeDir = TRADE_DIR_SELL; break;
        }

        // 交易开平类型
        switch (pTrade->OffsetFlag)
        {
        case THOST_FTDC_OF_Open:                stTrade.nTradeOperate = TRADE_OPERATE_OPEN; break;
        case THOST_FTDC_OF_CloseToday:          stTrade.nTradeOperate = TRADE_OPERATE_CLOSE_TODAY; break;
        case THOST_FTDC_OF_Close:               stTrade.nTradeOperate = TRADE_OPERATE_CLOSE; break;
        }

        stTrade.dPrice = pTrade->Price;
        stTrade.nVolume = pTrade->Volume;

        strcpy_s(stTrade.szTradeTime, sizeof(stTrade.szTradeTime), pTrade->TradeTime);
        strcpy_s(stTrade.szTradingDay, sizeof(stTrade.szTradingDay), pTrade->TradingDay);

		m_pITrade->OnTradeQueryTrade(&stTrade, bIsLast);
    }
}

// 报单操作请求响应
void CCTPTradeSpi::OnRspOrderAction(CThostFtdcInputOrderActionField* pInputOrderAction, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    if (pRspInfo != NULL)
    {
		m_pITrade->OnTradeError(pRspInfo->ErrorID, pRspInfo->ErrorMsg,pInputOrderAction->RequestID);
    }
}

// 报单操作错误回报
void CCTPTradeSpi::OnErrRtnOrderAction(CThostFtdcOrderActionField* pOrderAction, CThostFtdcRspInfoField* pRspInfo)
{
    if (pRspInfo != NULL)
    {
		m_pITrade->OnTradeError(pRspInfo->ErrorID, pRspInfo->ErrorMsg,pOrderAction->RequestID);
    }
}

//----------------------------------------------------------------------------------------------------------------------
// CThostFtdcTraderSpi impl - 持仓响应
//----------------------------------------------------------------------------------------------------------------------

// 请求查询投资者持仓响应
void CCTPTradeSpi::OnRspQryInvestorPosition(CThostFtdcInvestorPositionField* pInvestorPosition, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    if (pInvestorPosition == NULL)
    {
		m_pITrade->OnTradeQueryPosition((tagPosition*)NULL, bIsLast);
    }
    else
    {
        tagPosition stPosition;
        memset(&stPosition, 0, sizeof(stPosition));

        strcpy_s(stPosition.szINSTRUMENT, sizeof(stPosition.szINSTRUMENT), pInvestorPosition->InstrumentID);

        //// 交易类型(投机,套利,套保)
        //switch (pInvestorPosition->HedgeFlag)
        //{
        //case  THOST_FTDC_HF_Speculation:    stPosition.nTradeType = TRADE_TYPE_SPECULATION; break;
        //case  THOST_FTDC_HF_Arbitrage:      stPosition.nTradeType = TRADE_TYPE_ARBITRAGE; break;
        //case  THOST_FTDC_HF_Hedge:          stPosition.nTradeType = TRADE_TYPE_HEDGE; break;
        //}

        // 交易买卖方向
        switch (pInvestorPosition->PosiDirection)
        {
        case THOST_FTDC_PD_Long:            stPosition.nTradeDir = TRADE_DIR_BUY; break;
        case THOST_FTDC_PD_Short:           stPosition.nTradeDir = TRADE_DIR_SELL; break;
        }

		stPosition.dPositionProfit = pInvestorPosition->PositionProfit;
        stPosition.nPosition = pInvestorPosition->Position;
        stPosition.nTodayPosition = pInvestorPosition->TodayPosition;
        stPosition.nYesterdayPosition = stPosition.nPosition - stPosition.nTodayPosition;
		stPosition.dAvgPrice = pInvestorPosition->OpenCost / m_fPointMoney / (pInvestorPosition->TodayPosition > 0 ? pInvestorPosition->TodayPosition:1);
        //stPosition.dAvgPrice = stPosition->Position == 0 ? 0 : (pInvestorPosition->PositionCost / _id_INSTRUMENT1[pInvestorPosition->InstrumentID].VolumeMultiple / stPosition->Position);

		m_pITrade->OnTradeQueryPosition(&stPosition, bIsLast);
    }
}

// 请求查询投资者持仓明细响应
void CCTPTradeSpi::OnRspQryInvestorPositionDetail(CThostFtdcInvestorPositionDetailField* pInvestorPositionDetail, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    if (pInvestorPositionDetail == NULL)
    {
		m_pITrade->OnTradeQueryPositionDetail((tagPositionDetail*)NULL, bIsLast);
    }
    else
    {
        tagPositionDetail stDetail;
        memset(&stDetail, 0, sizeof(stDetail));

        strcpy_s(stDetail.szBrokerID, sizeof(stDetail.szBrokerID), pInvestorPositionDetail->BrokerID);
        strcpy_s(stDetail.szInvestorID, sizeof(stDetail.szInvestorID), pInvestorPositionDetail->InvestorID);
        strcpy_s(stDetail.szTradeID, sizeof(stDetail.szTradeID), pInvestorPositionDetail->TradeID);
        strcpy_s(stDetail.szINSTRUMENT, sizeof(stDetail.szINSTRUMENT), pInvestorPositionDetail->InstrumentID);
        strcpy_s(stDetail.szExchangeID, sizeof(stDetail.szExchangeID), pInvestorPositionDetail->ExchangeID);

        // 交易类型(投机, 套利, 套保)
        switch (pInvestorPositionDetail->HedgeFlag)
        {
        case THOST_FTDC_HF_Speculation: stDetail.nTradeType = TRADE_TYPE_SPECULATION; break;
        case THOST_FTDC_HF_Arbitrage:   stDetail.nTradeType = TRADE_TYPE_ARBITRAGE; break;
        case THOST_FTDC_HF_Hedge:       stDetail.nTradeType = TRADE_TYPE_HEDGE; break;
        }

        // 交易买卖方向
        switch (pInvestorPositionDetail->Direction)
        {
        case THOST_FTDC_PD_Long:        stDetail.nTradeDir = TRADE_DIR_BUY; break;
        case THOST_FTDC_PD_Short:       stDetail.nTradeDir = TRADE_DIR_SELL; break;
        }

        stDetail.dOpenPrice = pInvestorPositionDetail->OpenPrice;
        stDetail.nVolume = pInvestorPositionDetail->Volume;

        strcpy_s(stDetail.szOpenDate, sizeof(stDetail.szOpenDate), pInvestorPositionDetail->OpenDate);

		m_pITrade->OnTradeQueryPositionDetail(&stDetail, bIsLast);
    }
}


// 请求查询资金账户响应
void CCTPTradeSpi::OnRspQryTradingAccount(CThostFtdcTradingAccountField* pTradingAccount, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    if (pTradingAccount == NULL)
    {
		m_pITrade->OnTradeQueryTradingAccount((tagTradingAccount*)NULL, bIsLast);
    }
    else
    {
        tagTradingAccount stAccount;
        memset(&stAccount, 0, sizeof(stAccount));

        stAccount.dPreBalance = pTradingAccount->PreBalance;
        stAccount.dPositionProfit = pTradingAccount->PositionProfit;
        stAccount.dCloseProfit = pTradingAccount->CloseProfit;
        stAccount.dCommission = pTradingAccount->Commission;
        stAccount.dCurrentMargin = pTradingAccount->CurrMargin;
        stAccount.dFrozenCapital = pTradingAccount->FrozenCash;
        stAccount.dAvaiableCapital = pTradingAccount->Available;
        stAccount.dDynamicEquity = stAccount.dPreBalance + stAccount.dCloseProfit + stAccount.dPositionProfit + pTradingAccount->Deposit - pTradingAccount->Withdraw;

		m_pITrade->OnTradeQueryTradingAccount(&stAccount, bIsLast);
    }
}

// 请求查询合约响应
void CCTPTradeSpi::OnRspQryInstrument(CThostFtdcInstrumentField* pINSTRUMENT1, CThostFtdcRspInfoField* pRspInfo, int nRequestID, bool bIsLast)
{
    tagInstrument stINSTRUMENT1;
    memset(&stINSTRUMENT1, 0, sizeof(stINSTRUMENT1));

    if (pINSTRUMENT1 != NULL)
    {
        strcpy_s(stINSTRUMENT1.szINSTRUMENT, sizeof(stINSTRUMENT1.szINSTRUMENT), pINSTRUMENT1->InstrumentID);
        strcpy_s(stINSTRUMENT1.szExchangeID, sizeof(stINSTRUMENT1.szExchangeID), pINSTRUMENT1->ExchangeID);
        strcpy_s(stINSTRUMENT1.szProductID, sizeof(stINSTRUMENT1.szProductID), pINSTRUMENT1->ProductID);
        stINSTRUMENT1.dMinMove  = pINSTRUMENT1->PriceTick;
        stINSTRUMENT1.nMultiple = pINSTRUMENT1->VolumeMultiple;
    }

	m_pITrade->OnTradeQueryInstrument(&stINSTRUMENT1, bIsLast);
}