
#include "ctpImpl/CTPQuoteApi.h"
#include "FABase/common.h"

//#pragma comment(lib,"../lib/thostmduserapi.lib")

//********************************************CTPQuoteApi*******************************************************//
int CCTPQuoteApi::m_nRequestSeq = 0;
CCTPQuoteApi::CCTPQuoteApi(void)
{
	m_pThostMdApi = NULL;
	m_nSessionID = 0;
}

CCTPQuoteApi::~CCTPQuoteApi(void)
{

}


// 初始化
bool CCTPQuoteApi::Init(CThostFtdcMdSpi* pThostMdSpi, char* FrontAddr)
{
	m_pThostMdApi = CThostFtdcMdApi::CreateFtdcMdApi();
	if (m_pThostMdApi == NULL) return false;

	m_pThostMdApi->RegisterSpi(pThostMdSpi);
	m_pThostMdApi->RegisterFront(FrontAddr);
	m_pThostMdApi->Init();
	//m_pThostMdApi->Join();

	return true;
}

// 清理
void CCTPQuoteApi::Fini()
{
	if (m_pThostMdApi == NULL) return;

	m_pThostMdApi->RegisterSpi(NULL);
	m_pThostMdApi->Release();
	m_pThostMdApi = NULL;
}


// 登录
bool CCTPQuoteApi::Login(char* pszBroker, char* pszInvestor, char* pszPassword)
{
	if (m_pThostMdApi == NULL) return false;

	CThostFtdcReqUserLoginField stLoginField;
	memset(&stLoginField, 0, sizeof(stLoginField));

	strcpy_s(stLoginField.BrokerID, sizeof(stLoginField.BrokerID), pszBroker);
	strcpy_s(stLoginField.UserID, sizeof(stLoginField.UserID), pszInvestor);   
	strcpy_s(stLoginField.Password, sizeof(stLoginField.Password), pszPassword);
	//strcpy_s(stLoginField.UserProductInfo, sizeof(stLoginField.UserProductInfo), "@YunXiang");

	strcpy_s(m_szBrokerID, sizeof(m_szBrokerID), pszBroker);
	strcpy_s(m_szInvestorID, sizeof(m_szBrokerID), pszInvestor);

	if (m_pThostMdApi->ReqUserLogin(&stLoginField, ++m_nRequestSeq) == 0) return true;
	else return false;

}

// 断开前置
void CCTPQuoteApi::Disconnect()
{
	if (m_pThostMdApi == NULL) return;

	m_pThostMdApi->RegisterSpi(NULL);
	m_pThostMdApi->Release();
	m_pThostMdApi = NULL;

	m_nSessionID = 0;
}

// 订阅行情
bool CCTPQuoteApi::SubMarketData(char* pszInstrumentID[], int nCount)
{
	_ASSERTE(m_pThostMdApi != NULL);
	if (m_pThostMdApi == NULL) return false;

	if (m_pThostMdApi->SubscribeMarketData(pszInstrumentID, nCount) == 0) return true;
	else return false;
}
// 退订行情
bool CCTPQuoteApi::UnSubMarketData(char* pszInstrumentID[], int nCount)
{
	_ASSERTE(m_pThostMdApi != NULL);
	if (m_pThostMdApi == NULL) return false;

	if (m_pThostMdApi->UnSubscribeMarketData(pszInstrumentID, nCount) == 0) return true;
	else return false;
}

// 获取当前交易日
const char* CCTPQuoteApi::GetTradingDay()
{
	if (m_pThostMdApi == NULL || m_nSessionID == 0) return "";
	return m_pThostMdApi->GetTradingDay();
}