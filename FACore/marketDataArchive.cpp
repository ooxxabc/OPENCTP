#pragma once

#include "ctpImpl/marketDataArchive.h"
#include "FABase/tools.h"

void tagMarketDataArchive::ToWrite(Writer<StringBuffer> &writer)
{
	RapidjsonWriteBegin(writer);
	//custom
	RapidjsonWriteChar(szINSTRUMENT);
	RapidjsonWriteDouble(dBidPrice1);
	RapidjsonWriteInt(nBidVolume1);
	RapidjsonWriteDouble(dAskPrice1);
	RapidjsonWriteInt(nAskVolume1);
	RapidjsonWriteDouble(dLastPrice);
	RapidjsonWriteDouble(dAvgPrice);
	RapidjsonWriteInt(nVolume);
	RapidjsonWriteDouble(dOpenInt);
	RapidjsonWriteDouble(dUpperLimitPrice);
	RapidjsonWriteDouble(dLowerLimitPrice);
	RapidjsonWriteChar(szUpdateTime);
	RapidjsonWriteInt(nUpdateMillisec);
	//
	RapidjsonWriteEnd();
}

void tagMarketDataArchive::ParseJson(const Value& val)
{
	//RapidjsonParseBegin(val);
	////custom
	//RapidjsonParseToChar(szINSTRUMENT);
	//RapidjsonParseToDouble(dBidPrice1);
	//RapidjsonParseToInt(nBidVolume1);
	//RapidjsonParseToDouble(dAskPrice1);
	//RapidjsonParseToInt(nAskVolume1);
	//RapidjsonParseToDouble(dLastPrice);
	//RapidjsonParseToDouble(dAvgPrice);
	//RapidjsonParseToInt(nVolume);
	//RapidjsonParseToDouble(dOpenInt);
	//RapidjsonParseToDouble(dUpperLimitPrice);
	//RapidjsonParseToDouble(dLowerLimitPrice);
	//RapidjsonParseToChar(szUpdateTime);
	//RapidjsonParseToInt(nUpdateMillisec);
	////
	//RapidjsonParseEnd();
}

void tagMarketDataArchive::GetRootName(char *root)
{
	sprintf_s(root,31,"%s",szINSTRUMENT);
	for (int i=0;i<strlen(szINSTRUMENT);i++)
	{
		if (CTools::IsDigital(szINSTRUMENT[i]))
		{
			root[i]='\0';
			break;
		}
	}
}