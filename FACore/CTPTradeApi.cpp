#include "ctpImpl/CTPTradeApi.h"
#include <string>
#include "FABase/common.h"

//#pragma comment(lib,"../lib/thosttraderapi.lib")

volatile int CCTPTradeApi::m_nRequestSeq = 0;

CCTPTradeApi::CCTPTradeApi()
	:
	m_pTradeApi(NULL),
	m_nSessionID(0),
	m_nFrontID(0),
	m_szBrokerID(""),
	m_szInvestorID("")
{
}

CCTPTradeApi::~CCTPTradeApi()
{
	Fini();
}

//----------------------------------------------------------------------------------------
// ITrade impl - 基础接口
//----------------------------------------------------------------------------------------

/*
20. 程序使用 TradeApi 和 MdApi，并且把这 2 个 dll 放在同一个目录下。程序再次启动后，
如果某个 api 采用 Resume 模式订阅公有流/私有流，就会去参考相关的本地流文件。可
能会导致数据异常？ 
【答：】相同目录下的 2 个 dll 会把数据写入相同本地流文件，导致 2 个 dll 不断的覆盖
对方写下的流文件。程序再次启动时，TradeApi 可能去参考 MdApi 写下的流文件，所以
导致数据流不连续。解决方法：如果一定要把 2 个 dll 放在相同的目录下，可以在创建
api 时指定流文件的路径。使得不同的 dll 写入不同流文件。 
static CThostFtdcUserApi *CreateFtdcUserApi(const char *pszFlowPath = "", const bool 
bIsUsingUdp=false); 
*/

// 初始化
bool CCTPTradeApi::Init(CThostFtdcTraderSpi* pThostTradeSpi, char* FrontAddr)
{
	m_pTradeApi = CThostFtdcTraderApi::CreateFtdcTraderApi();
	if (m_pTradeApi == NULL) return false;

	m_pTradeApi->RegisterSpi(pThostTradeSpi);
	// 订阅私有流
	// TERT_RESTART:从本交易日开始重传
	// TERT_RESUME:从上次收到的续传
	// TERT_QUICK:只传送登录后私有流的内容
	m_pTradeApi->SubscribePrivateTopic(THOST_TERT_RESTART);
	// 订阅公共流
	// TERT_RESTART:从本交易日开始重传
	// TERT_RESUME:从上次收到的续传
	// TERT_QUICK:只传送登录后公共流的内容
	m_pTradeApi->SubscribePublicTopic(THOST_TERT_RESTART);
	m_pTradeApi->RegisterFront(FrontAddr);
	m_pTradeApi->Init();
	return true;
}

// 清理
void CCTPTradeApi::Fini()
{
	Disconnect();
}



// 登录
bool CCTPTradeApi::Login(const char* pszBroker, const char* pszInvestor, const char* pszPassword)
{
	_ASSERTE(pszBroker != NULL && pszInvestor != NULL && pszPassword != NULL);
	if (pszBroker == NULL || pszInvestor == NULL || pszPassword == NULL) return false;

	_ASSERTE(m_pTradeApi != NULL);
	if (m_pTradeApi == NULL) return false;

	CThostFtdcReqUserLoginField stLoginField;
	memset(&stLoginField, 0, sizeof(stLoginField));

	strcpy_s(stLoginField.BrokerID, sizeof(stLoginField.BrokerID), pszBroker);
	strcpy_s(stLoginField.UserID, sizeof(stLoginField.UserID), pszInvestor);
	strcpy_s(stLoginField.Password, sizeof(stLoginField.Password), pszPassword);
	strcpy_s(stLoginField.UserProductInfo, sizeof(stLoginField.UserProductInfo), PROXY_PRODUCT_FLAG);

	m_szBrokerID = pszBroker;
	m_szInvestorID = pszInvestor;

	if (m_pTradeApi->ReqUserLogin(&stLoginField, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 断开前置
void CCTPTradeApi::Disconnect()
{
	if (m_pTradeApi == NULL) return;

	m_pTradeApi->RegisterSpi(NULL);
	m_pTradeApi->Release();
	m_pTradeApi = NULL;

	m_nSessionID = 0;
	m_nFrontID = 0;
}

// 获取当前交易日
const char* CCTPTradeApi::GetTradingDay()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return "";

	return m_pTradeApi->GetTradingDay();
}

//----------------------------------------------------------------------------------------
// ITrade impl - 结算单接口
//----------------------------------------------------------------------------------------

// 确认结算单
bool CCTPTradeApi::ConfirmSettlement()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcSettlementInfoConfirmField stConfirm;
	memset(&stConfirm, 0, sizeof(stConfirm));

	strcpy_s(stConfirm.BrokerID, sizeof(stConfirm.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stConfirm.InvestorID, sizeof(stConfirm.InvestorID), m_szInvestorID.c_str());

	if (m_pTradeApi->ReqSettlementInfoConfirm(&stConfirm, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 查询结算单确认情况
bool CCTPTradeApi::QuerySettlementConfirm()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQrySettlementInfoConfirmField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.BrokerID, sizeof(stQuery.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stQuery.InvestorID, sizeof(stQuery.InvestorID), m_szInvestorID.c_str());

	if (m_pTradeApi->ReqQrySettlementInfoConfirm(&stQuery, ++m_nRequestSeq) == 0) 
		return true;
	else 
		return false;
}

// 查询结算单(不提供交易日,则默认为上一交易日)
bool CCTPTradeApi::QuerySettlement(const char* pszTradingDay/*=NULL*/)
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQrySettlementInfoField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.BrokerID, sizeof(stQuery.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stQuery.InvestorID, sizeof(stQuery.InvestorID), m_szInvestorID.c_str());
	if (pszTradingDay != NULL) strcpy_s(stQuery.TradingDay, sizeof(stQuery.TradingDay), pszTradingDay);

	if (m_pTradeApi->ReqQrySettlementInfo(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}

//----------------------------------------------------------------------------------------
// ITrade impl - 账户接口
//----------------------------------------------------------------------------------------

// 修改账户密码
bool CCTPTradeApi::UpdateUserPassword(const char* pszOldPassword, const char* pszNewPassword)
{
	_ASSERTE(pszOldPassword != NULL && strlen(pszOldPassword) != 0);
	_ASSERTE(pszNewPassword != NULL && strlen(pszNewPassword) != 0);
	if (pszOldPassword == NULL || strlen(pszOldPassword) == 0) return false;
	if (pszNewPassword == NULL || strlen(pszNewPassword) == 0) return false;    

	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcUserPasswordUpdateField stUpdate;
	memset(&stUpdate, 0, sizeof(stUpdate));

	strcpy_s(stUpdate.BrokerID, sizeof(stUpdate.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stUpdate.UserID, sizeof(stUpdate.UserID), m_szInvestorID.c_str());
	strcpy_s(stUpdate.OldPassword, sizeof(stUpdate.OldPassword), pszOldPassword);
	strcpy_s(stUpdate.NewPassword, sizeof(stUpdate.NewPassword), pszNewPassword);

	if (m_pTradeApi->ReqUserPasswordUpdate(&stUpdate, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 修改资金账户密码
bool CCTPTradeApi::UpdateTradingAccountPassword(const char* pszOldPassword, const char* pszNewPassword)
{
	_ASSERTE(pszOldPassword != NULL && strlen(pszOldPassword) != 0);
	_ASSERTE(pszNewPassword != NULL && strlen(pszNewPassword) != 0);
	if (pszOldPassword == NULL || strlen(pszOldPassword) == 0) return false;
	if (pszNewPassword == NULL || strlen(pszNewPassword) == 0) return false;

	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcTradingAccountPasswordUpdateField stUpdate;
	memset(&stUpdate, 0, sizeof(stUpdate));

	strcpy_s(stUpdate.BrokerID, sizeof(stUpdate.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stUpdate.AccountID, sizeof(stUpdate.AccountID), m_szInvestorID.c_str());
	strcpy_s(stUpdate.OldPassword, sizeof(stUpdate.OldPassword), pszOldPassword);
	strcpy_s(stUpdate.NewPassword, sizeof(stUpdate.NewPassword), pszNewPassword);
	//strcpy_s(stUpdate.CurrencyID, sizeof(stUpdate.CurrencyID), pszCurrencyID);

	if (m_pTradeApi->ReqTradingAccountPasswordUpdate(&stUpdate, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 查询资金账户
bool CCTPTradeApi::QueryTradingAccount()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryTradingAccountField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.BrokerID, sizeof(stQuery.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stQuery.InvestorID, sizeof(stQuery.InvestorID), m_szInvestorID.c_str());
	//strcpy_s(stQuery.CurrencyID, sizeof(stQuery.CurrencyID), pszCurrencyID);

	if (m_pTradeApi->ReqQryTradingAccount(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}


//----------------------------------------------------------------------------------------
// ITrade impl - 基础查询
//----------------------------------------------------------------------------------------

// 查询交易所
bool CCTPTradeApi::QueryExchange(const char* pszExchangeID)
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryExchangeField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.ExchangeID, sizeof(stQuery.ExchangeID), pszExchangeID);

	if (m_pTradeApi->ReqQryExchange(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 查询合约
bool CCTPTradeApi::QueryInstrument(const char* pszInstrumentID)
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryInstrumentField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));
	if (pszInstrumentID != 0)
	{
		strcpy_s(stQuery.InstrumentID, sizeof(stQuery.InstrumentID), pszInstrumentID);
	}

	/////交易所代码
	//TThostFtdcExchangeIDType	ExchangeID;
	/////合约在交易所的代码
	//TThostFtdcExchangeInstIDType	ExchangeInstID;
	/////产品代码
	//TThostFtdcINSTRUMENT1IDType	ProductID;

	if (m_pTradeApi->ReqQryInstrument(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 查询商品
bool CCTPTradeApi::QueryProduct(const char* pszProductID, enProductType nProductType)
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryProductField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.ProductID, sizeof(stQuery.ProductID), pszProductID);

	switch (nProductType)
	{
	case PRODUCT_TYPE_FUTURES:      stQuery.ProductClass = THOST_FTDC_PC_Futures; break;
	case PRODUCT_TYPE_OPTIONS:      stQuery.ProductClass = THOST_FTDC_PC_Options; break;
	case PRODUCT_TYPE_COMBINATION:  stQuery.ProductClass = THOST_FTDC_PC_Combination; break;
	case PRODUCT_TYPE_SPOT:         stQuery.ProductClass = THOST_FTDC_PC_Spot; break;
	case PRODUCT_TYPE_EFP:          stQuery.ProductClass = THOST_FTDC_PC_EFP; break;
	case PRODUCT_TYPE_SPOT_OPTION:  stQuery.ProductClass = THOST_FTDC_PC_SpotOption; break;
	}

	if (m_pTradeApi->ReqQryProduct(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}

//----------------------------------------------------------------------------------------
// ITrade impl - 持仓接口
//----------------------------------------------------------------------------------------

// 查询持仓
bool CCTPTradeApi::QueryPosition()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryInvestorPositionField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.BrokerID, sizeof(stQuery.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stQuery.InvestorID, sizeof(stQuery.InvestorID), m_szInvestorID.c_str());

	if (m_pTradeApi->ReqQryInvestorPosition(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 查询持仓明细
bool CCTPTradeApi::QueryPositionDetail(const char* pszInstrumentID)
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryInvestorPositionDetailField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.BrokerID, sizeof(stQuery.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stQuery.InvestorID, sizeof(stQuery.InvestorID), m_szInvestorID.c_str());
	strcpy_s(stQuery.InstrumentID, sizeof(stQuery.InstrumentID), pszInstrumentID);

	if (m_pTradeApi->ReqQryInvestorPositionDetail(&stQuery, ++m_nRequestSeq) == 0) 
		return true;
	else 
		return false;
}

//----------------------------------------------------------------------------------------
// ITrade impl - 报单接口
//----------------------------------------------------------------------------------------

// 报单录入
bool CCTPTradeApi::OrderInsert(const char* pszInstrumentID, enTradeType nTradeType, \
	enTradeDir nTradeDir, enTradeOperate nTradeOperate, enTradeOrderType nOrderType, \
	double dPrice, int nVolume, char* pszOrderRefSuffix,int &nRequestID)
{
	_ASSERTE(pszInstrumentID != NULL);
	if (pszInstrumentID == NULL) return false;

	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;


	CThostFtdcInputOrderField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.InstrumentID, sizeof(stQuery.InstrumentID), pszInstrumentID);
	strcpy_s(stQuery.BrokerID, sizeof(stQuery.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stQuery.InvestorID, sizeof(stQuery.InvestorID), m_szInvestorID.c_str());

	// 交易类型(投机,套利,套保)
	switch (nTradeType)
	{
	case TRADE_TYPE_SPECULATION:    stQuery.CombHedgeFlag[0] = THOST_FTDC_HF_Speculation; break;
	case TRADE_TYPE_ARBITRAGE:      stQuery.CombHedgeFlag[0] = THOST_FTDC_HF_Arbitrage; break;
	case TRADE_TYPE_HEDGE:          stQuery.CombHedgeFlag[0] = THOST_FTDC_HF_Hedge;  break;
	}

	// 交易买卖方向
	switch (nTradeDir)
	{
	case TRADE_DIR_BUY:        stQuery.Direction = THOST_FTDC_D_Buy; break;
	case TRADE_DIR_SELL:       stQuery.Direction = THOST_FTDC_D_Sell; break;
	}

	// 交易开平类型
	switch (nTradeOperate)
	{
	case TRADE_OPERATE_OPEN:        stQuery.CombOffsetFlag[0] = THOST_FTDC_OF_Open; break;
	case TRADE_OPERATE_CLOSE:       stQuery.CombOffsetFlag[0] = THOST_FTDC_OF_Close; break;
	case TRADE_OPERATE_CLOSE_TODAY: stQuery.CombOffsetFlag[0] = THOST_FTDC_OF_CloseToday; break;
	case TRADE_OPERATE_EXCUTE:      break;
	}

	stQuery.VolumeTotalOriginal = nVolume;
	stQuery.IsAutoSuspend       = 0;
	stQuery.ContingentCondition = THOST_FTDC_CC_Immediately;
	stQuery.ForceCloseReason    = THOST_FTDC_FCC_NotForceClose;
	stQuery.IsSwapOrder         = 0;
	stQuery.UserForceClose      = 0;
	stQuery.OrderPriceType      = THOST_FTDC_OPT_LimitPrice;
	stQuery.VolumeCondition     = THOST_FTDC_VC_AV;
	stQuery.TimeCondition       = THOST_FTDC_TC_IOC;
	stQuery.MinVolume           = 1;
	stQuery.LimitPrice          = dPrice;

	// 交易订单类型
	switch (nOrderType)
	{
	case TRADE_ORDER_TYPE_LIMIT:    stQuery.TimeCondition = THOST_FTDC_TC_GFD; break;
	case TRADE_ORDER_TYPE_MARKET:   stQuery.OrderPriceType = THOST_FTDC_OPT_AnyPrice; stQuery.LimitPrice = 0; break;
	case TRADE_ORDER_TYPE_FAK:      break;
	case TRADE_ORDER_TYPE_FOK:      stQuery.VolumeCondition = THOST_FTDC_VC_CV; break;
	}

	// OrderRef
	std::string szOrderRefSuffix = pszOrderRefSuffix == NULL ? "\n" : pszOrderRefSuffix;
	int len = strlen(PROXY_PRODUCT_FLAG);
	szOrderRefSuffix = szOrderRefSuffix.length() > len ? szOrderRefSuffix.substr(0, len) : (std::string(len - szOrderRefSuffix.length(), ' ') + szOrderRefSuffix);
	sprintf_s(stQuery.OrderRef, sizeof(stQuery.OrderRef),"%d%s", ++m_nRequestSeq, szOrderRefSuffix.c_str());
	stQuery.RequestID = m_nRequestSeq;
	nRequestID = m_nRequestSeq;

	//USES_CONVERSION;
	//CString str;
	//str.Format(L"发送报单: m_nRequestSeq = %d  OrderRef = %s", m_nRequestSeq, A2T(stQuery.OrderRef));
	//OutputDebugString(str);
	if (m_pTradeApi->ReqOrderInsert(&stQuery, m_nRequestSeq) == 0)
	{
		return true;
	}
	nRequestID = 0;
	return false;
}

// 报单操作
bool CCTPTradeApi::OrderAction(tagCancelOrderInsert* pOrder)
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;


	CThostFtdcInputOrderActionField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));
	strcpy_s(stQuery.InstrumentID, 10, pOrder->szINSTRUMENT);
	stQuery.ActionFlag = THOST_FTDC_AF_Delete;
	stQuery.RequestID = ++m_nRequestSeq;
	stQuery.OrderActionRef = m_nRequestSeq;

	//[1]
	strcpy_s(stQuery.UserID, sizeof(stQuery.UserID), m_szInvestorID.c_str());
	strcpy_s(stQuery.BrokerID, sizeof(stQuery.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stQuery.InvestorID, sizeof(stQuery.InvestorID), m_szInvestorID.c_str());
	stQuery.FrontID = m_nFrontID;
	stQuery.SessionID = pOrder->nSession;
	strcpy_s(stQuery.OrderRef, sizeof(stQuery.OrderRef), pOrder->OrderRef);
		
	////[2]
	//sprintf_s(stQuery.OrderSysID,21,"%d",pOrder->lOrderSysID);
	strcpy_s(stQuery.ExchangeID, sizeof(stQuery.ExchangeID), pOrder->szExchangeID);
	
	if (m_pTradeApi->ReqOrderAction(&stQuery, m_nRequestSeq) == 0) return true;
	else return false;

	return true;

}

/*
52. OnRtnOrder 每次在登陆时都会把上一次的下单结果再重新返回一次，这样是不是有些
不妥啊？ 
【答：】CTP 的公有流和私有流提供三种订阅方式，TERT_RESTART:从本交易日开始重传，
TERT_RESUME:从上次收到的续传，TERT_QUICK:只传送登录后的内容。每次都重传是因
为在订阅时（SubscribePrivateTopic/SubscribePublicTopic）选择了 TERT_RESTART 方式。
*/

// 查询报单
bool CCTPTradeApi::QueryOrder()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryOrderField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.BrokerID, sizeof(stQuery.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stQuery.InvestorID, sizeof(stQuery.InvestorID), m_szInvestorID.c_str());

	if (m_pTradeApi->ReqQryOrder(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}


// 查询成交
bool CCTPTradeApi::QueryTrade()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryTradeField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.BrokerID, sizeof(stQuery.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stQuery.InvestorID, sizeof(stQuery.InvestorID), m_szInvestorID.c_str());

	if (m_pTradeApi->ReqQryTrade(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}


//----------------------------------------------------------------------------------------
// ITrade impl - 预埋单接口
//----------------------------------------------------------------------------------------

// 预埋单录入
bool CCTPTradeApi::ParkedOrderInsert()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcParkedOrderField stParked;
	memset(&stParked, 0, sizeof(stParked));

	/////预埋单
	//struct CThostFtdcParkedOrderField
	//{
	//    ///经纪公司代码
	//    TThostFtdcBrokerIDType	BrokerID;
	//    ///投资者代码
	//    TThostFtdcInvestorIDType	InvestorID;
	//    ///合约代码
	//    TThostFtdcINSTRUMENT1IDType	InstrumentID;
	//    ///报单引用
	//    TThostFtdcOrderRefType	OrderRef;
	//    ///用户代码
	//    TThostFtdcUserIDType	UserID;
	//    ///报单价格条件
	//    TThostFtdcOrderPriceTypeType	OrderPriceType;
	//    ///买卖方向
	//    TThostFtdcDirectionType	Direction;
	//    ///组合开平标志
	//    TThostFtdcCombOffsetFlagType	CombOffsetFlag;
	//    ///组合投机套保标志
	//    TThostFtdcCombHedgeFlagType	CombHedgeFlag;
	//    ///价格
	//    TThostFtdcPriceType	LimitPrice;
	//    ///数量
	//    TThostFtdcVolumeType	VolumeTotalOriginal;
	//    ///有效期类型
	//    TThostFtdcTimeConditionType	TimeCondition;
	//    ///GTD日期
	//    TThostFtdcDateType	GTDDate;
	//    ///成交量类型
	//    TThostFtdcVolumeConditionType	VolumeCondition;
	//    ///最小成交量
	//    TThostFtdcVolumeType	MinVolume;
	//    ///触发条件
	//    TThostFtdcContingentConditionType	ContingentCondition;
	//    ///止损价
	//    TThostFtdcPriceType	StopPrice;
	//    ///强平原因
	//    TThostFtdcForceCloseReasonType	ForceCloseReason;
	//    ///自动挂起标志
	//    TThostFtdcBoolType	IsAutoSuspend;
	//    ///业务单元
	//    TThostFtdcBusinessUnitType	BusinessUnit;
	//    ///请求编号
	//    TThostFtdcRequestIDType	RequestID;
	//    ///用户强评标志
	//    TThostFtdcBoolType	UserForceClose;
	//    ///交易所代码
	//    TThostFtdcExchangeIDType	ExchangeID;
	//    ///预埋报单编号
	//    TThostFtdcParkedOrderIDType	ParkedOrderID;
	//    ///用户类型
	//    TThostFtdcUserTypeType	UserType;
	//    ///预埋单状态
	//    TThostFtdcParkedOrderStatusType	Status;
	//    ///错误代码
	//    TThostFtdcErrorIDType	ErrorID;
	//    ///错误信息
	//    TThostFtdcErrorMsgType	ErrorMsg;
	//    ///互换单标志
	//    TThostFtdcBoolType	IsSwapOrder;
	//};

	if (m_pTradeApi->ReqParkedOrderInsert(&stParked, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 预埋单操作
bool CCTPTradeApi::ParketOrderAction()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcParkedOrderActionField stParked;
	memset(&stParked, 0, sizeof(stParked));

	/////输入预埋单操作
	//struct CThostFtdcParkedOrderActionField
	//{
	//    ///经纪公司代码
	//    TThostFtdcBrokerIDType	BrokerID;
	//    ///投资者代码
	//    TThostFtdcInvestorIDType	InvestorID;
	//    ///报单操作引用
	//    TThostFtdcOrderActionRefType	OrderActionRef;
	//    ///报单引用
	//    TThostFtdcOrderRefType	OrderRef;
	//    ///请求编号
	//    TThostFtdcRequestIDType	RequestID;
	//    ///前置编号
	//    TThostFtdcFrontIDType	FrontID;
	//    ///会话编号
	//    TThostFtdcSessionIDType	SessionID;
	//    ///交易所代码
	//    TThostFtdcExchangeIDType	ExchangeID;
	//    ///报单编号
	//    TThostFtdcOrderSysIDType	OrderSysID;
	//    ///操作标志
	//    TThostFtdcActionFlagType	ActionFlag;
	//    ///价格
	//    TThostFtdcPriceType	LimitPrice;
	//    ///数量变化
	//    TThostFtdcVolumeType	VolumeChange;
	//    ///用户代码
	//    TThostFtdcUserIDType	UserID;
	//    ///合约代码
	//    TThostFtdcINSTRUMENT1IDType	InstrumentID;
	//    ///预埋撤单单编号
	//    TThostFtdcParkedOrderActionIDType	ParkedOrderActionID;
	//    ///用户类型
	//    TThostFtdcUserTypeType	UserType;
	//    ///预埋撤单状态
	//    TThostFtdcParkedOrderStatusType	Status;
	//    ///错误代码
	//    TThostFtdcErrorIDType	ErrorID;
	//    ///错误信息
	//    TThostFtdcErrorMsgType	ErrorMsg;
	//};


	if (m_pTradeApi->ReqParkedOrderAction(&stParked, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 删除预埋单
bool CCTPTradeApi::RemoveParketOrder()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcRemoveParkedOrderField stRemove;
	memset(&stRemove, 0, sizeof(stRemove));

	strcpy_s(stRemove.BrokerID, sizeof(stRemove.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stRemove.InvestorID, sizeof(stRemove.InvestorID), m_szInvestorID.c_str());
	// TODO:
	//strcpy_s(stRemove.ParkedOrderID, m_mapINSTRUMENT1s[stOrder.szINSTRUMENT].szExchangeID);

	if (m_pTradeApi->ReqRemoveParkedOrder(&stRemove, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 删除预埋单操作
bool CCTPTradeApi::RemoveParkedOrderAction()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcRemoveParkedOrderActionField stRemove;
	memset(&stRemove, 0, sizeof(stRemove));

	strcpy_s(stRemove.BrokerID, sizeof(stRemove.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stRemove.InvestorID, sizeof(stRemove.InvestorID), m_szInvestorID.c_str());
	// TODO:
	//strcpy_s(stRemove.ParkedOrderActionID, m_mapINSTRUMENT1s[stOrder.szINSTRUMENT].szExchangeID);

	if (m_pTradeApi->ReqRemoveParkedOrderAction(&stRemove, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 查询预埋单
bool CCTPTradeApi::QueryParkedOrder(const char* pszExchangeID, const char* pszInstrumentID)
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryParkedOrderField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.BrokerID, sizeof(stQuery.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stQuery.InvestorID, sizeof(stQuery.InvestorID), m_szInvestorID.c_str());
	strcpy_s(stQuery.ExchangeID, sizeof(stQuery.ExchangeID), pszExchangeID);
	strcpy_s(stQuery.InstrumentID, sizeof(stQuery.InstrumentID), pszInstrumentID);

	if (m_pTradeApi->ReqQryParkedOrder(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 查询预埋单操作
bool CCTPTradeApi::QueryParkedOrderAction(const char* pszExchangeID, const char* pszInstrumentID)
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryParkedOrderActionField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	strcpy_s(stQuery.BrokerID, sizeof(stQuery.BrokerID), m_szBrokerID.c_str());
	strcpy_s(stQuery.InvestorID, sizeof(stQuery.InvestorID), m_szInvestorID.c_str());
	strcpy_s(stQuery.ExchangeID, sizeof(stQuery.ExchangeID), pszExchangeID);
	strcpy_s(stQuery.InstrumentID, sizeof(stQuery.InstrumentID), pszInstrumentID);

	if (m_pTradeApi->ReqQryParkedOrderAction(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}

//----------------------------------------------------------------------------------------
// ITrade impl - 银期接口
//----------------------------------------------------------------------------------------

// 银行资金转期货
bool CCTPTradeApi::TransferFromBankToFuture()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcReqTransferField stTransfer;
	memset(&stTransfer, 0, sizeof(stTransfer));

	/////转账请求
	//struct CThostFtdcReqTransferField
	//{
	//    ///业务功能码
	//    TThostFtdcTradeCodeType	TradeCode;
	//    ///银行代码
	//    TThostFtdcBankIDType	BankID;
	//    ///银行分支机构代码
	//    TThostFtdcBankBrchIDType	BankBranchID;
	//    ///期商代码
	//    TThostFtdcBrokerIDType	BrokerID;
	//    ///期商分支机构代码
	//    TThostFtdcFutureBranchIDType	BrokerBranchID;
	//    ///交易日期
	//    TThostFtdcTradeDateType	TradeDate;
	//    ///交易时间
	//    TThostFtdcTradeTimeType	TradeTime;
	//    ///银行流水号
	//    TThostFtdcBankSerialType	BankSerial;
	//    ///交易系统日期 
	//    TThostFtdcTradeDateType	TradingDay;
	//    ///银期平台消息流水号
	//    TThostFtdcSerialType	PlateSerial;
	//    ///最后分片标志
	//    TThostFtdcLastFragmentType	LastFragment;
	//    ///会话号
	//    TThostFtdcSessionIDType	SessionID;
	//    ///客户姓名
	//    TThostFtdcIndividualNameType	CustomerName;
	//    ///证件类型
	//    TThostFtdcIdCardTypeType	IdCardType;
	//    ///证件号码
	//    TThostFtdcIdentifiedCardNoType	IdentifiedCardNo;
	//    ///客户类型
	//    TThostFtdcCustTypeType	CustType;
	//    ///银行帐号
	//    TThostFtdcBankAccountType	BankAccount;
	//    ///银行密码
	//    TThostFtdcPasswordType	BankPassWord;
	//    ///投资者帐号
	//    TThostFtdcAccountIDType	AccountID;
	//    ///期货密码
	//    TThostFtdcPasswordType	Password;
	//    ///安装编号
	//    TThostFtdcInstallIDType	InstallID;
	//    ///期货公司流水号
	//    TThostFtdcFutureSerialType	FutureSerial;
	//    ///用户标识
	//    TThostFtdcUserIDType	UserID;
	//    ///验证客户证件号码标志
	//    TThostFtdcYesNoIndicatorType	VerifyCertNoFlag;
	//    ///币种代码
	//    TThostFtdcCurrencyIDType	CurrencyID;
	//    ///转帐金额
	//    TThostFtdcTradeAmountType	TradeAmount;
	//    ///期货可取金额
	//    TThostFtdcTradeAmountType	FutureFetchAmount;
	//    ///费用支付标志
	//    TThostFtdcFeePayFlagType	FeePayFlag;
	//    ///应收客户费用
	//    TThostFtdcCustFeeType	CustFee;
	//    ///应收期货公司费用
	//    TThostFtdcFutureFeeType	BrokerFee;
	//    ///发送方给接收方的消息
	//    TThostFtdcAddInfoType	Message;
	//    ///摘要
	//    TThostFtdcDigestType	Digest;
	//    ///银行帐号类型
	//    TThostFtdcBankAccTypeType	BankAccType;
	//    ///渠道标志
	//    TThostFtdcDeviceIDType	DeviceID;
	//    ///期货单位帐号类型
	//    TThostFtdcBankAccTypeType	BankSecuAccType;
	//    ///期货公司银行编码
	//    TThostFtdcBankCodingForFutureType	BrokerIDByBank;
	//    ///期货单位帐号
	//    TThostFtdcBankAccountType	BankSecuAcc;
	//    ///银行密码标志
	//    TThostFtdcPwdFlagType	BankPwdFlag;
	//    ///期货资金密码核对标志
	//    TThostFtdcPwdFlagType	SecuPwdFlag;
	//    ///交易柜员
	//    TThostFtdcOperNoType	OperNo;
	//    ///请求编号
	//    TThostFtdcRequestIDType	RequestID;
	//    ///交易ID
	//    TThostFtdcTIDType	TID;
	//    ///转账交易状态
	//    TThostFtdcTransferStatusType	TransferStatus;
	//};


	if (m_pTradeApi->ReqFromBankToFutureByFuture(&stTransfer, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 期货资金转银行
bool CCTPTradeApi::TransferFromFutureToBank()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcReqTransferField stTransfer;
	memset(&stTransfer, 0, sizeof(stTransfer));

	/////转账请求
	//struct CThostFtdcReqTransferField
	//{
	//    ///业务功能码
	//    TThostFtdcTradeCodeType	TradeCode;
	//    ///银行代码
	//    TThostFtdcBankIDType	BankID;
	//    ///银行分支机构代码
	//    TThostFtdcBankBrchIDType	BankBranchID;
	//    ///期商代码
	//    TThostFtdcBrokerIDType	BrokerID;
	//    ///期商分支机构代码
	//    TThostFtdcFutureBranchIDType	BrokerBranchID;
	//    ///交易日期
	//    TThostFtdcTradeDateType	TradeDate;
	//    ///交易时间
	//    TThostFtdcTradeTimeType	TradeTime;
	//    ///银行流水号
	//    TThostFtdcBankSerialType	BankSerial;
	//    ///交易系统日期 
	//    TThostFtdcTradeDateType	TradingDay;
	//    ///银期平台消息流水号
	//    TThostFtdcSerialType	PlateSerial;
	//    ///最后分片标志
	//    TThostFtdcLastFragmentType	LastFragment;
	//    ///会话号
	//    TThostFtdcSessionIDType	SessionID;
	//    ///客户姓名
	//    TThostFtdcIndividualNameType	CustomerName;
	//    ///证件类型
	//    TThostFtdcIdCardTypeType	IdCardType;
	//    ///证件号码
	//    TThostFtdcIdentifiedCardNoType	IdentifiedCardNo;
	//    ///客户类型
	//    TThostFtdcCustTypeType	CustType;
	//    ///银行帐号
	//    TThostFtdcBankAccountType	BankAccount;
	//    ///银行密码
	//    TThostFtdcPasswordType	BankPassWord;
	//    ///投资者帐号
	//    TThostFtdcAccountIDType	AccountID;
	//    ///期货密码
	//    TThostFtdcPasswordType	Password;
	//    ///安装编号
	//    TThostFtdcInstallIDType	InstallID;
	//    ///期货公司流水号
	//    TThostFtdcFutureSerialType	FutureSerial;
	//    ///用户标识
	//    TThostFtdcUserIDType	UserID;
	//    ///验证客户证件号码标志
	//    TThostFtdcYesNoIndicatorType	VerifyCertNoFlag;
	//    ///币种代码
	//    TThostFtdcCurrencyIDType	CurrencyID;
	//    ///转帐金额
	//    TThostFtdcTradeAmountType	TradeAmount;
	//    ///期货可取金额
	//    TThostFtdcTradeAmountType	FutureFetchAmount;
	//    ///费用支付标志
	//    TThostFtdcFeePayFlagType	FeePayFlag;
	//    ///应收客户费用
	//    TThostFtdcCustFeeType	CustFee;
	//    ///应收期货公司费用
	//    TThostFtdcFutureFeeType	BrokerFee;
	//    ///发送方给接收方的消息
	//    TThostFtdcAddInfoType	Message;
	//    ///摘要
	//    TThostFtdcDigestType	Digest;
	//    ///银行帐号类型
	//    TThostFtdcBankAccTypeType	BankAccType;
	//    ///渠道标志
	//    TThostFtdcDeviceIDType	DeviceID;
	//    ///期货单位帐号类型
	//    TThostFtdcBankAccTypeType	BankSecuAccType;
	//    ///期货公司银行编码
	//    TThostFtdcBankCodingForFutureType	BrokerIDByBank;
	//    ///期货单位帐号
	//    TThostFtdcBankAccountType	BankSecuAcc;
	//    ///银行密码标志
	//    TThostFtdcPwdFlagType	BankPwdFlag;
	//    ///期货资金密码核对标志
	//    TThostFtdcPwdFlagType	SecuPwdFlag;
	//    ///交易柜员
	//    TThostFtdcOperNoType	OperNo;
	//    ///请求编号
	//    TThostFtdcRequestIDType	RequestID;
	//    ///交易ID
	//    TThostFtdcTIDType	TID;
	//    ///转账交易状态
	//    TThostFtdcTransferStatusType	TransferStatus;
	//};


	if (m_pTradeApi->ReqFromFutureToBankByFuture(&stTransfer, ++m_nRequestSeq) == 0) return true;
	else return false;
}


// 查询转账银行
bool CCTPTradeApi::QueryTransferBank()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryTransferBankField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	// TODO:
	/////银行代码
	//TThostFtdcBankIDType	BankID;
	/////银行分中心代码
	//TThostFtdcBankBrchIDType	BankBrchID;

	if (m_pTradeApi->ReqQryTransferBank(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 查询转账流水
bool CCTPTradeApi::QueryTransferStatement()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryTransferSerialField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	/////经纪公司代码
	//TThostFtdcBrokerIDType	BrokerID;
	/////投资者帐号
	//TThostFtdcAccountIDType	AccountID;
	/////银行编码
	//TThostFtdcBankIDType	BankID;
	/////币种代码
	//TThostFtdcCurrencyIDType	CurrencyID;


	if (m_pTradeApi->ReqQryTransferSerial(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 查询银期签约关系
bool CCTPTradeApi::QueryBankFutureContract()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryAccountregisterField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	/////经纪公司代码
	//TThostFtdcBrokerIDType	BrokerID;
	/////投资者帐号
	//TThostFtdcAccountIDType	AccountID;
	/////银行编码
	//TThostFtdcBankIDType	BankID;
	/////银行分支机构编码
	//TThostFtdcBankBrchIDType	BankBranchID;
	/////币种代码
	//TThostFtdcCurrencyIDType	CurrencyID;

	if (m_pTradeApi->ReqQryAccountregister(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 查询签约银行
bool CCTPTradeApi::QueryContractBank()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcQryContractBankField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	/////经纪公司代码
	//TThostFtdcBrokerIDType	BrokerID;
	/////银行代码
	//TThostFtdcBankIDType	BankID;
	/////银行分中心代码
	//TThostFtdcBankBrchIDType	BankBrchID;

	if (m_pTradeApi->ReqQryContractBank(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}

// 查询银行余额
bool CCTPTradeApi::QueryBankRemainingBalance()
{
	_ASSERTE(m_pTradeApi != NULL && m_nSessionID != 0);
	if (m_pTradeApi == NULL || m_nSessionID == 0) return false;

	CThostFtdcReqQueryAccountField stQuery;
	memset(&stQuery, 0, sizeof(stQuery));

	/////查询账户信息请求
	//struct CThostFtdcReqQueryAccountField
	//{
	//    ///业务功能码
	//    TThostFtdcTradeCodeType	TradeCode;
	//    ///银行代码
	//    TThostFtdcBankIDType	BankID;
	//    ///银行分支机构代码
	//    TThostFtdcBankBrchIDType	BankBranchID;
	//    ///期商代码
	//    TThostFtdcBrokerIDType	BrokerID;
	//    ///期商分支机构代码
	//    TThostFtdcFutureBranchIDType	BrokerBranchID;
	//    ///交易日期
	//    TThostFtdcTradeDateType	TradeDate;
	//    ///交易时间
	//    TThostFtdcTradeTimeType	TradeTime;
	//    ///银行流水号
	//    TThostFtdcBankSerialType	BankSerial;
	//    ///交易系统日期 
	//    TThostFtdcTradeDateType	TradingDay;
	//    ///银期平台消息流水号
	//    TThostFtdcSerialType	PlateSerial;
	//    ///最后分片标志
	//    TThostFtdcLastFragmentType	LastFragment;
	//    ///会话号
	//    TThostFtdcSessionIDType	SessionID;
	//    ///客户姓名
	//    TThostFtdcIndividualNameType	CustomerName;
	//    ///证件类型
	//    TThostFtdcIdCardTypeType	IdCardType;
	//    ///证件号码
	//    TThostFtdcIdentifiedCardNoType	IdentifiedCardNo;
	//    ///客户类型
	//    TThostFtdcCustTypeType	CustType;
	//    ///银行帐号
	//    TThostFtdcBankAccountType	BankAccount;
	//    ///银行密码
	//    TThostFtdcPasswordType	BankPassWord;
	//    ///投资者帐号
	//    TThostFtdcAccountIDType	AccountID;
	//    ///期货密码
	//    TThostFtdcPasswordType	Password;
	//    ///期货公司流水号
	//    TThostFtdcFutureSerialType	FutureSerial;
	//    ///安装编号
	//    TThostFtdcInstallIDType	InstallID;
	//    ///用户标识
	//    TThostFtdcUserIDType	UserID;
	//    ///验证客户证件号码标志
	//    TThostFtdcYesNoIndicatorType	VerifyCertNoFlag;
	//    ///币种代码
	//    TThostFtdcCurrencyIDType	CurrencyID;
	//    ///摘要
	//    TThostFtdcDigestType	Digest;
	//    ///银行帐号类型
	//    TThostFtdcBankAccTypeType	BankAccType;
	//    ///渠道标志
	//    TThostFtdcDeviceIDType	DeviceID;
	//    ///期货单位帐号类型
	//    TThostFtdcBankAccTypeType	BankSecuAccType;
	//    ///期货公司银行编码
	//    TThostFtdcBankCodingForFutureType	BrokerIDByBank;
	//    ///期货单位帐号
	//    TThostFtdcBankAccountType	BankSecuAcc;
	//    ///银行密码标志
	//    TThostFtdcPwdFlagType	BankPwdFlag;
	//    ///期货资金密码核对标志
	//    TThostFtdcPwdFlagType	SecuPwdFlag;
	//    ///交易柜员
	//    TThostFtdcOperNoType	OperNo;
	//    ///请求编号
	//    TThostFtdcRequestIDType	RequestID;
	//    ///交易ID
	//    TThostFtdcTIDType	TID;
	//};

	if (m_pTradeApi->ReqQueryBankAccountMoneyByFuture(&stQuery, ++m_nRequestSeq) == 0) return true;
	else return false;
}


